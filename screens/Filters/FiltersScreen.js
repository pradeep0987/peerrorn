import { View, StyleSheet, Modal, ImageBackground, Text, Image, TouchableOpacity, Pressable, PanResponder, Platform } from 'react-native';
import BackgroundHideButton from '../../components/comman/BackgroundHideButton.js';
import CommanButton from '../../components/comman/CommanButton';
import { Colors } from '../../src/colors/Colors.js';
import { useRoute } from '@react-navigation/native';
import CheckBox from '@react-native-community/checkbox';
import { useCallback, useEffect, useState, useRef } from 'react';
import RangeSlider from 'rn-range-slider';
import AsyncStorage from '@react-native-async-storage/async-storage';

function FilterScreen({ navigation, onBackPress, filter }) {
    const [partTime, setPartTime] = useState(true);
    const [fullTime, setFullTime] = useState(true);
    const [seasonal, setSeasonal] = useState(true);
    const [low, setLow] = useState();
    const [high, setHigh] = useState();
    const [pay, setPay] = useState(0);
    const [distence, setDistence] = useState(50);
    const distanceRef = useRef();

    useEffect(() => {
        getList();
    }, [])

    async function getList() {
        const filterRes = await getFilterList();
        if (filterRes != null || filterRes !== "") {
            setPay(filterRes.salary != null ? filterRes.salary : 0)
            setDistence(filterRes.distence)
            if (filterRes.partTime === 1) {
                setPartTime(true)
            } else {
                setPartTime(false)
            }

            if (filterRes.fullTime === 2) {
                setFullTime(true)
            } else {
                setFullTime(false)
            }

            if (filterRes.seasonal === 4) {
                setSeasonal(true)
            } else {
                setSeasonal(false)
            }
        }
    }

    async function getFilterList() {
        try {
            const list = await AsyncStorage.getItem('filterData')
            return list != null ? JSON.parse(list) : null;
        } catch (e) {
            console.log(e)
        }
    }

    function partTimeHandle() {
        if (partTime) {
            setPartTime(false)
        } else {
            setPartTime(true)
        }
    }

    function fullTimeHandle() {
        if (fullTime) {
            setFullTime(false)
        } else {
            setFullTime(true)
        }
    }

    function seasonalHandle() {
        if (seasonal) {
            setSeasonal(false)
        } else {
            setSeasonal(true)
        }
    }

    async function handleApplyFilter() {
        const filterData = JSON.stringify({
            salary: pay > 0 || pay != null ? pay : 0,
            distence: distence,
            partTime: partTime ? 1 : '',
            fullTime: fullTime ? 2 : '',
            seasonal: seasonal ? 4 : '',
        });

        await AsyncStorage.setItem("filterData", filterData);
        onBackPress(true)
    }

    const renderThumb = useCallback(() => <View style={{ width: 15, height: 15, backgroundColor: Colors.white, borderRadius: 100, borderColor: Colors.blue, borderWidth: 1.5 }}></View>, []);
    const renderRail = useCallback(() => <View style={{ width: '100%', height: 4, backgroundColor: "#f0f0f0" }}></View>, []);
    const renderRailSelected = useCallback(() => <View style={{ width: '100%', height: 4, backgroundColor: Colors.blue }}></View>, []);
    const renderLabel = useCallback(value => (
        <View style={{ marginBottoma: 30, backgroundColor: Colors.blue, width: 80, alignItems: 'center', height: 35, justifyContent: 'center' }}>
            <Text style={{ textAlign: 'left', color: Colors.white, fontFamily: 'ws_semibold', fontWeight: '600', fontSize: 20 }}>{value} mi</Text>
        </View>), []);
    const renderNotch = useCallback(() => <View style={{
        width: 0,
        height: 0,
        backgroundColor: "transparent",
        borderStyle: "solid",
        borderLeftWidth: 5,
        borderRightWidth: 5,
        borderBottomWidth: 10,
        borderLeftColor: "transparent",
        borderRightColor: "transparent",
        borderBottomColor: Colors.blue,
        transform: [{ rotate: '180deg' }]
    }}></View>, []);
    const handleValueChange = useCallback((low, high, distence) => {
        setDistence(low)
    }, []);


    const renderThumbPay = useCallback(() => <View style={{ width: 15, height: 15, backgroundColor: Colors.white, borderRadius: 100, borderColor: Colors.blue, borderWidth: 1.5 }}></View>, []);
    const renderRailPay = useCallback(() => <View style={{ width: '100%', height: 4, backgroundColor: "#f0f0f0" }}></View>, []);
    const renderRailSelectedPay = useCallback(() => <View style={{ width: '100%', height: 4, backgroundColor: Colors.blue }}></View>, []);
    const renderLabelPay = useCallback(value => (
        <View style={{ marginBottoma: 30, backgroundColor: Colors.blue, width: 100, alignItems: 'center', height: 35, justifyContent: 'center' }}>
            <Text style={{ textAlign: 'left', color: Colors.white, fontFamily: 'ws_semibold', fontWeight: '600', fontSize: 20 }}>$ {value}/hr</Text>
        </View>), []);
    const renderNotchPay = useCallback(() => <View style={{
        width: 0,
        height: 0,
        backgroundColor: "transparent",
        borderStyle: "solid",
        borderLeftWidth: 5,
        borderRightWidth: 5,
        borderBottomWidth: 10,
        borderLeftColor: "transparent",
        borderRightColor: "transparent",
        borderBottomColor: Colors.blue,
        transform: [{ rotate: '180deg' }]
    }} ></View>, []);
    const handleValueChangePay = useCallback((low, high, pay) => {
        setPay(low)
    }, []);

    return (
        <Modal
            animationType='fade'
            transparent={true}
            visible={filter}>
            <View style={{ flex: 1, backgroundColor: Colors.white, marginTop: Platform.OS === 'ios' ? 40 : 0 }}>
                <View style={{ padding: 20 }}>
                    <TouchableOpacity style={{ width: 20, height: 20 }} onPress={() => onBackPress(true)}>
                        <Image style={{ width: 20, height: 20 }} source={require('../../src/images/arrow-back.png')} />
                    </TouchableOpacity>
                </View>
                <View style={{ paddingEnd: 22, paddingStart: 30 }}>
                    <Text style={{ textAlign: 'left', color: Colors.blue, fontFamily: 'ws_semibold', fontSize: 24 }}>Filters</Text>
                </View>
                <View style={{ paddingEnd: 22, paddingStart: 30, marginTop: 20 }}>
                    <Text style={{ textAlign: 'left', color: Colors.colorBlack, fontFamily: 'ws_semibold', fontWeight: '600', fontSize: 20 }}>Distence</Text>
                    <RangeSlider
                        style={{ width: '100%', height: 80, marginTop: 5 }}
                        gravity={'center'}
                        min={10}
                        max={200}
                        step={1}
                        low={distence}
                        disableRange={true}
                        renderThumb={renderThumb}
                        renderRail={renderRail}
                        renderLabel={renderLabel}
                        renderNotch={renderNotch}
                        onValueChanged={handleValueChange}
                        renderRailSelected={renderRailSelected}
                    />

                </View>
                <View style={{ paddingEnd: 22, paddingStart: 30, marginTop: 20 }}>
                    <Text style={{ textAlign: 'left', color: Colors.colorBlack, fontFamily: 'ws_semibold', fontWeight: '600', fontSize: 20 }}>Pay</Text>
                    <RangeSlider
                        style={{ width: '100%', height: 80, marginTop: 5 }}
                        gravity={'center'}
                        min={0}
                        max={100}
                        step={1}
                        low={pay}
                        floatingLabel={false}
                        disableRange={true}
                        renderThumb={renderThumbPay}
                        renderRail={renderRailPay}
                        renderLabel={renderLabelPay}
                        renderNotch={renderNotchPay}
                        onValueChanged={handleValueChangePay}
                        renderRailSelected={renderRailSelectedPay}
                    />
                </View>
                <View style={{ paddingEnd: 22, paddingStart: 30, marginTop: 40 }} >
                    <Text style={{ textAlign: 'left', color: Colors.colorBlack, fontFamily: 'ws_semibold', fontWeight: '600', fontSize: 20 }}>Jobs Type</Text>
                    <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 15 }}>
                        <Pressable style={{ flexDirection: 'row', alignItems: 'center' }} onPress={partTimeHandle}>
                            <CheckBox value={partTime} onCheckColor='Blue' tintColors={{ true: '#06ccc2', false: Colors.blue }} disabled={true} />
                            <Text style={{ textAlign: 'left', color: Colors.colorGrey, fontFamily: 'ws_medium', fontWeight: '600', fontSize: 16 }}>Part-Time</Text>
                        </Pressable>
                    </View>
                    <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 10 }}>
                        <Pressable style={{ flexDirection: 'row', alignItems: 'center' }} onPress={fullTimeHandle}>
                            <CheckBox value={fullTime} onCheckColor='Blue' tintColors={{ true: '#06ccc2', false: Colors.blue }} disabled={true} />
                            <Text style={{ textAlign: 'left', color: Colors.colorGrey, fontFamily: 'ws_medium', fontWeight: '600', fontSize: 16 }}>Full-Time</Text>
                        </Pressable>
                    </View>
                    <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 10 }}>
                        <Pressable style={{ flexDirection: 'row', alignItems: 'center' }} onPress={seasonalHandle}>
                            <CheckBox value={seasonal} onCheckColor='Blue' tintColors={{ true: '#06ccc2', false: Colors.blue }} disabled={true} />
                            <Text style={{ textAlign: 'left', color: Colors.colorGrey, fontFamily: 'ws_medium', fontWeight: '600', fontSize: 16 }}>Seasonal</Text>
                        </Pressable>
                    </View>
                </View>
                <View style={{
                    flex: 1,
                    width: '100%',
                    height: 50,
                    position: 'absolute',
                    marginBottom: 30,
                    bottom: 0,
                    paddingEnd: 40,
                    paddingStart: 40
                }}>
                    <CommanButton title='APPLY FILTERS' width={'100%'} height={50} backgroundColor={Colors.accentMagenta} borderRadius={20} textColor={Colors.white}
                        onPress={handleApplyFilter} />
                </View>
            </View>
        </Modal>
    );
}

export default FilterScreen;