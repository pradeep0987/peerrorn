import { View,StyleSheet, TouchableOpacity, Image, ImageBackground, Modal,Text, Share, ToastAndroid, Alert } from 'react-native';
import BackgroundHideButton from '../../components/comman/BackgroundHideButton';
import CommanButton from '../../components/comman/CommanButton';
import { Colors } from '../../src/colors/Colors';
import { Vimeo } from 'react-native-vimeo';
import { useEffect, useState } from 'react';
import { getKeepWatching } from '../../restapi/Services';
import VimeoPlayer from '../../components/VimeoPlayer/VimeoPlayer';
import LoadingScreen from '../LoadingScreens/LoadingScreen';
import NetInfo from '@react-native-community/netinfo';
import { Platform } from 'react-native/Libraries/Utilities/Platform';
import AsyncStorage from '@react-native-async-storage/async-storage';

function VideoWizwedJobsScreen({navigation}) {
    const [modalShow , setModalShow] = useState(false);
    const [video , setVideo] = useState();
    const [loading , setLoading] = useState(false);

    async function onShare() {
      try {
        const onBoard = await getData();
        const result = await Share.share({
          message:
          "Hey what’s up!\n" +
          "You should check out this app called Peerro. It’s great for finding new opportunities near you.\n" + onBoard[0].referral_data.referral_short_link,
        });
  
        if (result.action === Share.sharedAction) {
          if (result.activityType) {
            console.log('ready')
          } else {
            console.log('shared')
          }
        } else if (result.action === Share.dismissedAction) {
          console.log('dismiss')
        }
      } catch (error) {
        console.log(error)
      }
    };

    useEffect(() => {
      handleRead();
    })

    async function handleRead() {
      const response = await getData();
      if (response !== null) 
      {
          if (response.length > 0) 
          {
              var videoIndex = parseInt(0, 10);
              const data = response[videoIndex]
              if (data != null) 
              {
                if (data.has_video === 1)
                {
                  setVideo(data.vimeo_embed_url);
                }
              }

          }
      }
  }

  const getData = async () => {
      try {
        const response = await AsyncStorage.getItem('onboardinfo')
        return response != null ? JSON.parse(response) : null;
      } catch(e) {
        console.log(e)
      }
  }

  async function handleVideo() {
    await AsyncStorage.setItem('index', '1');
    navigation.navigate('onBoardVideos');
  }

  const screenData = async () => {
    try {
        await AsyncStorage.setItem('screenState', '5')
    } catch (e) {
        console.log(e);
    }
}

    return(
        <View style={{flex: 1}} >
          <VimeoPlayer video_url={video} height={950} width={500} />
          <View style={{padding: 20,position: 'absolute', top: 0, width: '100%'}}>
              <TouchableOpacity style={{width: 50 ,height: 40, alignSelf: 'flex-end', alignItems: 'center'}} onPress={onShare}>
                  <Image style={{width: 40 ,height: 40, tintColor: Colors.lightWhite}} source={require('../../src/images/share-inactive.png')} />
                  <Text style={{fontSize: 12, alignSelf: 'center', textAlign: 'center', fontFamily: 'ws_regular', color: Colors.white}} >SHARE</Text>
              </TouchableOpacity>
          </View>
          <View style={{width: '100%',position: 'absolute',bottom: 0, padding: 25}}>
              <CommanButton title='KEEP WATCHING VIDEOS' width={'100%'} height={40} backgroundColor={Colors.white} borderRadius={20} textColor={Colors.accentMagenta} onPress={handleVideo}/>
              <View style={{width: '100%', height: 50, flexDirection: 'row',justifyContent: 'space-between', alignItems: 'center', marginTop: 20}}>
                  <BackgroundHideButton title='CAREER WIZERD' width={170} height={40} borderColor={Colors.white} borderWidth={2} borderRadius={20} textColor={Colors.white} onPress={() => navigation.navigate('CareerWizerd')}/>
                  <BackgroundHideButton title='VIEW JOBS' width={170} height={40} borderColor={Colors.white} borderWidth={2} borderRadius={20} textColor={Colors.white} marginStart={5} onPress={() => {
                    navigation.navigate('PersonalInfo')
                    screenData()}}/>
              </View>
          </View>
          {loading && <LoadingScreen/>}
        </View>
    );
}

export default VideoWizwedJobsScreen;