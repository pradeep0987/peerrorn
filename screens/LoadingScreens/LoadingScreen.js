import { Image, Modal, View } from "react-native";
import { Colors } from "../../src/colors/Colors";


function LoadingScreen () {
    return(
        <Modal
        animationType='fade'
        transparent={true}
        visible={true}
        style={{flex: 1, borderRadius: 10, justifyContent: 'center', alignItems: 'center'}}>
            <View style={{flex: 1, alignItems: 'center', justifyContent: 'center', backgroundColor: Colors.colorTransparentBlack}}>
                <Image
                    source={require('../../src/images/peerro_gif.gif')}
                    style={{
                      width: 100,
                      height: 100,
                    }}
                />
            </View>
        </Modal>
    );
}

export default LoadingScreen;