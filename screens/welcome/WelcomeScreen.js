import { View, Text, StyleSheet, Image, TouchableOpacity, ToastAndroid, Pressable, ImageBackground } from 'react-native';
import { Colors } from '../../src/colors/Colors';
import { useState, useEffect } from 'react';
import { getOnBoardService } from '../../restapi/Services';
import { useRoute } from '@react-navigation/native';
import LoadingScreen from '../LoadingScreens/LoadingScreen';
import AsyncStorage from '@react-native-async-storage/async-storage';

function WelcomeScreen({ navigation }) {
    const [videoThumbnail, setVideoThumbnail] = useState('');
    const [loading, setLoading] = useState(false);
    const route = useRoute();
    const status = route.params?.status
    useEffect(() => {
        if (status)
        {
            handleTimer();
        } else {
            handleTimer();
        }
    }, [navigation, videoThumbnail, route])

    async function handleTimer() {
        const response = await getOnBoardService();
        setLoading(true)
        console.log(response.data)
        if (response.data != null) {
            if (response.data.status) {
                setLoading(false)
                setVideoThumbnail(response.data.onboard_info[0].video_thumbnail)
                setTimeout(function () {
                    navigation.navigate('SignMain', { onBoardInfo: response.data.onboard_info[0].vimeo_embed_url, show: true })
                    handleApplyFilter()
                }, 4000)
            }
        }
    }

    async function handleApplyFilter() {
        const filterData = JSON.stringify({
            salary: 0,
            distence: 50,
            partTime: 1,
            fullTime: 2,
            seasonal: 4,
        });

        await AsyncStorage.setItem("filterData", filterData);
    }

    if (!videoThumbnail) {
        return <LoadingScreen />
    }

    return (
        <View style={{ flex: 1 }}>
            <ImageBackground style={{ flex: 1, justifyContent: 'center' }} source={{ uri: videoThumbnail }} >
                <ImageBackground style={{ flex: 1, justifyContent: 'center', padding: 20 }} source={require('../../src/images/keep_watch.png')} >
                    <View style={{ width: '100%', position: 'absolute', bottom: 0, marginBottom: 40, marginStart: 20 }} >
                        <Text style={{ color: Colors.white, textAlign: 'center', fontSize: 24, fontFamily: 'ws_semibold' }} >Weclcome To Peerro</Text>
                        <Text style={{ color: Colors.white, textAlign: 'center', fontSize: 26, fontFamily: 'ws_italic' }} >Find your portion</Text>
                    </View>
                </ImageBackground>
            </ImageBackground>
        </View>
    );
}

export default WelcomeScreen;