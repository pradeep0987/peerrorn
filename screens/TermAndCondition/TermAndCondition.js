import { useRoute } from "@react-navigation/native";
import { View, TouchableOpacity, Image, Modal, Text, Platform } from "react-native";
import LoadingScreen from "../LoadingScreens/LoadingScreen";
import { useState, useEffect } from 'react';
import { Colors } from "../../src/colors/Colors";

const { default: WebView } = require("react-native-webview");


function TermAndConditions ({navigation}) {
    const route = useRoute();
    const [loading , setLoading] = useState(true);
    const uri = route.params?.termsAndCondition;

    useEffect(() => {
        if (uri)
        {
            setLoading(false)
        }
    }, [route,  loading])

    const IndicatorLoadingView = () => { return <LoadingScreen/> };

    return(
        <View style={{flex: 1, marginTop: Platform.OS === 'ios' ? 40 : 0}}>
            <View style={{padding: 20, backgroundColor: Colors.blue, flexDirection: 'row'}}>
                <TouchableOpacity style={{width: 20 ,height: 20}} onPress={() => navigation.navigate('SignUp')}>
                    <Image style={{width: 20 ,height: 20, tintColor: Colors.white}} source={require('../../src/images/arrow-back.png')} />
                </TouchableOpacity>
                <Text style={{color: Colors.white, fontSize: 18, fontFamily: 'ws_semibold', textAlign: 'center', width: '85%'}}>Term & Conditions</Text>
            </View>
            <WebView source={{uri : uri}} renderLoading={IndicatorLoadingView}/>
            {!uri && <LoadingScreen/>}
        </View>
        
    );
}

export default TermAndConditions;