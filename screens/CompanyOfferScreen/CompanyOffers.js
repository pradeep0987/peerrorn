import { View,StyleSheet, TouchableOpacity, Image, ImageBackground, Text, Modal, Platform } from 'react-native';
import BackgroundHideButton from '../../components/comman/BackgroundHideButton';
import CommanButton from '../../components/comman/CommanButton';
import { Colors } from '../../src/colors/Colors';
import { useState } from 'react';
import AsyncStorage from '@react-native-async-storage/async-storage';

function CompanyOfferScreen({navigation, pay, age, companyName, template, companyLogo, onPressBack}) {
    const [back , setBack] = useState(template);

    function handleBack()
    {
        if(back)
        {
            setBack(false)
            onPressBack(false)
        }
    }

    const screenData = async () => {
        try {
            await AsyncStorage.setItem('screenState', '5')
        } catch (e) {
          console.log(e);
        }
    }

    return(
        <Modal
            animationType="fade"
            transparent={true}
            visible={back}>
            <View style={{flex: 1, backgroundColor: Colors.white, marginTop: Platform.OS === 'ios' ? 40 : 0}}>
                <View style={{padding: 30, justifyContent: 'center'}}>
                        <Image style={{width: 120 ,height: 120, alignSelf: 'center', marginTop: 10, borderRadius: 100}} source={companyLogo ? {uri : companyLogo} : require('../../src/images/img_background.png')} />
                        <Text style={{fontSize: 20, alignSelf: 'center', marginTop: 20,marginEnd: 50, marginStart: 50, textAlign: 'center',fontWeight: '600', color: Colors.accentMagenta, fontFamily: 'ws_medium'}} >{companyName} is{'\n'}offering{'\n'}you an interview!</Text>
                        <Text style={{fontSize: 16, alignSelf: 'center', marginTop: 20,marginEnd: 50, marginStart: 50, textAlign: 'center',fontWeight: '500', fontFamily: 'ws_regular'}} >
                            Do you want to schedule an{'\n'}interview for a job that pays{'\n'}${pay}/Hourly?</Text>
                        <Text style={{fontSize: 16, alignSelf: 'center', marginTop: 20, textAlign: 'center',fontWeight: '400', fontFamily: 'ws_regular'}} >Are you {age} years or older?</Text>
                        <CommanButton title={'Schedule Interview'} width={'100%'} height={50} textColor={Colors.white} backgroundColor={Colors.accentMagenta} borderRadius={100} marginTop={30}
                            onPress={() => {
                                navigation.navigate('PersonalInfo')
                                screenData()}}/>
                        <BackgroundHideButton title={'SKIP'} width={'100%'} height={50} textColor={Colors.accentMagenta} marginTop={10}
                            onPress={handleBack}/>
                    </View>
            </View>
        </Modal>
    );
}

export default CompanyOfferScreen;