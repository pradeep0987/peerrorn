import { View, StyleSheet, Modal, ImageBackground, Text, TouchableOpacity, Image, Pressable, Alert, ActivityIndicator, Platform } from 'react-native';
import { Colors } from '../../src/colors/Colors';
import CardStack, { Card } from 'react-native-card-stack-swiper';
import { useRef, useState } from 'react';
import { updateIndustry } from '../../restapi/Services';
import AsyncStorage from '@react-native-async-storage/async-storage';
import LoadingScreen from '../LoadingScreens/LoadingScreen';

const cardList = [
    { industry_id: '1', industry_name: 'industry_business', drawable: require('../../src/images/industry_business.png') },
    { industry_id: '2', industry_name: 'industry_engineering', drawable: require('../../src/images/industry_engineering.png') },
    { industry_id: '3', industry_name: 'industry_service_retail_culinary', drawable: require('../../src/images/industry_service_retail_culinary.png') },
    { industry_id: '4', industry_name: 'industry_legal', drawable: require('../../src/images/industry_legal.png') },
    { industry_id: '5', industry_name: 'industry_education', drawable: require('../../src/images/industry_education.png') },
    { industry_id: '6', industry_name: 'industry_arts', drawable: require('../../src/images/industry_arts.png') },
    { industry_id: '7', industry_name: 'industry_healthcare', drawable: require('../../src/images/industry_healthcare.png') },
    { industry_id: '8', industry_name: 'industry_construction', drawable: require('../../src/images/industry_construction.png') },
    { industry_id: '9', industry_name: 'industry_farming', drawable: require('../../src/images/industry_farming.png') },
    { industry_id: '10', industry_name: 'industry_manufacturing', drawable: require('../../src/images/industry_manufacturing.png') },
    { industry_id: '11', industry_name: 'industry_transport_warehousing', drawable: require('../../src/images/industry_transport_warehousing.png') }
]

function CardSwapingScreen({ navigation }) {
    const [swiper, setSwiper] = useState();
    const [end, setEnd] = useState(false);
    const [leftId, setLeftId] = useState();
    const [rightId, setRightId] = useState();
    const [listData] = useState([])
    const [showOpp, setShowOpp] = useState(false);
    const [showPoint, setShowPoints] = useState(false);
    const [numPoint, setPoint] = useState(0);
    const [plainText, setPlainText] = useState('');
    const [loading, setLoading] = useState(false);


    async function hnadleSwipedAll() {
        setLoading(true)
        const user = await getData();
        let data = JSON.stringify({
            "userdata": {
                "user_id": user.user_id,
                "industry_id": "1,2,3,4,5,6,7,8,9,10,11"
            }
        });
        const response = await updateIndustry(data);
        console.log(response)
        if (response != null) {
            if (response.data != null) {
                if (response.data.status) {
                    screenData()
                    setLoading(false)
                    showPointAndOpp(response.data)
                }
            }
        }
    }

    function showPointAndOpp(data) {
        if (data.show_point === 1) {
            setShowOpp(true)
            setShowPoints(true)
            setPoint(data.point)
            setPlainText(data.point_text)
            setTimeout(function () {
                setShowOpp(false)
                setShowPoints(false)
                setEnd(true)
                navigation.navigate('PersonalInfo')
            }, 3000)
        } else {
            setShowOpp(true)
            setTimeout(function () {
                setShowOpp(false)
                setEnd(true)
                navigation.navigate('PersonalInfo')
            }, 3000)
        }
    }

    const getData = async () => {
        try {
            const response = await AsyncStorage.getItem('userdata')
            return response != null ? JSON.parse(response) : null;
        } catch (e) {
            console.log(e)
        }
    }

    const screenData = async () => {
        try {
            await AsyncStorage.setItem('screenState', '5')
        } catch (e) {
            console.log(e);
        }
    }

    return (
        <View style={{ flex: 1, marginTop: Platform.OS === 'ios' ? 40 : 0 }}>
            <View style={{ paddingEnd: 20, paddingStart: 30, paddingTop: 30 }}>
                <Pressable style={{ width: 20, height: 20 }} >
                    <Image style={{ width: 20, height: 20, tintColor: Colors.colorBlack }} source={require('../../src/images/arrow-back.png')} />
                </Pressable>
            </View>
            <View style={{ paddingEnd: 20, paddingStart: 30, paddingTop: 30 }}>
                <Text style={{ fontSize: 24, alignSelf: 'center', fontFamily: 'ws_regular', color: Colors.colorBlack }} >Career Wizard</Text>
                <Text style={{ fontSize: 18, alignSelf: 'center', fontFamily: 'ws_semibold', color: Colors.colorBlack, marginTop: 20, textAlign: 'center' }} >What opportunities interest you?</Text>
            </View>
            <CardStack
                style={{ width: 350, height: 510, justifyContent: 'center', alignItems: 'center', alignSelf: 'center' }}
                renderNoMoreCards={() => <Text style={{ fontWeight: '700', fontSize: 18, color: 'gray' }}>No more cards :</Text>}
                ref={(swiper) => setSwiper(swiper)}
                disableBottomSwipe={true}
                disableTopSwipe={true}
                onSwipedLeft={(end) => setLeftId(end + 1)}
                onSwipedRight={(right) => {
                    setRightId(right + 1)
                    console.log(right + 1)
                }}
                onSwipedAll={hnadleSwipedAll}

            >
                <Card style={{ width: '100%', height: 500 }}><Image style={{ width: 300, height: 500 }} source={require('../../src/images/industry_business.png')} /></Card>
                <Card style={{ width: '100%', height: 500 }}><Image style={{ width: 300, height: 500 }} source={require('../../src/images/industry_engineering.png')} /></Card>
                <Card style={{ width: '100%', height: 500 }}><Image style={{ width: 300, height: 500 }} source={require('../../src/images/industry_service_retail_culinary.png')} /></Card>
                <Card style={{ width: '100%', height: 500 }}><Image style={{ width: 300, height: 500 }} source={require('../../src/images/industry_legal.png')} /></Card>
                <Card style={{ width: '100%', height: 500 }}><Image style={{ width: 300, height: 500 }} source={require('../../src/images/industry_education.png')} /></Card>
                <Card style={{ width: '100%', height: 500 }}><Image style={{ width: 300, height: 500 }} source={require('../../src/images/industry_arts.png')} /></Card>
                <Card style={{ width: '100%', height: 500 }}><Image style={{ width: 300, height: 500 }} source={require('../../src/images/industry_healthcare.png')} /></Card>
                <Card style={{ width: '100%', height: 500 }}><Image style={{ width: 300, height: 500 }} source={require('../../src/images/industry_construction.png')} /></Card>
                <Card style={{ width: '100%', height: 500 }}><Image style={{ width: 300, height: 500 }} source={require('../../src/images/industry_farming.png')} /></Card>
                <Card style={{ width: '100%', height: 500 }}><Image style={{ width: 300, height: 500 }} source={require('../../src/images/industry_manufacturing.png')} /></Card>
                <Card style={{ width: '100%', height: 500 }}><Image style={{ width: 300, height: 500 }} source={require('../../src/images/industry_transport_warehousing.png')} /></Card>
            </CardStack>
            <View style={{ position: 'absolute', bottom: 0, flexDirection: 'row', justifyContent: 'space-around', width: '100%', marginBottom: 10 }}>
                <Pressable style={{ width: 100, height: 100, borderRadius: 100, justifyContent: 'center', alignItems: 'center', marginStart: 30 }}
                    onPress={() => { end ? navigation.navigate('PersonalInfo') : swiper.swipeLeft() }} >
                    <Image style={{ width: 100, height: 100 }} source={require('../../src/images/unlike.png')} />
                </Pressable>
                <Pressable style={{ width: 100, height: 100, borderRadius: 100, justifyContent: 'center', alignItems: 'center', marginEnd: 30 }}
                    onPress={() => { end ? navigation.navigate('PersonalInfo') : swiper.swipeRight() }}>
                    <Image style={{ width: 100, height: 100 }} source={require('../../src/images/like.png')} />
                </Pressable>
            </View>
            <Modal
                animationType='fade'
                transparent={true}
                visible={showPoint}>
                <View style={{ flex: 1, backgroundColor: Colors.colorTransparentBlack }}>
                    <ImageBackground style={{ flex: 1, opacity: 0.8, backgroundColor: 'rgb(0, 0, 5, 5)' }} source={require('../../src/images/keep_watch.png')} />
                    <View style={{ width: '80%', height: '10%', flexDirection: 'row', backgroundColor: Colors.white, marginTop: 20, marginStart: 40, alignItems: 'center', borderRadius: 10, position: 'absolute', top: 0 }}>
                        <Image style={{ width: 40, height: 40, marginStart: 10 }} source={require('../../src/images/ic_point.png')} />
                        <View style={{ marginStart: 10 }}>
                            <Text style={{ fontSize: 20, textAlign: 'left', fontFamily: 'ws_semibold', color: Colors.accentMagenta }} >+ {numPoint} Points</Text>
                            <Text style={{ fontSize: 16, textAlign: 'left', fontFamily: 'ws_regular', color: Colors.accentMagenta }} >{plainText}</Text>
                        </View>
                    </View>
                </View>
            </Modal>
            <Modal
                animationType='fade'
                transparent={true}
                visible={showOpp}>
                <View style={{ flex: 1, backgroundColor: Colors.blue }}>
                    <View style={{ padding: 20, justifyContent: 'center', flex: 1, alignItems: 'center' }}>
                        <ActivityIndicator size='large' color={Colors.white} />
                        <Text style={{ fontSize: 16, alignSelf: 'center', fontFamily: 'ws_regular', color: Colors.white, textAlign: 'center', marginTop: 25 }} >Matching you with opportunities, it{'\n'}should only take a moment...</Text>
                    </View>
                </View>
            </Modal>
            {loading && <LoadingScreen />}
        </View>
    );
}

export default CardSwapingScreen;