import { View, Text, StyleSheet, Image, TouchableOpacity, ToastAndroid,ScrollView, ImageBackground, Pressable, Modal, FlatList, Platform } from 'react-native';
import CommanTextInput from '../../components/comman/CommanTextInput';
import { Colors } from '../../src/colors/Colors';
import CommanButton from '../../components/comman/CommanButton';
import { useState } from 'react';
import { isVaildEmail, isVaildPassword } from '../../src/utils/Utils';
import { CommanMessage, showWarningMessage } from '../../components/comman/CommanMassage';
import { useRoute } from '@react-navigation/native';
import LoadingScreen from '../LoadingScreens/LoadingScreen';
import BackgroundHideButton from '../../components/comman/BackgroundHideButton';
import DatePicker from 'react-native-date-picker';


function CareerWizerdHomeScreen({navigation}) {
    const route = useRoute();
    const [optionOne , setOptionOne] = useState(''); 
    const [optionTwo , setOptionTwo] = useState(''); 
    const [optionThree , setOptionThree] = useState(''); 
    const [loading , setLoading] = useState(true);
    const careerWizerdHome = route.params?.careerWizerdHome


    if (!careerWizerdHome) {
        return <LoadingScreen/>
    }

    return(
        <View style={{flex: 1, marginTop: Platform.OS === 'ios' ? 40 : 0}}>
            <View style={{padding: 20}}>
                <TouchableOpacity style={{width: 20 ,height: 20}} onPress={() => navigation.navigate('CareerWizerdMain')}>
                    <Image style={{width: 20 ,height: 20}} source={require('../../src/images/arrow-back.png')} />
                </TouchableOpacity>
            </View>
            <View style={{padding: 20}}>
                <Text style={{fontSize: 24, alignSelf: 'center', fontFamily: 'ws_regular',color: Colors.colorBlack}} >Career Wizard</Text>
                <Text style={{fontSize: 16, alignSelf: 'center', fontFamily: 'ws_regular',color: Colors.colorBlack, marginTop: 10}} >What kind of home would you like to own?</Text>
            </View>
            <View style={{flex: 1, padding: 30, justifyContent: 'center', alignItems: 'center'}}>
                <FlatList data={careerWizerdHome.details} renderItem={(itemData) => {
                    return (
                        <View style={{flex: 1, marginTop: 30}}>
                            <Pressable onPress={() => navigation.navigate('CareerWizerdSalaryGoal',{type : 'home', details : itemData.item , option : itemData.item.career_sub_label})}>
                                    <View style={{flexDirection: 'row', alignItems: 'center', width: '100%'}} key={itemData.item.career_id}>
                                        <Image style={{width: 110 ,height: 110, borderRadius: 100, overflow: 'hidden'}} source={{uri : itemData.item.career_image}} key={itemData.item.career_id}/>
                                        <View style={{marginStart: 5, alignItems: 'center'}}>
                                            <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between',width: 200}}>
                                                <Text style={{fontSize: 12,fontFamily: 'ws_regular',color: Colors.colorBlack, alignSelf: 'flex-start', textAlign: 'right'}} key={itemData.item.career_id} >{itemData.item.career_sub_label}</Text>
                                                <View style={{width: 25 ,height: 20, marginEnd: 10, transform: [{rotate: '180deg'}], alignSelf: 'flex-end'}}>
                                                    <Image style={{width: 25 ,height: 20, tintColor: Colors.accentMagenta}} source={require('../../src/images/arrow-back.png')} />
                                                </View>
                                            </View>
                                            <Text style={{fontSize: 16, fontFamily: 'ws_regular',color: Colors.colorBlack, marginTop: 10, textAlign: 'right', alignSelf: 'flex-start'}} key={itemData.item.career_id} >{itemData.item.career_label}</Text>
                                        </View>
                                    </View>
                            </Pressable>
                        </View>
                    );
                }} keyExtractor={(item , index) => index}/>
                {/* <TouchableOpacity style={{width: '100%',flexDirection: 'row', justifyContent: 'center', alignItems: 'center', marginTop: 20}}>
                    <Image style={{width: 110 ,height: 110, borderRadius: 100, overflow: 'hidden'}} source={require('../../src/images/profile_inactive.png')} />
                    <View style={{marginStart: 5, marginTop: 10, justifyContent: 'center', alignItems: 'center'}}>
                        <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                            <Text style={{width: '80%',fontSize: 12, textAlign: 'left', fontFamily: 'ws_regular',color: Colors.colorBlack}} >Career Wizard</Text>
                            <View style={{width: 25 ,height: 20, marginEnd: 10, transform: [{rotate: '180deg'}]}}>
                                <Image style={{width: 25 ,height: 20, tintColor: Colors.accentMagenta}} source={require('../../src/images/arrow-back.png')} />
                            </View>
                        </View>
                        <Text style={{fontSize: 16, textAlign: 'left', fontFamily: 'ws_regular',color: Colors.colorBlack, marginTop: 10}} >What kind of car would you like to own?</Text>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity style={{width: '100%',flexDirection: 'row', justifyContent: 'center', alignItems: 'center', marginTop: 20}}>
                    <Image style={{width: 110 ,height: 110, borderRadius: 100, overflow: 'hidden'}} source={require('../../src/images/profile_inactive.png')} />
                    <View style={{marginStart: 5, marginTop: 10, justifyContent: 'center', alignItems: 'center'}}>
                        <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                            <Text style={{width: '80%',fontSize: 12, textAlign: 'left', fontFamily: 'ws_regular',color: Colors.colorBlack}} >Career Wizard</Text>
                            <View style={{width: 25 ,height: 20, marginEnd: 10, transform: [{rotate: '180deg'}]}}>
                                <Image style={{width: 25 ,height: 20, tintColor: Colors.accentMagenta}} source={require('../../src/images/arrow-back.png')} />
                            </View>
                        </View>
                        <Text style={{fontSize: 16, textAlign: 'left', fontFamily: 'ws_regular',color: Colors.colorBlack, marginTop: 10}} >What kind of car would you like to own?</Text>
                    </View>
                </TouchableOpacity> */}
            </View>
            {/* {loading && <LoadingScreen/> } */}
        </View>
    );
}

export default CareerWizerdHomeScreen;