import { View, Text, StyleSheet, Image, TouchableOpacity, ToastAndroid,ScrollView, ImageBackground, Pressable, Modal , Platform} from 'react-native';
import CommanTextInput from '../../components/comman/CommanTextInput';
import { Colors } from '../../src/colors/Colors';
import CommanButton from '../../components/comman/CommanButton';
import { useState } from 'react';
import { isVaildEmail, isVaildPassword } from '../../src/utils/Utils';
import { CommanMessage, showWarningMessage } from '../../components/comman/CommanMassage';
import { useRoute } from '@react-navigation/native';
import BackgroundHideButton from '../../components/comman/BackgroundHideButton';
import { transformer } from '../../metro.config';

function CareerWizerdConfirmSalaryGoal({navigation}) {
    return(
        <View style={{flex: 1, backgroundColor: Colors.blue, marginTop: Platform.OS === 'ios' ? 40 : 0}}>
            <View style={{paddingEnd: 20, paddingStart: 20, paddingTop: 50}}>
                <TouchableOpacity style={{width: 20 ,height: 20, alignSelf: 'flex-end', marginEnd: 30, borderRadius: 20, borderWidth: 2, borderColor: Colors.white, justifyContent: 'center', alignItems: 'center'}}
                    onPress={() => navigation.navigate('CareerWizerdSalaryGoal')}>
                    <Image style={{width: 8 ,height: 8, tintColor: Colors.white}} source={require('../../src/images/close-icon.png')} />
                </TouchableOpacity>
            </View>
            <View style={{padding: 20, justifyContent: 'center', flex: 1}}>
                <Text style={{fontSize: 56, alignSelf: 'center', fontFamily: 'ws_semibold',color: Colors.white}} >$0</Text>
                <Text style={{fontSize: 16, alignSelf: 'center', fontFamily: 'ws_regular',color: Colors.white, textAlign: 'center', marginTop: 25}} >This is the pay that you’ll work{'\n'}towards, so think long term</Text>
                <View style={{marginEnd: 40, marginStart: 40, paddingTop: 10, marginTop: 25}}>
                    <CommanButton title={'Confirm Salary Goal'} width={'100%'} height={40} textColor={Colors.accentMagenta} backgroundColor={Colors.white} borderRadius={100}
                        />
                </View>
            </View>
        </View>
    );
}

export default CareerWizerdConfirmSalaryGoal;