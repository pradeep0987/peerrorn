import { View, Text, StyleSheet, Image, TouchableOpacity, ToastAndroid, ScrollView, ImageBackground, Pressable, Modal, Alert, PermissionsAndroid, Linking, Platform, Share } from 'react-native';
import CommanTextInput from '../../components/comman/CommanTextInput';
import { Colors } from '../../src/colors/Colors';
import CommanButton from '../../components/comman/CommanButton';
import { useState, useEffect, useRef } from 'react';
import { isVaildEmail, isVaildPassword } from '../../src/utils/Utils';
import CommanMessage from '../../components/comman/CommanMassage';
import { useRoute } from '@react-navigation/native';
import LoadingScreen from '../LoadingScreens/LoadingScreen';
import BackgroundHideButton from '../../components/comman/BackgroundHideButton';
import DatePicker from 'react-native-date-picker';
import * as ImagePicker from 'react-native-image-picker'
import AsyncStorage from '@react-native-async-storage/async-storage';
import TimeZone from 'react-native-timezone';
import { profileUpdate, uploadMedia } from '../../restapi/Services';

function CareerWizerdScreen({ navigation }) {
  const [name, setName] = useState('');
  const nameInputRef = useRef(null);
  const dateOfBirthInputRef = useRef(null);
  const numberInputRef = useRef(null);
  const [dateOfBirth, setDateOfBirth] = useState('');
  const [phoneNumber, setPhoneNumber] = useState('');
  const [modalShow, setModalShow] = useState(false);
  const [warning, setWarning] = useState(true);
  const [date, setDate] = useState(new Date());
  const [open, setOpen] = useState(false);
  const [imagePicker, setImagePicker] = useState(false);
  const [pickedImage, setPickedImage] = useState('');
  const [permissions, setPermissions] = useState(false);
  const [millis, setMillis] = useState();
  const [loading, setLoading] = useState(false);
  const [userId, setUserId] = useState();
  const [text, setText] = useState('ADD PHOTO');
  const [numPoint, setPoint] = useState(0);
  const [plainText, setPlainText] = useState('');
  const [afterGotot, setAfterGotit] = useState(false);

  async function onPressNextHandler() {
    if (name === '') {
      ToastAndroid.show('Please enter your name', ToastAndroid.SHORT);
    } else if (phoneNumber === '') {
      ToastAndroid.show('Please enter your phone number', ToastAndroid.SHORT);
    } else if (phoneNumber.length !== 12 || phoneNumber.length < 12) {
      ToastAndroid.show('Please enter valid phone number', ToastAndroid.SHORT);
    } else if (dateOfBirth === '') {
      ToastAndroid.show('Please enter your date of birth', ToastAndroid.SHORT);
    } else {
      const timeZone = await TimeZone.getTimeZone().then(zone => zone);
      console.log(timeZone);

      let data = JSON.stringify({
        "userdata": {
          "user_id": userId,
          "first_name": name,
          "phone": phoneNumber,
          "date_of_birth": millis,
          "timeZone": timeZone
        }
      });

      setLoading(true)

      const response = await profileUpdate(data);
      console.log(response)
      if (response.data != null) {
        setLoading(true)
        if (response.data.status) {
          setLoading(false)
          setModalShow(true)
          const user = response.data.userdata
          console.log(user)
          // userData(user)
          screenData()
          await AsyncStorage.setItem('first_name', name)
          await AsyncStorage.setItem('phone', phoneNumber)
          await AsyncStorage.setItem('date_of_birth', dateOfBirth)
          CommanMessage(response.data.message)
        } else {
          setLoading(false)
          CommanMessage(response.data.message)
        }
      } else {
        CommanMessage(response.data.message)
      }
    }
  }

  const userData = async (data) => {
    try {
      const userList = JSON.stringify(data)
      await AsyncStorage.setItem('userdata', userList)
    } catch (e) {
      console.log(e);
    }
  }

  function onChange(text) {
    const format = formatMobileNumber(text);
    setPhoneNumber(format);
  }

  function formatMobileNumber(text) {
    var cleaned = ("" + text).replace(/\D/g, "");
    var match = cleaned.match(/^(1|)?(\d{3})(\d{3})(\d{4})$/);
    if (match) {
      var intlCode = match[1] ? "+1 " : "",
        number = [intlCode, match[2], "-", match[3], "-", match[4]].join(
          ""
        );
      return number;
    }
    return text;
  }

  function getFormatedDate(date) {
    const monthNames = ["January", "February", "March", "April", "May", "June",
      "July", "August", "September", "October", "November", "December"
    ];
    const day = date.getDate()
    const month = date.getMonth()
    const year = date.getFullYear()
    const formateDay = (day < 10 ? '0' + day : day)
    const formateMonth = (month < 10 ? '0' + month : month)
    const formattedDate = monthNames[month] + " " + formateDay + "," + year

    const inputDateTime = new Date(year, date.getMonth(), day);
    const timeInMill = inputDateTime.getTime();
    setMillis(timeInMill)
    console.log('time in mill', timeInMill);

    setDateOfBirth(formattedDate)
  }

  useEffect(() => {
    async function handlePermission() {
      const permission = await requestCameraPermission();
      setPermissions(permission);
    }
    async function handleData() {
      const response = await getData();
      console.log(response)
      if (response != null) {
        setUserId(response.user_id)
      }
      const profile = await userProfile();
      if (profile != null || profile !== "") {
        setPickedImage(profile)
      }
    }
    handleData();
    handlePermission();
  }, [requestCameraPermission])

  const getData = async () => {
    try {
      const response = await AsyncStorage.getItem('userdata')
      return response != null ? JSON.parse(response) : null;
    } catch (e) {
      console.log(e)
    }
  }

  const requestCameraPermission = async () => {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.CAMERA,
        {
          title: "App Camera Permission",
          message: "App needs access to your camera ",
          buttonNeutral: "Ask Me Later",
          buttonNegative: "Cancel",
          buttonPositive: "OK"
        }
      );
      const grantedstorage = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
        {
          title: "App Camera Permission",
          message: "App needs access to your camera ",
          buttonNeutral: "Ask Me Later",
          buttonNegative: "Cancel",
          buttonPositive: "OK"
        }
      );
      if (grantedstorage === PermissionsAndroid.RESULTS.GRANTED && granted === PermissionsAndroid.RESULTS.GRANTED) {
        return true
      } else {
        console.log("Camera permission denied");
        return false
      }
    } catch (err) {
      console.warn(err);
      return false
    }
    return true
  };

  function handleCamera() {
    if (permissions) {
      selectFile()
      setImagePicker(false)
    } else {
      Alert.alert('Permission', 'Please grand permission', [{ text: 'Cancal' }, { text: 'Ok', onPress: handleOpenSetting }])
    }
  }

  function handleImagePicker() {
    if (permissions) {
      pickImageFile()
      setImagePicker(false)
    } else {
      Alert.alert('Permission', 'Please grand permission')
    }
  }

  function handleOpenSetting() {
    if (Platform.OS === 'ios') {
      Linking.openURL('app-settings:')
    } else {
      Linking.openSettings();
    }
  }

  const pickImageFile = () => {
    var options = {
      title: 'Select Image',
      customButtons: [
        {
          name: 'customOptionKey',
          title: 'Choose file from Custom Option'
        },
      ],
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };

    ImagePicker.launchImageLibrary(options, async (res) => {
      console.log('Response = ', res);
      if (res.didCancel) {
        console.log('User cancelled image picker');
      } else if (res.error) {
        console.log('ImagePicker Error: ', res.error);
      } else if (res.customButton) {
        console.log('User tapped custom button: ', res.customButton);
      } else {
        setPickedImage(res.assets[0].uri);
        storeProfiledata(res.assets[0].uri)

        const getdate = await getData();
        var myHeaders = new Headers();
        myHeaders.append('Content-Type', 'multipart/form-data;');
        myHeaders.append('session_token', `${getdate.session_token}`);


        let formData = new FormData();
        formData.append('user_id', getdate.user_id);
        formData.append('type', "profile");
        formData.append('id', getdate.user_id);

        formData.append('file', {
          uri: res.assets[0].uri,
          type: 'image/png',
          name: res.assets[0].fileName,
        });
        var url = 'https://dev.peerro.com/api_v13/profile/upload_media'

        var requestOptions = {
          url: url,
          method: 'post',
          headers: myHeaders,
          body: formData
        };


        fetch(url, requestOptions).then(response => response.json())
          .then(result => {
            setLoading(true)
            console.log('Resutl', result);
            if (result != null) {
              setLoading(true)
              if (result.status) {
                setLoading(true)
                if (result.show_point === 1) {
                  setLoading(false)
                  setAfterGotit(true)
                  setPoint(result.point)
                  setPlainText(result.point_text)
                  setTimeout(function () {
                    setAfterGotit(false)
                  }, 3000)
                }
                setText('CHANGE')
                CommanMessage(result.message)
              } else {
                setLoading(false)
                CommanMessage(result.message)
              }
            }
          })
          .catch(error => {
            Alert.alert('Image is not uploaded');
            console.log('error', error);
          });
      }
    });
  };

  const selectFile = () => {
    var options = {
      title: 'Select Image',
      customButtons: [
        {
          name: 'customOptionKey',
          title: 'Choose file from Custom Option'
        },
      ],
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };

    ImagePicker.launchCamera(options, async (res) => {
      console.log('Response = ', res);
      if (res.didCancel) {
        console.log('User cancelled image picker');
      } else if (res.error) {
        console.log('ImagePicker Error: ', res.error);
      } else if (res.customButton) {
        console.log('User tapped custom button: ', res.customButton);
      } else {
        setPickedImage(res.assets[0].uri);
        storeProfiledata(res.assets[0].uri)

        const getdate = await getData();

        var myHeaders = new Headers();
        myHeaders.append('Content-Type', 'multipart/form-data;');
        myHeaders.append('session_token', `${getdate.session_token}`);


        let formData = new FormData();
        formData.append('user_id', getdate.user_id);
        formData.append('type', "profile");
        formData.append('id', getdate.user_id);

        formData.append('file', {
          uri: res.assets[0].uri,
          type: 'image/png',
          name: res.assets[0].fileName,
        });
        var url = 'https://dev.peerro.com/api_v13/profile/upload_media'

        var requestOptions = {
          url: url,
          method: 'post',
          headers: myHeaders,
          body: formData
        };


        fetch(url, requestOptions).then(response => response.json())
          .then(result => {
            setLoading(true)
            if (result != null) {
              setLoading(true)
              if (result.status) {
                setLoading(true)
                if (result.show_point === 1) {
                  setLoading(false)
                  setAfterGotit(true)
                  setPoint(result.point)
                  setPlainText(result.point_text)
                  setTimeout(function () {
                    setAfterGotit(false)
                  }, 3000)
                }
                setText('CHANGE')
                CommanMessage(result.message)
              } else {
                setLoading(false)
                CommanMessage(result.message)
              }
            }
          })
          .catch(error => {
            Alert.alert('Image is not uploaded');
            console.log('error', error);
          });
      }
    });
  };

  function handlePicker() {
    setImagePicker(true)
  }

  const screenData = async () => {
    try {
      await AsyncStorage.setItem('screenState', '6')
    } catch (e) {
      console.log(e);
    }
  }

  function navigateToCareerWizerdMain() {
    setModalShow(false)
    navigation.navigate('CareerWizerdMain')
  }

  async function onShare() {
    try {
      const result = await Share.share({
        message:
          "Hey what’s up!\n" +
          "You should check out this app called Peerro. It’s great for finding new opportunities near you.\n",
      });

      if (result.action === Share.sharedAction) {
        if (result.activityType) {
          console.log('ready')
        } else {
          console.log('shared')
        }
      } else if (result.action === Share.dismissedAction) {
        console.log('dismiss')
      }
    } catch (error) {
      console.log(error)
    }
  };

  const storeProfiledata = async (data) => {
    try {
      const profileData = JSON.stringify(data)
      await AsyncStorage.setItem('profileUrl', profileData)
    } catch (e) {
      console.log(e);
    }
  }

  const userProfile = async () => {
    try {
      const response = await AsyncStorage.getItem('profileUrl')
      return response != null ? JSON.parse(response) : null;
    } catch (e) {
      console.log(e)
    }
  }

  return (
    <View style={{ flex: 1 , marginTop: Platform.OS === 'ios' ? 40 : 0}}>
      <ScrollView>
        <View style={{ padding: 20, marginTop: 50 }}>
          <Text style={{ fontSize: 24, alignSelf: 'center', fontFamily: 'ws_medium' }} >Complete Your Profile</Text>
          <Text style={{ fontSize: 16, alignSelf: 'center', marginTop: 15, textAlign: 'center', fontFamily: 'ws_regular' }} >This will help us match you with{'\n'}the next opportunities for you</Text>
          <View style={{ width: '100%', padding: 20, justifyContent: 'center' }}>
            <Text style={{ fontSize: 16, alignSelf: 'flex-start', marginTop: 15, color: Colors.blue, fontFamily: 'ws_medium' }} >Profile Picture</Text>
            <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 15 }}>
              <Image style={{ width: 150, height: 150, borderRadius: 100, overflow: 'hidden' }} source={pickedImage ? { uri: pickedImage } : require('../../src/images/profile_inactive.png')} />
              <CommanButton title={text} width={160} height={40} textColor={Colors.white} backgroundColor={Colors.accentMagenta} borderRadius={100} marginStart={5}
                onPress={handlePicker} />
            </View>
          </View>
        </View>
        <View style={{ flex: 1, paddingEnd: 40, paddingStart: 40 }}>
          <CommanTextInput
            inputRef={nameInputRef}
            title='Name'
            height={50}
            backgroundColor={Colors.white}
            marginTopInput={5}
            borderRadius={10}
            textColor={Colors.blue}
            OnChangeText={(name) => setName(name)}
            Value={name}
            paddingStart={15}
            onSubmitEditing={() => numberInputRef.current.focus()}
            returnKeyType={'next'}
          />
          <CommanTextInput
            inputRef={numberInputRef}
            title='Phone Number'
            height={50}
            backgroundColor={Colors.white}
            marginTopInput={5} borderRadius={10}
            textColor={Colors.blue}
            marginTop={20}
            OnChangeText={(number) => onChange(number)}
            Value={phoneNumber}
            placeholder={'###-###-####'}
            paddingStart={15}
            keyboardType={'numeric'}
            maxLength={12}
            onSubmitEditing={() => dateOfBirthInputRef.current.focus()}
            returnKeyType={'next'}
          />
          <CommanTextInput
            inputRef={dateOfBirthInputRef}
            title='Date Of Birth'
            height={50}
            backgroundColor={Colors.white}
            marginTopInput={5}
            borderRadius={10}
            textColor={Colors.blue}
            marginTop={20}
            OnChangeText={(dob) => setDateOfBirth(dob)}
            Value={dateOfBirth}
            placeholder={'Month ##,####'}
            paddingStart={15}
            editable={false}
            onPress={() => setOpen(true)}
            returnKeyType={'done'}
          />

          {open && <DatePicker date={date} mode='date' modal={true} open={open} onCancel={() => setOpen(false)} onDateChange={(date) => setDate(date)} onConfirm={(date) => {
            setOpen(false)
            getFormatedDate(date)
          }} />}

          <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
            <BackgroundHideButton title={'SKIP'} width={150} height={40} textColor={Colors.colorBlack} borderRadius={100} marginTop={20} borderColor={Colors.accentMagenta} borderWidth={1} onPress={() => {
              navigation.navigate('CareerWizerdMain')
              screenData()
            }} />
            <CommanButton title={'Save'} width={150} height={40} textColor={Colors.white} backgroundColor={Colors.accentMagenta} borderRadius={100} marginTop={20} onPress={onPressNextHandler} />
          </View>
        </View>
      </ScrollView>
      <Modal
        animationType='fade'
        transparent={true}
        visible={imagePicker}
        style={{ borderRadius: 10, justifyContent: 'center', alignItems: 'center' }}>
        <View style={{ flex: 1, backgroundColor: Colors.colorTransparentBlack, alignItems: 'center', justifyContent: 'center' }}>
          <View style={{ width: '80%', height: 220, justifyContent: 'center', backgroundColor: Colors.white, alignSelf: 'center' }}>
            <View style={{ flex: 1, justifyContent: 'center', paddingEnd: 20, paddingStart: 20, paddingBottom: 10 }}>
              <Text style={{ fontSize: 20, alignSelf: 'flex-start', fontWeight: '600', fontFamily: 'ws_medium', color: Colors.colorBlack }}>Upload Image</Text>
              <TouchableOpacity style={{ marginTop: 30 }} onPress={handleCamera} >
                <Text style={{ fontSize: 16, alignSelf: 'flex-start', fontWeight: '600', fontFamily: 'ws_regular', color: Colors.colorBlack }} >Take Photo</Text>
              </TouchableOpacity>
              <TouchableOpacity style={{ marginTop: 30 }} onPress={handleImagePicker}>
                <Text style={{ fontSize: 16, alignSelf: 'flex-start', fontWeight: '600', fontFamily: 'ws_regular', color: Colors.colorBlack }} >Choose from Library</Text>
              </TouchableOpacity>
              <TouchableOpacity style={{ marginTop: 30 }} onPress={() => setImagePicker(false)}>
                <Text style={{ fontSize: 16, alignSelf: 'flex-start', fontWeight: '600', fontFamily: 'ws_regular', color: Colors.colorBlack }} >Cancel</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </Modal>
      <Modal
        animationType='fade'
        transparent={true}
        visible={modalShow}
        style={{ borderRadius: 10 }}
      >
        <View style={{ flex: 1, backgroundColor: Colors.colorTransparentBlack, alignItems: 'center', justifyContent: 'center' }}>
          <View style={{ width: '80%', height: '80%', backgroundColor: Colors.white, borderRadius: 20, elevation: 10 }}>
            <View style={{ padding: 20, justifyContent: 'center' }}>
              <Pressable style={{ width: 20, height: 20, alignSelf: 'flex-end', marginTop: 10, marginStart: 20 }} onPress={navigateToCareerWizerdMain}>
                <Image style={{ width: 20, height: 20 }} source={require('../../src/images/icon_cross.png')} />
              </Pressable>
              <Text style={{ fontSize: 24, alignSelf: 'center', marginTop: 20, marginEnd: 50, marginStart: 50, textAlign: 'center', color: Colors.accentMagenta, fontFamily: 'ws_semibold' }} >+ 10 Points</Text>
              <Image style={{ width: 150, height: 150, alignSelf: 'center', marginTop: 10 }} source={require('../../src/images/gif_point_hand.gif')} />
              <Text style={{ fontSize: 20, alignSelf: 'center', marginTop: 20, marginEnd: 50, marginStart: 50, textAlign: 'center', fontFamily: 'ws_regular', color: Colors.colorBlack }} >You earned{'\n'}10 point for{'\n'}complrting your{'\n'}profile</Text>
              <Text style={{ fontSize: 16, alignSelf: 'center', marginTop: 20, marginEnd: 50, marginStart: 50, textAlign: 'center', fontFamily: 'ws_regular', color: Colors.colorBlack }} >Asesome work{'\n'}racking up those{'\n'}points</Text>
              <View style={{ justifyContent: 'center', padding: 20, borderWidth: 2, borderColor: Colors.blue, borderRadius: 15, marginTop: 20 }}>
                <Text style={{ fontSize: 20, alignSelf: 'center', textAlign: 'center', fontFamily: 'ws_regular', color: Colors.colorBlack }} >Invite a friend to earn 25{'\n'}points!</Text>
                <CommanButton title={'INVITE FRIENDS'} width={'100%'} height={50} textColor={Colors.white} backgroundColor={Colors.accentMagenta} borderRadius={100} marginTop={10}
                  onPress={onShare} />
              </View>
            </View>
          </View>
        </View>
      </Modal>
      <Modal
        animationType='fade'
        transparent={true}
        visible={afterGotot}>
        <View style={{ flex: 1, backgroundColor: Colors.colorTransparentBlack }}>
          <ImageBackground style={{ flex: 1, opacity: 0.8, backgroundColor: 'rgb(0, 0, 5, 5)' }} source={require('../../src/images/keep_watch.png')} />
          <View style={{ width: '80%', height: '10%', flexDirection: 'row', backgroundColor: Colors.white, marginTop: 20, marginStart: 40, alignItems: 'center', borderRadius: 10, position: 'absolute', top: 0 }}>
            <Image style={{ width: 40, height: 40, marginStart: 10 }} source={require('../../src/images/ic_point.png')} />
            <View style={{ marginStart: 10 }}>
              <Text style={{ fontSize: 18, textAlign: 'left', fontFamily: 'ws_semibold', color: Colors.accentMagenta }} >+ {numPoint} Points</Text>
              <Text style={{ fontSize: 14, textAlign: 'left', fontFamily: 'ws_regular', color: Colors.accentMagenta }} >{plainText}</Text>
            </View>
          </View>
        </View>
      </Modal>
      {loading && <LoadingScreen />}
    </View>
  );
}

export default CareerWizerdScreen;