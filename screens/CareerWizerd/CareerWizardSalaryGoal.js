import { View, Text, StyleSheet, Image, TouchableOpacity, ToastAndroid, ScrollView, ImageBackground, Pressable, Modal, Platform } from 'react-native';
import CommanTextInput from '../../components/comman/CommanTextInput';
import { Colors } from '../../src/colors/Colors';
import CommanButton from '../../components/comman/CommanButton';
import { useState, useCallback, useEffect } from 'react';
import { isVaildEmail, isVaildPassword } from '../../src/utils/Utils';
import { CommanMessage, showWarningMessage } from '../../components/comman/CommanMassage';
import { useRoute } from '@react-navigation/native';
import BackgroundHideButton from '../../components/comman/BackgroundHideButton';
import RangeSlider from 'rn-range-slider';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { updateProfileSalary } from '../../restapi/Services';
import LoadingScreen from '../LoadingScreens/LoadingScreen';


function CareerWizerdSalaryGoalScreen({ navigation }) {
    const route = useRoute();
    const [confirm, setConfirm] = useState(false);
    const [low, setLow] = useState();
    const [high, setHigh] = useState();
    const [pay, setPay] = useState("");
    const [tipLabel, setTiplabel] = useState('');
    const [selectedLabel, setSelectedLabel] = useState('');
    const [hourlyRate, setHourlyRate] = useState('');
    const [salaryYearly, setSalaryYearly] = useState('');
    const [progressBar, setProgressBar] = useState('');
    const [minRange, setMinRange] = useState(0);
    const [loading, setLoading] = useState(false);
    const type = route.params?.type
    const details = route.params?.details
    const option = route.params?.option

    function handleConfirm() {
        if (confirm) {
            setConfirm(false);
        } else {
            setConfirm(true);
        }
    }

    async function navigatiteToCardView() {
        setLoading(true)
        const user = await getData();
        let data = JSON.stringify({
            "userdata": {
                "user_id": user.user_id,
                "career_id": details.career_id,
                "salary_text": `${pay}000`
            }
        });
        const response = await updateProfileSalary(data);
        console.log(response)
        if (response.data != null) {
            setLoading(true)
            if (response.data.status) {
                setTimeout(function () {
                    setConfirm(false)
                    storeScreenState()
                    navigation.navigate('CardView')
                    setLoading(false)
                }, 3000)
            }
        }
    }

    const storeScreenState = async () => {
        try {
            await AsyncStorage.setItem('screenState', '8')
        } catch (e) {
            console.log(e);
        }
    }

    useEffect(() => {
        handleOptionsTypes()
    }, [handleOptionsTypes])

    async function handleOptionsTypes() {
        console.log(option)
        console.log(type)
        console.log(details)
        if (type === 'car') {
            setTiplabel('Budget no more than 20% of your income toward your car.')
            setSelectedLabel("This is what you will need to earn\n" + "to afford the car you selected:")

            if (option === 'Option 1') {
                setHourlyRate("$15")
                setSalaryYearly("$30k")
                const barData = JSON.stringify({
                    hourlyRate: "$15",
                    progressBar: "30",
                    SalaryYearly: "$30k"
                });
                await AsyncStorage.setItem("progressBar", barData);
            } else if (option === 'Option 2') {
                setHourlyRate("$28")
                setSalaryYearly("$58k")
                const barData = JSON.stringify({
                    hourlyRate: "$28",
                    progressBar: "58",
                    SalaryYearly: "$58k"
                });
                await AsyncStorage.setItem("progressBar", barData);
            } else if (option === 'Option 3') {
                setHourlyRate("$42")
                setSalaryYearly("$86k")
                const barData = JSON.stringify({
                    hourlyRate: "$42",
                    progressBar: "86",
                    SalaryYearly: "$58k"
                });
                await AsyncStorage.setItem("progressBar", barData);
            }
        } else if (type === 'home') {
            setTiplabel("Budget no more than 28% of your income toward your home.")
            setSelectedLabel("This is what you will need to earn\n" + "to afford the home you selected:")

            if (option === 'Option 1') {
                setHourlyRate("$15")
                setSalaryYearly("$30k")
                const barData = JSON.stringify({
                    hourlyRate: "$15",
                    progressBar: "30",
                    SalaryYearly: "$58k"
                });
                await AsyncStorage.setItem("progressBar", barData);
            } else if (option === 'Option 2') {
                setHourlyRate("$28")
                setSalaryYearly("$58k")
                const barData = JSON.stringify({
                    hourlyRate: "$28",
                    progressBar: "58",
                    SalaryYearly: "$58k"
                });
                await AsyncStorage.setItem("progressBar", barData);
            } else if (option === 'Option 3') {
                setHourlyRate("$42")
                setSalaryYearly("$86k")
                const barData = JSON.stringify({
                    hourlyRate: "$42",
                    progressBar: "86",
                    SalaryYearly: "$58k"
                });
                await AsyncStorage.setItem("progressBar", barData);
            }
        } else if (type === 'family') {
            setTiplabel("Budget no more than 10% of your income toward your family member assistance & support.")
            setSelectedLabel("This is what you will need to earn\n" + "to afford the family member assistance & support you selected:")

            if (option === 'Option 1') {
                setHourlyRate("$15")
                setSalaryYearly("$30k")
                const barData = JSON.stringify({
                    hourlyRate: "$15",
                    progressBar: "30",
                    SalaryYearly: "$58k"
                });
                await AsyncStorage.setItem("progressBar", barData);
            } else if (option === 'Option 2') {
                setHourlyRate("$28")
                setSalaryYearly("$58k")
                const barData = JSON.stringify({
                    hourlyRate: "$28",
                    progressBar: "58",
                    SalaryYearly: "$58k"
                });
                await AsyncStorage.setItem("progressBar", barData);
            } else if (option === 'Option 3') {
                setHourlyRate("$42")
                setSalaryYearly("$86k")
                const barData = JSON.stringify({
                    hourlyRate: "$42",
                    progressBar: "86",
                    SalaryYearly: "$86k"
                });
                await AsyncStorage.setItem("progressBar", barData);
            }
        }

        const data = await getProogressBarData();
        setProgressBar(data.progressBar)
    }

    const getData = async () => {
        try {
            const response = await AsyncStorage.getItem('userdata')
            return response != null ? JSON.parse(response) : null;
        } catch (e) {
            console.log(e)
        }
    }


    const getProogressBarData = async () => {
        try {
            const response = await AsyncStorage.getItem('progressBar')
            return response != null ? JSON.parse(response) : null;
        } catch (e) {
            console.log(e)
        }
    }

    const renderThumb = useCallback(() => <View style={{ width: 15, height: 15, backgroundColor: Colors.white, borderRadius: 100, borderColor: Colors.blue, borderWidth: 1.5 }}></View>, []);
    const renderRail = useCallback(() => <View style={{ width: '100%', height: 6, backgroundColor: "#f0f0f0" }}></View>, []);
    const renderRailSelected = useCallback(() => <View style={{ width: '100%', height: 6, backgroundColor: Colors.blue }}></View>, []);
    const renderLabel = useCallback(value => (
        <View style={{ marginBottoma: 30, backgroundColor: Colors.blue, width: 80, alignItems: 'center', height: 35, justifyContent: 'center' }}>
            <Text style={{ textAlign: 'left', color: Colors.white, fontFamily: 'ws_semibold', fontWeight: '600', fontSize: 20 }}>${value}k</Text>
        </View>), []);
    const renderNotch = useCallback(() => <View style={{
        width: 0,
        height: 0,
        backgroundColor: "transparent",
        borderStyle: "solid",
        borderLeftWidth: 5,
        borderRightWidth: 5,
        borderBottomWidth: 10,
        borderLeftColor: "transparent",
        borderRightColor: "transparent",
        borderBottomColor: Colors.blue,
        transform: [{ rotate: '180deg' }]
    }}></View>, []);
    const handleValueChange = useCallback((low, high, pay) => {
        setPay(low)
        console.log(low, high, pay)
    }, []);



    return (
        <View style={{ flex: 1, marginTop: Platform.OS === 'ios' ? 40 : 0 }}>
            <View style={{ padding: 20 }}>
                <TouchableOpacity style={{ width: 20, height: 20 }} onPress={() => navigation.navigate('CareerWizerdMain')}>
                    <Image style={{ width: 20, height: 20 }} source={require('../../src/images/arrow-back.png')} />
                </TouchableOpacity>
            </View>
            <View style={{ padding: 20 }}>
                <Text style={{ fontSize: 24, alignSelf: 'center', fontFamily: 'ws_regular', color: Colors.colorBlack }} >Career Wizard</Text>
            </View>
            <View style={{ backgroundColor: '#D8F1FF', marginEnd: 20, marginStart: 20, padding: 10, borderRadius: 10 }} >
                <View style={{ backgroundColor: '#4396C3', width: 30, borderRadius: 10, justifyContent: 'center', alignItems: 'center', }}>
                    <Text style={{ color: Colors.white, fontFamily: 'ws_regular', fontSize: 10 }}>TIP</Text>
                </View>
                <Text style={{ marginTop: 10, color: Colors.colorBlack, fontFamily: 'ws_regular', marginStart: 5, fontSize: 16 }}>{tipLabel}</Text>
            </View>
            <View style={{ marginTop: 20, marginEnd: 20, marginStart: 20 }}>
                <Text style={{ color: Colors.colorBlack, fontFamily: 'ws_medium', marginStart: 5, textAlign: 'center', fontSize: 16, fontWeight: '700' }}>{selectedLabel}</Text>
            </View>
            <View style={{ flexDirection: 'row', marginTop: 15, borderColor: Colors.blue, borderRadius: 10, borderWidth: 1, padding: 10, marginEnd: 20, marginStart: 20, alignItems: 'center', height: 60 }}>
                <Image style={{ width: 40, height: 40, }} source={details.career_image ? { uri: details.career_image } : require('../../src/images/profile_inactive.png')} />
                <Text style={{ fontSize: 16, textAlign: 'center', fontFamily: 'ws_regular', color: Colors.colorBlack, marginStart: 5 }} >{details.career_label}</Text>
            </View>
            <View style={{ flexDirection: 'row', marginTop: 10, padding: 10, marginEnd: 20, marginStart: 20, alignItems: 'center', justifyContent: "space-around" }}>
                <View style={{ backgroundColor: Colors.blue, width: 100, height: 100, borderRadius: 100, justifyContent: 'center', alignItems: 'center' }}>
                    <Text style={{ fontSize: 25, textAlign: 'center', fontFamily: 'ws_medium', color: Colors.white }} >{hourlyRate}</Text>
                    <Text style={{ fontSize: 20, textAlign: 'center', fontFamily: 'ws_regular', color: Colors.white, marginTop: 5 }} >hour</Text>
                </View>
                <View>
                    <View style={{ width: 4, height: 100, backgroundColor: Colors.blue, transform: [{ rotate: '20deg' }] }} />
                </View>
                <View style={{ backgroundColor: Colors.blue, width: 100, height: 100, borderRadius: 100, justifyContent: 'center' }}>
                    <Text style={{ fontSize: 25, textAlign: 'center', fontFamily: 'ws_medium', color: Colors.white }} >{salaryYearly}</Text>
                    <Text style={{ fontSize: 20, textAlign: 'center', fontFamily: 'ws_regular', color: Colors.white, marginTop: 5 }} >salary</Text>
                </View>
            </View>
            <View style={{ marginEnd: 20, marginStart: 20, paddingTop: 10, backgroundColor: Colors.white, borderRadius: 10, marginTop: 20, paddingBottom: 20 }}>
                <Text style={{ fontSize: 16, textAlign: 'center', fontFamily: 'ws_semibold', color: Colors.colorBlack }} >Adjust your target:</Text>
                <View style={{paddingStart: 15 , paddingEnd: 15}}>
                    <RangeSlider
                        style={{ width: '100%', height: 80, marginTop: 20 }}
                        gravity={'center'}
                        min={0}
                        max={200}
                        step={1}
                        disableRange={true}
                        floatingLabel={false}
                        renderThumb={renderThumb}
                        renderRail={renderRail}
                        renderLabel={renderLabel}
                        renderNotch={renderNotch}
                        onValueChanged={handleValueChange}
                        renderRailSelected={renderRailSelected}

                    />
                </View>
            </View>
            <View style={{ marginEnd: 20, marginStart: 20, paddingTop: 10, marginTop: 20 }}>
                <CommanButton title={'Set Salary Goal'} width={'100%'} height={50} textColor={Colors.white} backgroundColor={Colors.accentMagenta} borderRadius={100} marginTop={10}
                    onPress={handleConfirm} />
            </View>
            <Modal
                animationType='fade'
                transparent={true}
                visible={confirm}>
                <View style={{ flex: 1, backgroundColor: Colors.blue, marginTop: Platform.OS === 'ios' ? 40 : 0 }}>
                    <View style={{ paddingEnd: 20, paddingStart: 20, paddingTop: 50 }}>
                        <TouchableOpacity style={{ width: 20, height: 20, alignSelf: 'flex-end', marginEnd: 30, borderRadius: 20, borderWidth: 2, borderColor: Colors.white, justifyContent: 'center', alignItems: 'center' }}
                            onPress={handleConfirm}>
                            <Image style={{ width: 8, height: 8, tintColor: Colors.white }} source={require('../../src/images/close-icon.png')} />
                        </TouchableOpacity>
                    </View>
                    <View style={{ padding: 20, justifyContent: 'center', flex: 1 }}>
                        <Text style={{ fontSize: 56, alignSelf: 'center', fontFamily: 'ws_semibold', color: Colors.white }} >${pay}k</Text>
                        <Text style={{ fontSize: 16, alignSelf: 'center', fontFamily: 'ws_regular', color: Colors.white, textAlign: 'center', marginTop: 25 }} >This is the pay that you’ll work{'\n'}towards, so think long term</Text>
                        <View style={{ marginEnd: 20, marginStart: 20, paddingTop: 10, marginTop: 25 }}>
                            <CommanButton title={'Confirm Salary Goal'} width={'100%'} height={40} textColor={Colors.accentMagenta} backgroundColor={Colors.white} borderRadius={100}
                                onPress={navigatiteToCardView} />
                        </View>
                    </View>
                </View>
            </Modal>
            {loading && <LoadingScreen />}
        </View>
    );
}

export default CareerWizerdSalaryGoalScreen;