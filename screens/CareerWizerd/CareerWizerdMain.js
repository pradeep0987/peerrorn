import { View, Text, StyleSheet, Image, TouchableOpacity, ToastAndroid, ScrollView, ImageBackground, Pressable, Modal, Alert, Platform } from 'react-native';
import CommanTextInput from '../../components/comman/CommanTextInput';
import { Colors } from '../../src/colors/Colors';
import CommanButton from '../../components/comman/CommanButton';
import { useState, useEffect } from 'react';
import { isVaildEmail, isVaildPassword } from '../../src/utils/Utils';
import { CommanMessage, showWarningMessage } from '../../components/comman/CommanMassage';
import { useRoute } from '@react-navigation/native';
import LoadingScreen from '../LoadingScreens/LoadingScreen';
import BackgroundHideButton from '../../components/comman/BackgroundHideButton';
import DatePicker from 'react-native-date-picker';
import NetInfo from '@react-native-community/netinfo';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { getCareerWizerd } from '../../restapi/Services';

function CareerWizerdMainScreen({ navigation }) {
    const [careerWizerdCar, setCareerWizerdCar] = useState('');
    const [careerWizerdHome, setCareerWizerdHome] = useState('');
    const [careerWizerdFamily, setCareerWizerdFamily] = useState('');
    const [loading, setLoading] = useState(true);

    useEffect(() => {
        handler();
    }, [])

    async function handler() {
        const response = await getCareerWizerd();
        setLoading(true)
        if (response != null) {
            setLoading(true)
            if (response.data != null) {
                setLoading(true)
                if (response.data.status) {
                    setLoading(false)
                    storeCareerWizerd(response.data.career_wizard)
                    setCareerWizerdCar(response.data.career_wizard[0])
                    setCareerWizerdHome(response.data.career_wizard[1])
                    setCareerWizerdFamily(response.data.career_wizard[2])
                }
            }
        } else {
            ToastAndroid.show('Somthing went wrong', ToastAndroid.SHORT);
        }
    }

    const storeCareerWizerd = async (data) => {
        try {
            const careerwizerd = JSON.stringify(data)
            await AsyncStorage.setItem('careerwizerd', careerwizerd)
        } catch (e) {
            console.log(e);
        }
    }

    return (
        <View style={{ flex: 1, backgroundColor: Colors.white , marginTop: Platform.OS === 'ios' ? 40 : 0}}>
            <View style={{ padding: 20 }}>
                {/* <TouchableOpacity style={{width: 20 ,height: 20}} onPress={() => navigation.navigate('SignUp')}>
                    <Image style={{width: 20 ,height: 20}} source={require('../../src/images/arrow-back.png')} />
                </TouchableOpacity> */}
            </View>
            <View style={{ padding: 20 }}>
                <Text style={{ fontSize: 24, alignSelf: 'center', fontFamily: 'ws_regular', color: Colors.colorBlack }} >Career Wizard</Text>
                <Text style={{ fontSize: 16, alignSelf: 'center', fontFamily: 'ws_regular', color: Colors.colorBlack, marginTop: 10, textAlign: 'center' }} >Let’s figure out your target salary goal.{'\n'}What are you interested in using your {'\n'}money for?</Text>
            </View>
            <View style={{ marginEnd: 40, marginStart: 40 }}>
                <Pressable style={{ width: '100%', flexDirection: 'row', justifyContent: 'center', alignItems: 'center', marginTop: 50, borderColor: Colors.blue, borderRadius: 10, borderWidth: 1, padding: 15 }}
                    onPress={() => navigation.navigate('CareerWizerdCar', { careerWizerdCar: careerWizerdCar })}>
                    <Text style={{ width: '80%', fontSize: 24, textAlign: 'left', fontFamily: 'ws_regular', color: Colors.colorBlack }} >Buy a Car</Text>
                    <View style={{ width: 25, height: 20, transform: [{ rotate: '180deg' }] }}>
                        <Image style={{ width: 25, height: 20, tintColor: Colors.accentMagenta }} source={require('../../src/images/arrow-back.png')} />
                    </View>
                </Pressable>
                <Pressable style={{ width: '100%', flexDirection: 'row', justifyContent: 'center', alignItems: 'center', marginTop: 15, borderColor: Colors.blue, borderRadius: 10, borderWidth: 1, padding: 15 }}
                    onPress={() => navigation.navigate('CareerWizerdHome', { careerWizerdHome: careerWizerdHome })}>
                    <Text style={{ width: '80%', fontSize: 24, textAlign: 'left', fontFamily: 'ws_regular', color: Colors.colorBlack }} >Buy a Home</Text>
                    <View style={{ width: 25, height: 20, transform: [{ rotate: '180deg' }] }}>
                        <Image style={{ width: 25, height: 20, tintColor: Colors.accentMagenta }} source={require('../../src/images/arrow-back.png')} />
                    </View>
                </Pressable>
                <Pressable style={{ width: '100%', flexDirection: 'row', justifyContent: 'center', alignItems: 'center', marginTop: 15, borderColor: Colors.blue, borderRadius: 10, borderWidth: 1, padding: 15 }}
                    onPress={() => navigation.navigate('CareerWizerdFamily', { careerWizerdFamily: careerWizerdFamily })}>
                    <Text style={{ width: '80%', fontSize: 24, textAlign: 'left', fontFamily: 'ws_regular', color: Colors.colorBlack }} >Family Member Assistance and{'\n'}Support</Text>
                    <View style={{ width: 25, height: 20, transform: [{ rotate: '180deg' }] }}>
                        <Image style={{ width: 25, height: 20, tintColor: Colors.accentMagenta }} source={require('../../src/images/arrow-back.png')} />
                    </View>
                </Pressable>
            </View>
            {loading && <LoadingScreen />}
        </View>
    );
}

export default CareerWizerdMainScreen;