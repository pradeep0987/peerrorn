import { View, Text, StyleSheet, Image, TouchableOpacity, ToastAndroid, ScrollView, ImageBackground, Pressable, Modal, Platform } from 'react-native';
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import AllJonsScreen from "./dashBoardScreens/AllJobsScreen";
import ProfileScreen from "./dashBoardScreens/ProfileScreen";
import MoreScreen from "./dashBoardScreens/MoreScreen";
import { Colors } from "../../src/colors/Colors";
import CustomBottomBar from './CustomBottomBar';


const BottomTab = createBottomTabNavigator();

function DashBoardScreen() {
    return (
        <View style={{flex: 1, marginTop: Platform.OS === 'ios' ? 40 : 0}}>
            <BottomTab.Navigator initialRouteName='AllJobs' screenOptions={{ headerShown: false, tabBarHideOnKeyboard: true, tabBarStyle: { borderTopWidth: 1, borderTopColor: Colors.colorGrey, height: 60 } }} tabBar={props => <CustomBottomBar {...props} />} >
                <BottomTab.Screen name="AllJobs" component={AllJonsScreen} />
                <BottomTab.Screen name="Profile" component={ProfileScreen} />
                <BottomTab.Screen name="More" component={MoreScreen} />
            </BottomTab.Navigator>
        </View>
    );
}

export default DashBoardScreen;