import { View, Text, StyleSheet, Image, TouchableOpacity, ToastAndroid, ImageBackground, Pressable } from 'react-native';
import { Colors } from '../../src/colors/Colors';
import { useState } from 'react';
import AsyncStorage from '@react-native-async-storage/async-storage';

function CustomBottomBar({ state, descriptors, navigation, position }) {
    const [allJobs, setAllJobs] = useState(true);
    const [profile, setSetProfile] = useState(false);
    const [more, setMpre] = useState(false);

    function allJobsScreenHandler() {
        setAllJobs(true)
        setSetProfile(false)
        setMpre(false)
    }

    async function profileScreenHandler() {
        setSetProfile(true)
        setAllJobs(false)
        setMpre(false)
    }

    async function moreScreenHandler() {
        setMpre(true)
        setSetProfile(false)
        setAllJobs(false)
    }
    return (
        <View style={{ backgroundColor: Colors.white, borderTopColor: Colors.colorlightGrey, borderTopWidth: 1, flexDirection: 'row', justifyContent: 'space-around', alignItems: 'center', height: 65 }}>
            <TouchableOpacity onPress={() => {
                allJobsScreenHandler()
                navigation.navigate('AllJobs')
            }} style={{ justifyContent: 'center', alignItems: 'center' }}>
                <Image style={{ width: 30, height: 30, tintColor: allJobs ? Colors.blue : Colors.colorCGrey }} source={require('../../src/images/all_oppty_inactive.png')} />
                <Text style={{ textAlign: 'center', color: Colors.blue, fontFamily: 'ws_regular', fontSize: 12, marginTop: 4 }}>All Jobs</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => {
                profileScreenHandler()
                navigation.navigate('Profile')
            }} style={{ justifyContent: 'center', alignItems: 'center' }}>
                <Image style={{ width: 30, height: 30, tintColor: profile ? Colors.blue : Colors.colorCGrey }} source={require('../../src/images/profile_inactive.png')} />
                <Text style={{ textAlign: 'center', color: Colors.blue, fontFamily: 'ws_regular', fontSize: 12, marginTop: 4 }}>Profile</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => {
                moreScreenHandler()
                navigation.navigate('More')
            }} style={{ justifyContent: 'center', alignItems: 'center' }}>
                <Image style={{ width: 7, height: 35, tintColor: more ? Colors.blue : Colors.colorCGrey }} source={require('../../src/images/profile_dot.png')} />
                <Text style={{ textAlign: 'center', color: Colors.blue, fontFamily: 'ws_regular', fontSize: 12 }}>More</Text>
            </TouchableOpacity>
        </View>
    );
}

export default CustomBottomBar;