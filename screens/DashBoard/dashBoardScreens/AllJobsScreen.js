import { View, Text, StyleSheet, Image, TouchableOpacity, ToastAndroid, ImageBackground, Pressable, Modal, BackHandler, AppState } from 'react-native';
import { Colors } from '../../../src/colors/Colors';
import { useState, useEffect } from 'react';
import CommanTextInput from '../../../components/comman/CommanTextInput';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import JobsScreen from './JobAndTraning/JobsScreen';
import TraningScreen from './JobAndTraning/TraningScreen';
import MyTabBar from './JobAndTraning/CustomTabBar';
import FilterScreen from '../../Filters/FiltersScreen';
import AsyncStorage from '@react-native-async-storage/async-storage';

const TopBar = createMaterialTopTabNavigator();

function AllJonsScreen({ navigation }) {
    const [search, setSearch] = useState(true);
    const [searchText, setSearchText] = useState('');

    function handleSearch() {
        if (search) {
            setSearch(false)
        } else {
            setSearch(true)
        }
    }

    useEffect(() => {
        const backHandler = BackHandler.addEventListener(
            'hardwareBackPress',
            backAction,
        );

        return () => backHandler.remove();
    })

    async function backAction() {
        const filterData = JSON.stringify({
            salary: 0 ,
            distence: 50 ,
            partTime: 1 ,
            fullTime: 2 ,
            seasonal: 4 ,
        })
        await AsyncStorage.setItem('filterData', filterData);
        BackHandler.exitApp();
    }

    function handleSubmiteSearch(event) {
        // const searchText = event.nativeEvent.text
        navigation.navigate('JOBS', { searchText: searchText })
    }

    function handleClearSearch() {
        setSearchText('')
        navigation.navigate('JOBS', { searchText: "" })
    }

    function handleSearchTextChange(event) {
        // navigation.navigate('JOBS', { searchText: text })
    }

    return (
        <View style={{ flex: 1 }}>
            <View style={{ width: '100%', flexDirection: 'row', justifyContent: 'space-between', paddingStart: 20, paddingEnd: 20, paddingTop: 20 }}>
                <Text style={{ width: 100, fontSize: 24, fontWeight: '800', color: Colors.colorBlack, fontFamily: 'ws_semibold' }} >All Jobs</Text>
                <TouchableOpacity style={{ width: 24, height: 24, justifyContent: 'center', alignItems: 'center' }} onPress={handleSearch}>
                    <Image style={{ width: search ? 24 : 15, height: search ? 24 : 15, marginEnd: search ? 5 : 8, marginTop: search ? 5 : 8 }} source={search ? require('../../../src/images/search-icon.png') : require('../../../src/images/close-icon.png')} />
                </TouchableOpacity>
            </View>
            {!search && <View style={{ paddingStart: 20, paddingEnd: 20 }}>
                <CommanTextInput
                    height={50}
                    backgroundColor={Colors.white}
                    borderRadius={10}
                    textColor={Colors.blue}
                    placeholder={'Search'}
                    paddingStart={44}
                    Value={searchText}
                    onChange={handleSearchTextChange}
                    OnChangeText={(text) => setSearchText(text)}
                    onSubmitEditing={handleSubmiteSearch}
                    paddingEnd={60}
                    returnKeyType={'search'}
                />
                <Image style={{ width: 20, height: 20, position: 'absolute', bottom: 0, marginBottom: 35, marginStart: 30, marginBottom: 15 }} source={require('../../../src/images/search-icon.png')} />
                {searchText && <Pressable style={{ width: 15, height: 15, position: 'absolute', end: 0, bottom: 0, marginEnd: 40, marginBottom: 17, opacity: 0.7 }} onPress={handleClearSearch} ><Image style={{ width: 15, height: 15, opacity: 0.7 }} source={require('../../../src/images/close-icon.png')} /></Pressable>}
            </View>}
            <View style={{ flex: 1, marginTop: search ? 20 : 20 }}>
                <TopBar.Navigator initialRouteName='JOBS' initialLayout={JobsScreen} screenOptions={{ swipeEnabled: false }} tabBar={props => <MyTabBar {...props} />} >
                    <TopBar.Screen name='JOBS' component={JobsScreen} />
                    <TopBar.Screen name='TRANING' component={TraningScreen} />
                </TopBar.Navigator>
            </View>
        </View>
    );
}

export default AllJonsScreen;