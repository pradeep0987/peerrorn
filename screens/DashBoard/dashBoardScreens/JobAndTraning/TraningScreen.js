import { View, Text, FlatList, Image } from 'react-native';
import JobsList from '../../../../components/JobsList';
import CommanButton from '../../../../components/comman/CommanButton';
import { Colors } from '../../../../src/colors/Colors';
import { getOpportunities } from '../../../../restapi/Services';
import React, { useState, useEffect } from 'react';
import LoadingScreen from '../../../LoadingScreens/LoadingScreen';
import TrainingList from '../../../../components/TrainingList';
import { useNavigation, useRoute } from '@react-navigation/native';
import TrainingItem from '../../../../components/TraningItems';
import AsyncStorage from '@react-native-async-storage/async-storage';

function TraningScreen() {
    const navigation = useNavigation();
    const route = useRoute();
    const [allTraningList, setAllTraningList] = useState([]);
    const [advertisement, setAdvertisement] = useState('');
    const [loading, setLoading] = useState(false);
    const [noData, setNoData] = useState(true);
    const [partTime, setPartTime] = useState(1);
    const [fullTime, setFullTime] = useState(2);
    const [seasonal, setSeasonal] = useState(4);
    const [pay, setPay] = useState('');
    const [distence, setDistence] = useState(50);
    const searchText = route.params?.searchText ? route.params?.searchText : ""

    useEffect(() => {
        getList()
    }, [allTraningList])

    async function getList() {
        const filterRes = await getFilterList();
        if (filterRes != null || filterRes !== "")
        {
            console.log(filterRes.salary, filterRes.distence, filterRes.partTime, filterRes.fullTime, filterRes.seasonal)
            setPay(filterRes.salary)
            setDistence(filterRes.distence)
            setPartTime(filterRes.partTime)
            setFullTime(filterRes.fullTime)
            setSeasonal(filterRes.seasonal)
            getAllJobsList(searchText, filterRes.salary, filterRes.distence, filterRes.partTime, filterRes.fullTime, filterRes.seasonal);
        }
    }

    async function getFilterList () {
        try {
            const list = await AsyncStorage.getItem('filterData')
            return list != null ? JSON.parse(list) : null;
        } catch (e) {
            console.log(e)
        }
    }

    async function getAllJobsList(searchText, salary, distence, partTime, fullTime, seasonal) {
        const data = await getOpportunities(`${partTime},${fullTime},${seasonal}`, distence, salary, searchText, 1, 0, 0, 10, null);
        setLoading(true)
        if (data != null) {
            setLoading(true)
            if (data.status) {
                if (data.training.length !== 0 || data.training.length != null) {
                    setLoading(false)
                    setNoData(false)
                    setAdvertisement(data.advertisement)
                    allTraningList.push(...data.training)
                } else {
                    setLoading(false)
                    setNoData(true)
                }
            } else {
                setLoading(false)
                setNoData(true)
            }
        } else {
            setLoading(false)
            setNoData(true)
        }
    }

    return(
        <View style={{flex: 1, padding: 20}}>
            {noData && <View style={{flex: 1, justifyContent: 'center'}}>
                <Text style={{fontWeight: '400', color: Colors.colorBlack, fontSize: 18, textAlign: 'center', fontFamily: 'ws_medium'}}>Your filter may be too restricted.{'\n'}Please reset your filter to see the{'\n'}opportunities</Text>
            </View>}
            {!noData && <FlatList data={allTraningList} renderItem={(itemsData , index) => {
                return(<TrainingItem items={itemsData.item} advertisement={advertisement} index={index} />);
            }} keyExtractor={(item , index) => index} showsVerticalScrollIndicator={false} showsHorizontalScrollIndicator={false} />}
            {loading && <LoadingScreen/>}
        </View>
    );
}

export default TraningScreen;