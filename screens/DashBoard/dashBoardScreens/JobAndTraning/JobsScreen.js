import { View, Text, StyleSheet, Image, TouchableOpacity, ToastAndroid, ImageBackground, Pressable, ScrollView } from 'react-native';
import { Colors } from '../../../../src/colors/Colors';
import JobsList from '../../../../components/JobsList';
import CommanButton from '../../../../components/comman/CommanButton';
import { useEffect, useState, useRef, useLayoutEffect, useMemo } from 'react';
import { getOpportunities, getZipRecruiter } from '../../../../restapi/Services';
import LoadingScreen from '../../../LoadingScreens/LoadingScreen';
import { useRoute } from '@react-navigation/native';
import { searchViewText } from '../../../../src/utils/Utils';
import AsyncStorage from '@react-native-async-storage/async-storage';
import netinfo from '@react-native-community/netinfo';
import CommanMessage from '../../../../components/comman/CommanMassage';

function JobsScreen({ navigation }) {
    const route = useRoute();
    const flatListRef = useRef();
    const [allJobsList , setAllJobsList] = useState([]);
    const [filteredDataSource, setFilteredDataSource] = useState([]);
    const [advertisement, setAdvertisement] = useState('');
    const [loading, setLoading] = useState(false);
    const [noData, setNoData] = useState(true);
    const [offset, setOffset] = useState(1);
    const [zipOffset, setZipOffset] = useState(0);
    const [peerroJobFinished, setPeerroJobFinished] = useState(false)
    const [scrollBeginDrag, setScrollBeginFDrag] = useState(true)
    const [mainIndex, setMainIndex] = useState(0);
    const [partTime, setPartTime] = useState(1);
    const [fullTime, setFullTime] = useState(2);
    const [seasonal, setSeasonal] = useState(4);
    const [pay, setPay] = useState('');
    const [distence, setDistence] = useState(50);
    const searchText = route.params?.searchText ? route.params?.searchText : ""

    useEffect(() => {
        checkNetWork();
    }, [allJobsList , AsyncStorage , route])

    function checkNetWork() {
        netinfo.fetch().then(response => {
            if (response.isConnected) {
                getList(0)
            } else {
                setLoading(false)
                CommanMessage('No Internet connection..!!')
            }
        }).catch(e => {
            console.log(e)
            CommanMessage('Somthing went wrong..!!')
        })
    }

    async function getList(jobs) {
        const filterRes = await getFilterList();
        if (filterRes != null || filterRes !== "")
        {
            console.log(filterRes.salary, filterRes.distence, filterRes.partTime, filterRes.fullTime, filterRes.seasonal)
            setPay(filterRes.salary)
            setDistence(filterRes.distence)
            setPartTime(filterRes.partTime)
            setFullTime(filterRes.fullTime)
            setSeasonal(filterRes.seasonal)
            if(searchText === "" || searchText == null) {
                getAllJobsList(jobs, searchText, filterRes.salary, filterRes.distence, filterRes.partTime, filterRes.fullTime, filterRes.seasonal);
            } else {
                setOffset(0)
                applyFilterList(jobs, searchText, filterRes.salary, filterRes.distence, filterRes.partTime, filterRes.fullTime, filterRes.seasonal)
            }
        }
    }

    async function getFilterList () {
        try {
            const list = await AsyncStorage.getItem('filterData')
            return list != null ? JSON.parse(list) : null;
        } catch (e) {
            console.log(e)
        }
    }

    async function getAllJobsList(jobs, searchText , salary , distence , partTime , fullTime , seasonal) {
        console.log("Jobs : ", jobs)
        setLoading(true)
        console.log("search text : ", searchText)
        const data = await getOpportunities(`${partTime},${fullTime},${seasonal}`, distence, salary, searchText, 0, 1, jobs, 10, null);
        if (data != null) {
            setLoading(true)
            if (data.status) {
                setLoading(false)
                setNoData(false)
                setAdvertisement(data.advertisement)
                console.log("ext joboffset: ", data.ext_joboffset)
                if (data.ext_joboffset == null || data.ext_joboffset < 1) {
                    allJobsList.push(...data.training)
                } else {
                    if (jobs === 5) {
                        allJobsList.push(...data.training)
                    } else {
                        setPeerroJobFinished(true)
                        setZipOffset(zipOffset + 1)
                        getNewZipRecruiter(zipOffset, searchText)
                        setMainIndex(mainIndex + 10)
                    }
                }
            }
        }
    }


    async function applyFilterList(jobs, searchText , salary , distence , partTime , fullTime , seasonal) {
        console.log("Jobs : ", jobs)
        setLoading(true)
        console.log("search text : ", searchText)
        setAllJobsList([])
        const data = await getOpportunities(`${partTime},${fullTime},${seasonal}`, distence, salary, searchText, 0, 1, jobs, 10, null);
        if (data != null) {
            setLoading(true)
            if (data.status) {
                setLoading(false)
                setNoData(false)
                setAdvertisement(data.advertisement)
                console.log("ext joboffset: ", data.ext_joboffset)
                if (data.ext_joboffset == null || data.ext_joboffset < 1) {
                    allJobsList.push(...data.training)
                } else {
                    if (jobs === 5) {
                        allJobsList.push(...data.training)
                    } else {
                        setPeerroJobFinished(true)
                        setZipOffset(zipOffset + 1)
                        getNewZipRecruiter(zipOffset, searchText)
                        setMainIndex(mainIndex + 10)
                    }
                }
            }
        }
    }

    async function getNewZipRecruiter(zip, searchText, distence) {
        console.log("zip : ", zip)
        setLoading(true)
        console.log("search text : ", searchText)
        const zipRecruiterData = await getZipRecruiter(zip, searchText , distence)
        if (zipRecruiterData != null) {
            setLoading(true)
            if (zipRecruiterData.success) {
                setLoading(false)
                setNoData(false)
                setAdvertisement(zipRecruiterData.advertisement)
                allJobsList.push(...zipRecruiterData.jobs)
            }
        }
    }

    function onEndReached(reached) {
        if (reached) {
            if (!scrollBeginDrag) {
                if (peerroJobFinished) {
                    setZipOffset(zipOffset + 1)
                    if (searchText == null || searchText === "") {
                        getNewZipRecruiter(zipOffset, "", distence)
                        setMainIndex(mainIndex + 10)
                    } else {
                        getNewZipRecruiter(zipOffset, searchText, distence)
                        setMainIndex(mainIndex + 10)
                    }
                } else {
                    setOffset(offset + 1)
                    getList(offset)
                    setMainIndex(mainIndex + 10)
                }
            }
        }
    }

    function onScrolled(data) {
    }

    return (
        <View style={{ flex: 1, justifyContent: 'center', paddingEnd: 20, paddingStart: 20, paddingTop: 15, paddingBottom: 10 }}>
            {noData && <View style={{ flex: 1, justifyContent: 'center' }}>
                <Text style={{ fontWeight: '500', color: Colors.colorBlack, fontSize: 18, textAlign: 'center', fontFamily: 'ws_medium' }}>There are no opportunities available{'\n'}in your zip code with selected{'\n'}industries. Please select other{'\n'}industries from your profile or check{'\n'}all opportunities</Text>
                <View style={{ width: '80%', alignSelf: 'center' }}>
                    <CommanButton title={'EDIT PROFILE'} width={'100%'} height={50} textColor={Colors.white} backgroundColor={Colors.accentMagenta} borderRadius={100} marginTop={20}
                    />
                </View>
            </View>}
            {!noData && <JobsList data={allJobsList} advertisement={advertisement} onEndReached={onEndReached} flatListRef={flatListRef} onScroll={onScrolled} mainIndex={mainIndex} scrollBeginDrag={setScrollBeginFDrag} distanceFilter={distence} />}
            {loading && <LoadingScreen />}
        </View>
    );
}

export default JobsScreen;