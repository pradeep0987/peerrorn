import { Animated, View, TouchableOpacity,Text, Image, Pressable } from 'react-native';
import { Colors } from '../../../../src/colors/Colors';
import { useState } from 'react';
import FilterScreen from '../../../Filters/FiltersScreen';

function MyTabBar({ state, descriptors, navigation, position }) {
    const [jobs , setJobs] = useState(true);
    const [traning , setTraning] = useState(false);
    const [filter , setFilter] = useState(false);

    function jobsHandler() {
        setJobs(true)
        setTraning(false)
    }

    function traningHandler() {
        setTraning(true)
        setJobs(false)
    }

    function getBackData(event) {
        if (event)
        {
            setFilter(false)
        } else {
            setFilter(true)
        }
    }
    

    return(
        <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'center', width: '85%', alignSelf: 'center'}}>
            <Pressable style={{width: 35, height: 35, justifyContent: 'center', alignItems: 'center', marginStart : 10, borderWidth: 1, borderColor: Colors.accentMagenta, borderRadius: 5}}
                onPress={() => setFilter(true)}>
                <Image style={{width: 35 , height: 35, tintColor: Colors.accentMagenta}} source={require('../../../../src/images/ic_filter.png')}/>
            </Pressable>
            <View style={{ flexDirection: 'row', backgroundColor: Colors.white, borderRadius: 100, borderColor: Colors.accentMagenta, borderWidth: 1, marginStart: 10}} >
                <Pressable style={{width: '50%', height: 35, justifyContent: 'center', backgroundColor: jobs ? Colors.accentMagenta : Colors.white , borderBottomStartRadius: 100,borderTopStartRadius: 100}}
                    onPress={() => {
                    jobsHandler()
                    navigation.navigate('JOBS')}} 
                    >
                    <Text style={{textAlign: 'center', color: jobs ? Colors.white :  Colors.accentMagenta, fontWeight: 'bold' , fontFamily: 'ws_regular'}}>JOBS</Text>
                </Pressable>
                <Pressable style={{width: '50%', height: 35, justifyContent: 'center', backgroundColor: traning ? Colors.accentMagenta : Colors.white, borderBottomEndRadius: 100,borderTopEndRadius: 100}} 
                    onPress={() => {
                        traningHandler()
                        navigation.navigate('TRANING')
                    }} 
                    >
                    <Text style={{textAlign: 'center', color: traning ? Colors.white :  Colors.accentMagenta , fontWeight: 'bold' , fontFamily: 'ws_regular'}}>TRANING</Text>
                </Pressable>
            </View>
            {filter && <FilterScreen  navigation={navigation} onBackPress={getBackData} filter={filter} /> }
        </View>
    );
}

export default MyTabBar;