import * as React from "react"
import { Animated, TouchableOpacity } from "react-native"
import { Colors } from "../../../../src/colors/Colors"

const Tab = ({ focusAnim, title, onPress }) => {
  return (
    <TouchableOpacity onPress={onPress}>
      <Animated.View
        style={{
          padding: 10,
          borderRadius: 100,
          borderWidth: 1,
          backgroundColor: focusAnim.interpolate({
            inputRange: [0, 1],
            outputRange: [Colors.white, Colors.accentMagenta]
          })
        }}
      >
        <Animated.Text
          style={{
            color: focusAnim.interpolate({
              inputRange: [0, 1],
              outputRange: [Colors.accentMagenta, Colors.white]
            })
          }}
        >{title}</Animated.Text>
      </Animated.View>
    </TouchableOpacity>
  )
}

export default Tab;