import { View, Text, StyleSheet, Image, TouchableOpacity, ToastAndroid, ImageBackground, ScrollView, FlatList, Alert, Modal } from 'react-native';
import { Colors } from '../../../src/colors/Colors';
import { userLogout } from '../../../restapi/Services';
import { useState, useEffect } from 'react';
import { launchCamera } from 'react-native-image-picker';
import LoadingScreen from '../../LoadingScreens/LoadingScreen';
import AsyncStorage from '@react-native-async-storage/async-storage';
import CommanMessage from '../../../components/comman/CommanMassage';

function MoreScreen({ navigation }) {
    const [loading, setLoading] = useState(false);
    const [logout, setLogOut] = useState(false)

    const ConfirmLogOut = () => {
        Alert.alert(
            'Confirm',
            'Are you sure you want to logout?',
            [
                {
                    text: 'NO',
                    onPress: () => console.log('Cancel Pressed'),
                    style: 'cancel',
                },
                {
                    text: 'YES',
                    onPress: () => { getLogout() },
                },
            ],
            { cancelable: false },
        );
    };

    async function getLogout() {
        setLoading(true)
        const data = await userLogout();
        console.log(data)
        if (data != null) {
            if (data.status) {
                storeData(data.status)
                CommanMessage(data.message)
                setLoading(false)
            } else {
                CommanMessage(data.message)
                setLoading(false)
            }
        }
    }

    const storeData = async (status) => {
        try {

            const filterData = JSON.stringify({
                salary: 0,
                distence: 50,
                partTime: 1,
                fullTime: 2,
                seasonal: 4,
            });

            await AsyncStorage.removeItem('userdata')
            await AsyncStorage.removeItem('response')
            await AsyncStorage.setItem('screenState', "0")
            await AsyncStorage.setItem("filterData", filterData);
            await AsyncStorage.removeItem("profileUrl");
            await AsyncStorage.removeItem('advertisement')
            await AsyncStorage.removeItem('mixPanelData')
            navigation.navigate('Welcome', { status: status })
        } catch (e) {
            console.log(e);
        }
    }

    return (
        <View style={{ flex: 1, padding: 20 }}>
            <TouchableOpacity>
                <Text style={{ fontSize: 20, marginStart: 50, marginTop: 100, color: Colors.colorBlack, fontFamily: 'ws_regular' }}>
                    Notification
                </Text>
            </TouchableOpacity>
            <TouchableOpacity>
                <Text style={{ fontSize: 20, marginStart: 50, marginTop: 20, color: Colors.colorBlack, fontFamily: 'ws_regular' }}>
                    Leadernoard
                </Text>
            </TouchableOpacity>
            <TouchableOpacity>
                <Text style={{ fontSize: 20, marginStart: 50, marginTop: 20, color: Colors.colorBlack, fontFamily: 'ws_regular' }}>
                    Connections
                </Text>
            </TouchableOpacity>
            <TouchableOpacity>
                <Text style={{ fontSize: 20, marginStart: 50, marginTop: 20, color: Colors.colorBlack, fontFamily: 'ws_regular' }}>
                    Link with Organizations
                </Text>
            </TouchableOpacity>
            <TouchableOpacity>
                <Text style={{ fontSize: 20, marginStart: 50, marginTop: 20, color: Colors.colorBlack, fontFamily: 'ws_regular' }}>
                    My Rewards
                </Text>
            </TouchableOpacity>
            <TouchableOpacity>
                <Text style={{ fontSize: 20, marginStart: 50, marginTop: 20, color: Colors.colorBlack, fontFamily: 'ws_regular' }}>
                    Share
                </Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={ConfirmLogOut}>
                <Text style={{ fontSize: 20, marginStart: 50, marginTop: 20, color: Colors.colorBlack, fontFamily: 'ws_regular' }}>
                    Sign Out
                </Text>
            </TouchableOpacity>
            {loading && <LoadingScreen />}
        </View>
    );
}

export default MoreScreen;