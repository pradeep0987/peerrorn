import { View, Text, StyleSheet, Image, TouchableOpacity, ToastAndroid, ImageBackground, ScrollView, FlatList, Share, Pressable } from 'react-native';
import { Colors } from '../../../src/colors/Colors';
import { useNavigation } from '@react-navigation/native';
import React, { useEffect, useState, useMemo } from 'react';
import { getProfileInfoData } from '../../../restapi/Services';
import LoadingScreen from '../../LoadingScreens/LoadingScreen';
import AsyncStorage from '@react-native-async-storage/async-storage';

function ProfileScreen() {
    const navigation = useNavigation();
    const [yourStats, setYourStats] = useState([]);
    const [loading, setLoading] = useState(false);
    const [salary, setSalary] = useState('');
    const [showSalary, setShowSalary] = useState(false);
    const [userProfie, setUserProfile] = useState('');
    const [refrealData, setRefrealData] = useState();

    async function onSharePress() {
        try {
            const result = await Share.share({
                message: userProfie.name + " wants to connect with you on Peerro! Check out their profile here.\n" + refrealData.referral_short_link,
            });

            if (result.action === Share.sharedAction) {
                if (result.activityType) {
                    console.log('ready')
                } else {
                    console.log('shared')
                }
            } else if (result.action === Share.dismissedAction) {
                console.log('dismiss')
            }
        } catch (error) {
            console.log(error)
        }
    };

    useEffect(() => {
        readProfileInfoData();
    }, [])

    async function readProfileInfoData() {
        setLoading(true)
        const response = await getProfileInfoData();
        console.log("user profile: ",response)
        if (response != null) {
            if (response.status) {
                setUserProfile(response.profileinfo)
                setRefrealData(response.referral_data)
                if (response.profileinfo.salary === 'Not Available') {
                    setSalary('')
                    setShowSalary(false)
                } else {
                    setShowSalary(true)
                    const newSalaryInt = parseInt(response.profileinfo.salary)
                    let newSa = parseInt(response.profileinfo.salary) / 1000
                    setSalary(newSalaryInt.toFixed().length === 3 ? response.profileinfo.salary : newSa)
                }
                setLoading(false)
            }
        }
    }

    function removeDublicateData(list) {
        const arr = list.filter((val, id, array) => array.indexOf(val) == id);
        setYourStats(arr)
    }

    const RenderYourStatuData = React.memo(({ title, point, index }) => {
        return (
            <View style={{ marginTop: 10 }} key={index} >
                <View style={{ height: 1, backgroundColor: '#dbdbdb' }}></View>
                <View style={{ flexDirection: 'row', marginStart: 5, justifyContent: 'space-between', marginTop: 5 }}>
                    <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                        {index < 2 && <Image style={{ width: 15, height: 15 }} source={require('../../../src/images/points.png')} />}
                        <Text style={{ fontSize: 14, color: Colors.colorBlack, alignSelf: 'flex-start', marginStart: 5 }} >{title}</Text>
                    </View>
                    <Text style={{ fontSize: 14, color: Colors.colorBlack, alignSelf: 'flex-end', marginEnd: 10 }} >{point}</Text>
                </View>
            </View>
        );
    })

    if (!userProfie) {
        return <LoadingScreen />
    }

    return (
        <View style={{ flex: 1 }}>
            <ScrollView showsVerticalScrollIndicator={false} showsHorizontalScrollIndicator={false}>
                <View style={{ width: '100%', flexDirection: 'row', justifyContent: 'space-between', position: 'absolute', top: 0, paddingEnd: 20, paddingStart: 20, paddingTop: 20 }}>
                    <Text style={{ fontSize: 20, color: Colors.colorBlack, fontFamily: 'ws_regular' }} >Welcome back!</Text>
                    <TouchableOpacity style={{ width: 24, height: 24, justifyContent: 'center', alignItems: 'center' }} onPress={onSharePress}>
                        <Image style={{ width: 24, height: 24, marginEnd: 5, marginTop: 5, tintColor: Colors.accentMagenta }} source={require('../../../src/images/share-active.png')} />
                    </TouchableOpacity>
                </View>
                <View style={{ padding: 20, marginTop: 50 }}>
                    <Text style={{ fontSize: 20, color: Colors.colorBlack, alignSelf: 'flex-start', fontFamily: 'ws_regular' }}  >Your Profile</Text>
                    <View style={{ backgroundColor: Colors.white, borderRadius: 10, marginTop: 10, paddingBottom: 30, paddingStart: 10, paddingEnd: 10 }}>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-around', alignItems: 'center' }}>
                            <Image style={{ width: 100, height: 100, marginEnd: 5, marginTop: 15, borderRadius: 100, overflow: 'hidden' }} source={userProfie.photo_url ? { uri: userProfie.photo_url } : require('../../../src/images/profile_inactive.png')} />
                            <View>
                                <Text style={{ fontSize: 12, color: Colors.colorBlack, alignSelf: 'center', fontFamily: 'ws_regular' }}  >{userProfie.connected_count}</Text>
                                <Text style={{ fontSize: 12, color: Colors.colorBlack, alignSelf: 'center', marginTop: 10, fontFamily: 'ws_regular' }}  >Connections</Text>
                            </View>
                            <View>
                                <Text style={{ fontSize: 12, color: Colors.colorBlack, alignSelf: 'center', fontFamily: 'ws_regular' }}  >{userProfie.lifetime_point}</Text>
                                <View style={{ flexDirection: 'row', justifyContent: 'center', marginTop: 10, alignItems: 'center' }}>
                                    <Image style={{ width: 15, height: 15, }} source={require('../../../src/images/points.png')} />
                                    <Text style={{ fontSize: 12, color: Colors.colorBlack, alignSelf: 'center', marginStart: 5, fontFamily: 'ws_regular' }}  >Points</Text>
                                </View>
                            </View>
                        </View>
                        <Text style={{ fontSize: 20, color: Colors.colorBlack, alignSelf: 'flex-start', marginStart: 10, marginTop: 20, fontFamily: 'ws_medium' }}  >{userProfie.name}</Text>
                        <Text style={{ fontSize: 16, color: Colors.colorBlack, alignSelf: 'flex-start', marginStart: 10, marginTop: 10, fontFamily: 'ws_regular' }}  >{userProfie.short_intro}</Text>
                        <Text style={{ fontSize: 14, color: Colors.colorBlack, alignSelf: 'flex-start', marginStart: 10, marginTop: 5, fontFamily: 'ws_regular' }}  >Salary Goal</Text>
                        {showSalary && <Text style={{ fontSize: 14, color: Colors.colorBlack, alignSelf: 'flex-start', marginStart: 10, marginTop: 2, fontFamily: 'ws_regular' }}  >${salary}k</Text>}
                    </View>
                    <View style={{ backgroundColor: Colors.white, borderRadius: 10, marginTop: 20, paddingBottom: 30, paddingStart: 10, paddingEnd: 10 }}>
                        <Pressable>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                <Text style={{ fontSize: 14, color: Colors.colorBlack, alignSelf: 'flex-start', marginStart: 10, marginTop: 10, fontFamily: 'ws_regular' }}  >Connections</Text>
                                <Text style={{ fontSize: 14, color: Colors.colorBlack, alignSelf: 'flex-end', marginStart: 10, marginTop: 10, fontFamily: 'ws_regular' }}  >{userProfie.connected_count}</Text>
                            </View>
                            <View style={{ flexDirection: 'row', marginTop: 15, marginStart: 10, alignItems: 'center' }}>
                                <FlatList horizontal={true} style={{ flexDirection: 'row' }} data={userProfie.connected_list} renderItem={(itemData) => { return <View style={{ flexDirection: 'row' }}><Image style={{ width: 20, height: 20, marginRight: 0.5, borderRadius: 20, overflow: 'hidden', flexDirection: 'row' }} source={{ uri: itemData.item.logo }} key={itemData.item.user_id} /></View> }} keyExtractor={(item, index) => index} showsVerticalScrollIndicator={false} showsHorizontalScrollIndicator={false}
                                    scrollEnabled={false} />
                            </View>
                        </Pressable>
                    </View>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 20 }}>
                        <ScrollView horizontal={true} showsHorizontalScrollIndicator={false} >
                            <View style={{ width: 200, height: 200, backgroundColor: Colors.white, borderRadius: 10, justifyContent: 'center', alignItems: 'center' }}>
                                <Pressable style={{ width: 200, height: 200, backgroundColor: Colors.white, borderRadius: 10, justifyContent: 'center', alignItems: 'center' }}
                                >
                                    <Image style={{ width: 80, height: 100, marginTop: 5 }} source={require('../../../src/images/profile_personal_info.png')} />
                                    <Text style={{ fontSize: 18, color: Colors.blue, alignSelf: 'center', marginTop: 20, fontWeight: '800', fontFamily: 'ws_regular' }}  >Personal Info</Text>
                                </Pressable>
                            </View>
                            <View style={{ width: 200, height: 200, backgroundColor: Colors.white, borderRadius: 10, justifyContent: 'center', alignItems: 'center', marginStart: 20 }}>
                                <Pressable style={{ width: 200, height: 200, backgroundColor: Colors.white, borderRadius: 10, justifyContent: 'center', alignItems: 'center' }}>
                                    <Image style={{ width: 75, height: 70, marginTop: 5 }} source={require('../../../src/images/profile_referral.png')} />
                                    <Text style={{ fontSize: 18, color: Colors.blue, alignSelf: 'center', marginTop: 20, fontWeight: '800', fontFamily: 'ws_regular' }}  >Refreeal</Text>
                                </Pressable>
                            </View>
                            <View style={{ width: 200, height: 200, backgroundColor: Colors.white, borderRadius: 10, justifyContent: 'center', alignItems: 'center', marginStart: 20 }}>
                                <Pressable style={{ width: 200, height: 200, backgroundColor: Colors.white, borderRadius: 10, justifyContent: 'center', alignItems: 'center' }}>
                                    <Image style={{ width: 50, height: 110, marginTop: 5 }} source={require('../../../src/images/profile_references.png')} />
                                    <Text style={{ fontSize: 18, color: Colors.blue, alignSelf: 'center', marginTop: 20, fontWeight: '800', fontFamily: 'ws_regular' }}  >References</Text>
                                    <Text style={{ fontSize: 14, color: Colors.colorBlack, alignSelf: 'center', marginTop: 10 }}  >0 Added</Text>
                                </Pressable>
                            </View>
                            <View style={{ width: 200, height: 200, backgroundColor: Colors.white, borderRadius: 10, justifyContent: 'center', alignItems: 'center', marginStart: 20 }}>
                                <Pressable style={{ width: 200, height: 200, backgroundColor: Colors.white, borderRadius: 10, justifyContent: 'center', alignItems: 'center' }}>
                                    <Image style={{ width: 100, height: 100, marginTop: 5 }} source={require('../../../src/images/profile_experience.png')} />
                                    <Text style={{ fontSize: 18, color: Colors.blue, alignSelf: 'center', marginTop: 20, fontWeight: '800', fontFamily: 'ws_regular' }}  >Experience</Text>
                                    <Text style={{ fontSize: 14, color: Colors.colorBlack, alignSelf: 'center', marginTop: 10 }}  >0 Added</Text>
                                </Pressable>
                            </View>
                            <View style={{ width: 200, height: 200, backgroundColor: Colors.white, borderRadius: 10, justifyContent: 'center', alignItems: 'center', marginStart: 20 }}>
                                <Pressable style={{ width: 200, height: 200, backgroundColor: Colors.white, borderRadius: 10, justifyContent: 'center', alignItems: 'center' }}>
                                    <Image style={{ width: 120, height: 100, marginTop: 5 }} source={require('../../../src/images/profile_education.png')} />
                                    <Text style={{ fontSize: 18, color: Colors.blue, alignSelf: 'center', marginTop: 20, fontWeight: '800', fontFamily: 'ws_regular' }}  >Education</Text>
                                    <Text style={{ fontSize: 14, color: Colors.colorBlack, alignSelf: 'center', marginTop: 10 }}  >0 Added</Text>
                                </Pressable>
                            </View>
                            <View style={{ width: 200, height: 200, backgroundColor: Colors.white, borderRadius: 10, justifyContent: 'center', alignItems: 'center', marginStart: 20 }}>
                                <Pressable style={{ width: 200, height: 200, backgroundColor: Colors.white, borderRadius: 10, justifyContent: 'center', alignItems: 'center' }}>
                                    <Image style={{ width: 80, height: 80, marginTop: 5 }} source={require('../../../src/images/profile_certification.png')} />
                                    <Text style={{ fontSize: 18, color: Colors.blue, alignSelf: 'center', marginTop: 20, fontWeight: '800', fontFamily: 'ws_regular' }}  >Certification</Text>
                                    <Text style={{ fontSize: 14, color: Colors.colorBlack, alignSelf: 'center', marginTop: 10 }}  >0 Added</Text>
                                </Pressable>
                            </View>
                            <View style={{ width: 200, height: 200, backgroundColor: Colors.white, borderRadius: 10, justifyContent: 'center', alignItems: 'center', marginStart: 20 }}>
                                <Pressable style={{ width: 200, height: 200, backgroundColor: Colors.white, borderRadius: 10, justifyContent: 'center', alignItems: 'center' }}>
                                    <Image style={{ width: 100, height: 100, marginTop: 5 }} source={require('../../../src/images/profile_training.png')} />
                                    <Text style={{ fontSize: 18, color: Colors.blue, alignSelf: 'center', marginTop: 20, fontWeight: '800', fontFamily: 'ws_regular' }}  >Traning</Text>
                                    <Text style={{ fontSize: 14, color: Colors.colorBlack, alignSelf: 'center', marginTop: 10 }}  >0 Added</Text>
                                </Pressable>
                            </View>
                            <View style={{ width: 200, height: 200, backgroundColor: Colors.white, borderRadius: 10, justifyContent: 'center', alignItems: 'center', marginStart: 20 }}>
                                <Pressable style={{ width: 200, height: 200, backgroundColor: Colors.white, borderRadius: 10, justifyContent: 'center', alignItems: 'center' }}>
                                    <Image style={{ width: 115, height: 100, marginTop: 5 }} source={require('../../../src/images/profile_career_wizard.png')} />
                                    <Text style={{ fontSize: 18, color: Colors.blue, alignSelf: 'center', marginTop: 20, fontWeight: '800', fontFamily: 'ws_regular' }}  >Career & Salary</Text>
                                    <Text style={{ fontSize: 14, color: Colors.colorBlack, alignSelf: 'center', marginTop: 10 }}  >0 Added</Text>
                                </Pressable>
                            </View>
                        </ScrollView>
                    </View>
                    <View style={{ backgroundColor: Colors.white, borderRadius: 10, marginTop: 20, paddingBottom: 30, paddingStart: 10, paddingEnd: 10 }}>
                        <Text style={{ fontSize: 14, color: Colors.colorBlack, alignSelf: 'flex-start', marginTop: 10, fontWeight: 'bold', fontFamily: 'ws_regular' }}  >Stats</Text>
                        <FlatList data={userProfie.Your_Stats} renderItem={({ item, index }) => { return <RenderYourStatuData title={item.title} point={item.point} index={index} /> }} keyExtractor={(item, index) => index} showsVerticalScrollIndicator={false} showsHorizontalScrollIndicator={false}
                            scrollEnabled={false} />
                    </View>
                </View>
            </ScrollView>
            {loading && <LoadingScreen />}
        </View>
    );
}

export default ProfileScreen;