import { View, Text, StyleSheet, Image, TouchableOpacity, ToastAndroid, Pressable,ImageBackground, Alert } from 'react-native';
import { Colors } from '../../src/colors/Colors';
import { useState, useEffect } from 'react';
import { getOnBoardService } from '../../restapi/Services';
import NetInfo from '@react-native-community/netinfo';
import { Platform } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import CommanMessage from '../../components/comman/CommanMassage';

function SplaseScreen({navigation}) {
    const [videoThumbnail , setVideoThumbnail] = useState('');

    useEffect(() => {
        handleTimer();
    },[navigation])

    async function handleTimer() {
        setTimeout(function () {
            NetInfo.fetch().then( async (state) => {
                if (state.isConnected) 
                {
                    const screenState = await getData();
                    handleApplyFilter()
                    console.log(screenState);
                    if (screenState === 0 || screenState == null || screenState === "") {
                        navigation.navigate('Welcome')
                    } else if (screenState === 1) {
                        navigation.navigate('EarnPoints')
                    } else if (screenState === 2) {
                        navigation.navigate('GetMorePoint')
                    } else if (screenState === 3) {
                        navigation.navigate('GetRewards')
                    } else if (screenState === 4) {
                        navigation.navigate('VideoWizwedJobs')
                    } else if (screenState === 5) {
                        navigation.navigate('PersonalInfo')
                    } else if (screenState === 6) {
                        navigation.navigate('CareerWizerdMain')
                    } else if (screenState === 7) {
                        navigation.navigate('DashBoard')
                    } else if (screenState === 8) {
                        navigation.navigate('CardView')
                    }
                } else {
                    CommanMessage('Please connect to the internet')
                }
            })
        }, 3000)
    }

    
    async function handleApplyFilter() {
        const filterData = JSON.stringify({
            salary: 0,
            distence: 50,
            partTime: 1,
            fullTime: 2,
            seasonal: 4,
        });

        await AsyncStorage.setItem("filterData", filterData);
    }

    const getData = async () => {
        try {
          const response = await AsyncStorage.getItem('screenState')
          return response != null ? JSON.parse(response) : null;
        } catch(e) {
          console.log(e)
        }
    }

    return(
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center', backgroundColor: Colors.blue}}>
            <Image style={{width: 125, height: 150}} source={require('../../src/images/peerro_branding_logo.png')}/>
        </View>
    );
}

export default SplaseScreen;
