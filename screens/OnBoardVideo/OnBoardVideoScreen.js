import { View, Text, StyleSheet, Image, TouchableOpacity, ToastAndroid, Modal, ImageBackground, Alert, ActivityIndicator, Share, Platform } from 'react-native';
import CommanTextInput from '../../components/comman/CommanTextInput';
import { Colors } from '../../src/colors/Colors';
import CommanButton from '../../components/comman/CommanButton';
import { useState, useEffect, useRef } from 'react';
import BackgroundHideButton from '../../components/comman/BackgroundHideButton';
import { getKeepWatching, updateOnBorading, updateWatchOnBorading } from '../../restapi/Services';
import VimeoPlayer from '../../components/VimeoPlayer/VimeoPlayer';
import YoutubeIframe from 'react-native-youtube-iframe';
import WebView from 'react-native-webview';
import LoadingScreen from '../LoadingScreens/LoadingScreen';
import NetInfo from '@react-native-community/netinfo';
import { useNavigation, useRoute } from '@react-navigation/native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { youtubeLinkParser } from '../../components/comman/Extrscting';
import { onBoardInfoData } from '../../models/onBoardService/onBoardInfo';
import ImageAdsPanel from '../ScheduleInterview/ImageAdsPanel';
import ScheduleInterViewScreen from '../ScheduleInterview/ScheduleInterview';
import CompanyOfferScreen from '../CompanyOfferScreen/CompanyOffers';

function OnBoardVideoScreen() {
    const navigation = useNavigation();
    const playerRef = useRef();
    const route = useRoute();
    const newLoading = route.params?.loading
    const [modalShow, setModalShow] = useState(true);
    const [onShare, setOnShare] = useState(true);
    const [question, setQuestion] = useState(false);
    const [onCurrect, setOnCurrect] = useState(false);
    const [onNa, setOnNa] = useState(false);
    const [onNoThanks, setOnNoThanks] = useState(false);
    const [onWrong, setOnWrong] = useState(false);
    const [videoId, setVideo] = useState();
    const [mainVideoId, setMainVideoId] = useState();
    const [index, setIndex] = useState(1);
    const [loading, setLoading] = useState(newLoading ? newLoading : true);
    const [playing, setPlaying] = useState(true);
    const [duration, setDuration] = useState('');
    const [cureentTime, setCurrentTime] = useState('');
    const [onBoardInfo, setOnBoardInfo] = useState('');
    const [points, setPoints] = useState(false);
    const [timer, setTimer] = useState(2);
    const [currectAnswer, setCurrectAnswer] = useState('');
    const [questionName, setQuestionName] = useState('');
    const [questionOption, setQuestionOption] = useState([]);
    const [numPoint, setPoint] = useState(0);
    const [plainText, setPlainText] = useState('');
    const [imagePanel, setImagePanel] = useState(false);
    const [videoPanel, setVideoPanel] = useState(false);
    const [templatePanel, setTemplatePanel] = useState(false);
    const [imageUrl, setImageUrl] = useState('');
    const [adsVideoUrl, setAdsVideoUrl] = useState('');
    const [videoThem, setVideoThem] = useState('');
    const [companyLogo, setCompanyLogo] = useState('');
    const [age, setAge] = useState(0);
    const [hoursWeek, setHoursWeek] = useState('');
    const [companyname, setCompanyname] = useState('');
    const [AdsVideoTime, setAdsVideoTime] = useState('');

    function onPressNaHandler() {
        setModalShow(false)
        setOnShare(true)
        setOnCurrect(false)
        setOnNa(true)
        setQuestion(false)
        setOnNoThanks(false)
        setOnWrong(false)
    }
    async function onPressWrongAnsHnadler() {
        setLoading(true)
        const user = await userData();
        let data = JSON.stringify({
            "userdata": {
                "user_id": user.user_id,
                "video_id": mainVideoId,
                "video_answer": "Wrong Answer"
            }
        });
        const response = await updateOnBorading(data);
        if (response.data != null) {
            setLoading(true)
            if (response.data.status) {
                setLoading(true)
                if (response.data.show_point == 1) {
                    setLoading(false)
                    setPoints(true)
                    setPoint(response.data.point)
                    setPlainText(response.data.point_text)
                    setTimeout(async function () {
                        setModalShow(false)
                        setOnShare(false)
                        setOnCurrect(false)
                        setOnNa(false)
                        setQuestion(false)
                        setOnNoThanks(false)
                        setOnWrong(true)
                    }, 3000)
                } else {
                    setLoading(false)
                    setModalShow(false)
                    setOnShare(false)
                    setOnCurrect(false)
                    setOnNa(false)
                    setQuestion(false)
                    setOnNoThanks(false)
                    setOnWrong(true)
                }
            }
        }
    }
    async function onPressCurrectAnsHnadler() {
        setLoading(true)
        setQuestion(false)
        const user = await userData();
        let data = JSON.stringify({
            "userdata": {
                "user_id": user.user_id,
                "video_id": mainVideoId,
                "video_answer": currectAnswer
            }
        });
        const response = await updateOnBorading(data);
        console.log(response.data)
        if (response.data != null) {
            setLoading(true)
            if (response.data.status) {
                setLoading(true)
                if (response.data.show_point === 1) {
                    setLoading(false)
                    setPoints(true)
                    setPoint(response.data.point)
                    setPlainText(response.data.point_text)
                    setTimeout(async function () {
                        setPoints(false)
                        setModalShow(false)
                        setOnShare(false)
                        setOnCurrect(true)
                        setOnNa(false)
                        setQuestion(false)
                    }, 3000)
                } else {
                    setLoading(false)
                    setOnCurrect(true)
                    setQuestion(false)
                    setOnWrong(false)
                }
            }
        }
    }
    useEffect(() => {
        handleRead()
    }, [])

    async function handleRead() {
        setLoading(true)
        const response = await getData();
        console.log("On Boarding : ", response)
        if (response !== null) {
            if (response.length > 0) {
                setLoading(true)
                const Index = await AsyncStorage.getItem('index');
                var videoIndex = parseInt(Index, 10);
                const data = response[videoIndex]
                if (data != null) {
                    setLoading(true)
                    if (response.length > videoIndex) {
                        setLoading(true)
                        console.log(videoIndex)
                        setMainVideoId(data.video_id)
                        if (data.has_video === 1) {
                            navigation.navigate('KeepWatching', { index: index })
                            setLoading(false)
                        } else if (data.has_video === 2) {
                            if (data.question_name === "" || data.question_name == null) {
                                const youtubeId = youtubeLinkParser(data.video_link);
                                if (youtubeId != null) {
                                    setQuestion(false)
                                    setOnShare(true)
                                    setModalShow(false)
                                    setOnNa(true)
                                    setVideo(youtubeId);
                                    setLoading(false)
                                } else {
                                    setModalShow(false)
                                    setOnNa(true)
                                    console.log(youtubeId)
                                    setLoading(false)
                                }
                            } else {
                                setCurrectAnswer(data.question_answer)
                                setQuestionName(data.question_name)
                                setQuestionOption(data.question_option)
                                setQuestion(true)
                                setOnNa(false)
                                setOnShare(false)
                                setLoading(false)
                            }
                        }
                    }
                }
            }
        }
    }

    async function onPressNextHandler() {
        setLoading(true)
        const response = await getData();
        setIndex(index + 1)
        if (response !== null) {
            if (response.length > 0) {
                const data = response[index]
                console.log(data)
                if (data != null) {
                    if (response.length > index) {
                        if (data != null || data != "") {
                            setMainVideoId(data.video_id)
                            console.log("Index : ",index)
                            await AsyncStorage.setItem('index', `${index}`)
                            if (data.has_video === 1) {
                                navigation.navigate('KeepWatching', { index: index })
                                setLoading(false)
                            } else if (data.has_video === 2) {
                                if (data.question_name === "" || data.question_name == null) {
                                    const youtubeId = youtubeLinkParser(data.video_link);
                                    if (youtubeId != null) {
                                        setQuestion(false)
                                        setOnShare(true)
                                        setModalShow(false)
                                        setOnNa(true)
                                        setVideo(youtubeId);
                                        setLoading(false)
                                    } else {
                                        setModalShow(false)
                                        setOnNa(true)
                                        console.log(youtubeId)
                                        setLoading(false)
                                    }
                                } else {
                                    setCurrectAnswer(data.question_answer)
                                    setQuestionName(data.question_name)
                                    setQuestionOption(data.question_option)
                                    setQuestion(true)
                                    setOnNa(false)
                                    setOnShare(false)
                                    setLoading(false)
                                }
                            }
                        } else {
                            setLoading(false)
                            navigation.navigate('CareerWizerd')
                        }
                    } else {
                        setLoading(false)
                        navigation.navigate('CareerWizerd')
                    }
                } else {
                    setLoading(false)
                    navigation.navigate('CareerWizerd')
                }
            } else {
                setLoading(false)
                navigation.navigate('CareerWizerd')
            }
        }
    }

    function onPressNoThanks() {
        setQuestion(false)
        setOnNa(true)
        setOnShare(true)
        setOnWrong(false)
    }

    function rePlayVideo() {
        handleRead()
        setOnWrong(false)
        setOnNa(true)
    }

    const getData = async () => {
        try {
            const response = await AsyncStorage.getItem('onboardinfo')
            return response != null ? JSON.parse(response) : null;
        } catch (e) {
            console.log(e)
        }
    }

    function youtubeLinkParser(url) {
        var regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=|\?v=)([^#\&\?]*).*/;
        var match = url.match(regExp);
        if (match && match[2].length === 11) {
            return match[2];
        } else {
            return null;
        }
    }

    const onStateChange = (state) => {
        console.log(state)
        if (state === 'ended') {
            postUpdateWatch()
        }
    };

    const userData = async () => {
        try {
            const response = await AsyncStorage.getItem('userdata')
            return response != null ? JSON.parse(response) : null;
        } catch (e) {
            console.log(e)
        }
    }

    async function postUpdateWatch() {
        const user = await userData();
        let data = JSON.stringify({
            "userdata": {
                "user_id": user.user_id,
                "video_id": mainVideoId
            }
        });
        const response = await updateWatchOnBorading(data);
        const getOnBoard = await getData();
        console.log(response)
        if (response.data != null) {
            if (response.data.status) {
                if (response.data.show_point === 1) {
                    setPoints(true)
                    setPoint(response.data.point)
                    setPlainText(response.data.point_text)
                    setTimeout(function () {
                        if (getOnBoard !== null) {
                            if (getOnBoard.length > 0) {
                                const data = getOnBoard[index]
                                if (data != null) {
                                    if (data.question_name !== "" || data.question_name != null) {
                                        setPoints(false)
                                        handleAds()
                                    }
                                }
                            }
                        }
                    }, 3000)
                } else {
                    if (getOnBoard !== null) {
                        if (getOnBoard.length > 0) {
                            const data = getOnBoard[index]
                            if (data != null) {
                                if (data.question_name !== "" || data.question_name != null) {
                                    handleAds()
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    async function handleAds() {
        const advertisement = await getAdvertisement();
        if (advertisement != null) {
            if (advertisement.Ad_Type === 'Image') {
                setImagePanel(true)
                setImageUrl(advertisement.advertisement_thumbnail)
            } else if (advertisement.Ad_Type === 'Video') {
                setVideoPanel(true)
                setAdsVideoUrl(advertisement.advertisement_file_embed)
                setVideoThem(advertisement.advertisement_thumbnail)
                setCompanyLogo(advertisement.company_logo)
                setAge(advertisement.age)
                setHoursWeek(advertisement.hours_week)
                setCompanyname(advertisement.company_name)
                setAdsVideoTime(advertisement.advertisement_file_time)
            } else if (advertisement.Ad_Type === 'Template') {
                setTemplatePanel(true)
                setCompanyLogo(advertisement.company_logo)
                setAge(advertisement.age)
                setHoursWeek(advertisement.hours_week)
                setCompanyname(advertisement.company_name)
            }
        }
    }

    const getAdvertisement = async () => {
        try {
            const response = await AsyncStorage.getItem('advertisement')
            return response != null ? JSON.parse(response) : null;
        } catch (e) {
            console.log(e)
        }
    }

    function handleNext(back) {
        if (!back) {
            setImagePanel(false)
            setVideoPanel(false)
            setOnCurrect(false)
            if (question)
            {
                setOnNa(false)
            } else {
                setOnNa(true)
            }
            setOnShare(true)
            setTemplatePanel(false)
        } else {
            setImagePanel(true)
            setVideoPanel(true)
            setTemplatePanel(true)
        }
    }

    const screenData = async () => {
        try {
            await AsyncStorage.setItem('screenState', '5')
        } catch (e) {
            console.log(e);
        }
    }

    async function onSharePress() {
        try {
            const onBoard = await getData();
            console.log("referal index : ", index)
            const result = await Share.share({
                message:
                    "Hey what’s up!\n" +
                    "You should check out this app called Peerro. It’s great for finding new opportunities near you.\n" + onBoard[index].referral_data.referral_short_link,
            });

            if (result.action === Share.sharedAction) {
                if (result.activityType) {
                    console.log('ready')
                } else {
                    console.log('shared')
                }
            } else if (result.action === Share.dismissedAction) {
                console.log('dismiss')
            }
        } catch (error) {
            console.log(error)
        }
    };

    if (loading) {
        return <View style={{ flex: 1, backgroundColor: Colors.colorBlack }}><LoadingScreen /></View>
    }

    return (
        <View style={{ flex: 1, backgroundColor: Colors.colorBlack, marginTop: Platform.OS === 'ios' ? 40 : 0 }}>
            <YoutubeIframe ref={playerRef} play={playing} videoId={videoId} height={250} width={'100%'} forceAndroidAutoplay={true} onReady={() => console.log('ready')} onError={() => console.log('error')}
                onChangeState={onStateChange} />
            {onShare && <TouchableOpacity style={{ width: 50, height: 40, alignSelf: 'flex-end', alignItems: 'center', marginEnd: 10, marginTop: 10 }} onPress={onSharePress}>
                <Image style={{ width: 40, height: 40, tintColor: Colors.lightWhite }} source={require('../../src/images/share-inactive.png')} />
                <Text style={{ fontSize: 12, alignSelf: 'center', textAlign: 'center', color: Colors.white, fontFamily: 'ws_regular' }} >SHARE</Text>
            </TouchableOpacity>}
            {question && <View style={{ width: '100%', padding: 20, height: 300 }}>
                <Text style={{ fontSize: 24, alignSelf: 'center', marginTop: 20, textAlign: 'center', color: Colors.white, fontFamily: 'ws_regular' }} >{questionName}</Text>
                <BackgroundHideButton title='CUREECT ANSWER' width={'100%'} height={40} borderColor={Colors.white} borderWidth={2} borderRadius={20} textColor={Colors.white} marginTop={40}
                    onPress={onPressCurrectAnsHnadler} />
                <BackgroundHideButton title='WRONG ANSWER' width={'100%'} height={40} borderColor={Colors.white} borderWidth={2} borderRadius={20} textColor={Colors.white} marginTop={20}
                    onPress={onPressWrongAnsHnadler} />
                <BackgroundHideButton title='NA' width={'100%'} height={40} borderColor={Colors.white} borderWidth={2} borderRadius={20} textColor={Colors.white} marginTop={20}
                    onPress={onPressNaHandler} />
            </View>}
            {onWrong && <View style={{ width: '100%', padding: 20, height: 300 }}>
                <Text style={{ fontSize: 26, alignSelf: 'center', marginTop: 20, textAlign: 'center', fontWeight: 'bold', fontFamily: 'ws_semibold', color: Colors.white }} >Aw Snap</Text>
                <Text style={{ fontSize: 16, alignSelf: 'center', marginTop: 20, textAlign: 'center', color: Colors.white, fontFamily: 'ws_regular' }} >You did not answer correctly. Try{'\n'} watching the video again to discover{'\n'} the right answer</Text>
                <Text style={{ fontSize: 16, alignSelf: 'center', marginTop: 20, textAlign: 'center', color: Colors.white, fontFamily: 'ws_regular' }} >The correct answer was : {currectAnswer}</Text>
                <BackgroundHideButton title='Rewatch Video' width={'100%'} height={40} borderColor={Colors.white} borderWidth={2} borderRadius={20} textColor={Colors.white} marginTop={20} onPress={rePlayVideo} />
                <BackgroundHideButton title='No Thanks' width={'100%'} height={40} borderColor={Colors.white} borderWidth={2} borderRadius={20} textColor={Colors.white} marginTop={20}
                    onPress={onPressNoThanks} />
            </View>}
            {onCurrect && <TouchableOpacity style={{ width: '100%', height: '100%', marginTop: 50 }} onPress={handleAds} >
                <Image style={{ width: 50, height: 50, alignSelf: 'center' }} source={require('../../src/images/ic_correct_answer.png')} />
                <Text style={{ fontSize: 24, alignSelf: 'center', marginTop: 20, textAlign: 'center', fontFamily: 'ws_regular', color: Colors.white }} >You got it!</Text>
            </TouchableOpacity>}
            {onNa && <View style={{ width: '100%', justifyContent: 'center', position: 'absolute', bottom: 0, padding: 25 }}>
                <Text style={{ textAlign: 'center', fontSize: 22, fontWeight: '600', color: Colors.white }}>What's Next?</Text>
                <CommanButton title='NEXT VIDEO' width={'100%'} height={40} backgroundColor={Colors.white} borderRadius={20} textColor={Colors.accentMagenta} marginTop={20} onPress={onPressNextHandler} />
                <BackgroundHideButton title='REPLAY VIDEO' width={'100%'} height={40} borderColor={Colors.white} borderWidth={2} borderRadius={20} textColor={Colors.white} marginTop={20} onPress={rePlayVideo} />
                <BackgroundHideButton title='CAREER WIZARD' width={'100%'} height={40} borderColor={Colors.white} borderWidth={2} borderRadius={20} textColor={Colors.white} marginTop={20} onPress={() => navigation.navigate('CareerWizerd')} />
                <BackgroundHideButton title='VIEW JOBS' width={'100%'} height={40} borderColor={Colors.white} borderWidth={2} borderRadius={20} textColor={Colors.white} marginTop={20}
                    onPress={() => navigation.navigate('PersonalInfo')} />
            </View>}
            {modalShow && <View style={{ width: '100%', position: 'absolute', bottom: 0, padding: 25 }}>
                <View style={{ width: '100%', height: 50, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', marginTop: 20 }}>
                    <BackgroundHideButton title='CAREER WIZERD' width={170} height={40} borderColor={Colors.white} borderWidth={2} borderRadius={20} textColor={Colors.white} onPress={() => navigation.navigate('CareerWizerd')} />
                    <BackgroundHideButton title='VIEW JOBS' width={170} height={40} borderColor={Colors.white} borderWidth={2} borderRadius={20} textColor={Colors.white} marginStart={5} onPress={() => {
                        navigation.navigate('PersonalInfo')
                        screenData()
                    }} />
                </View>
            </View>}
            <Modal
                animationType='fade'
                transparent={true}
                visible={points}>
                <View style={{ flex: 1, backgroundColor: Colors.colorTransparentBlack }}>
                    <ImageBackground style={{ flex: 1, opacity: 0.8, backgroundColor: 'rgb(0, 0, 5, 5)' }} source={require('../../src/images/keep_watch.png')} />
                    <View style={{ width: '80%', height: '10%', flexDirection: 'row', backgroundColor: Colors.white, marginTop: 20, marginStart: 40, alignItems: 'center', borderRadius: 10, position: 'absolute', top: 0 }}>
                        <Image style={{ width: 40, height: 40, marginStart: 10 }} source={require('../../src/images/ic_point.png')} />
                        <View style={{ marginStart: 10 }}>
                            <Text style={{ fontSize: 20, textAlign: 'left', fontFamily: 'ws_semibold', color: Colors.accentMagenta }} >+ {numPoint} Points</Text>
                            <Text style={{ fontSize: 16, textAlign: 'left', fontFamily: 'ws_regular', color: Colors.accentMagenta }} >{plainText}</Text>
                        </View>
                    </View>
                </View>
            </Modal>
            {imagePanel && <ImageAdsPanel navigation={navigation} imagePanel={imagePanel} imageUri={imageUrl} onPressBack={handleNext} />}
            {videoPanel && <ScheduleInterViewScreen navigation={navigation} visible={videoPanel} videoUrl={adsVideoUrl} videoThem={videoThem} companyLogo={companyLogo} companyname={companyname} age={age} hoursWeek={hoursWeek} videoTime={AdsVideoTime} onPressBack={handleNext} />}
            {templatePanel && <CompanyOfferScreen navigation={navigation} template={templatePanel} pay={hoursWeek} age={age} companyName={companyname} companyLogo={companyLogo} onPressBack={handleNext} />}
            {loading && <LoadingScreen />}
        </View>
    );
}

export default OnBoardVideoScreen;