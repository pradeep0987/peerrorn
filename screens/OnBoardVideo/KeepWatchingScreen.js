import { View, Text, StyleSheet, Image, TouchableOpacity, ToastAndroid, Modal, ImageBackground, Pressable, Share, Platform } from 'react-native';
import CommanTextInput from '../../components/comman/CommanTextInput';
import { Colors } from '../../src/colors/Colors';
import CommanButton from '../../components/comman/CommanButton';
import { useState, useEffect } from 'react';
import BackgroundHideButton from '../../components/comman/BackgroundHideButton';
import { useRoute } from '@react-navigation/native';
import VimeoPlayer from '../../components/VimeoPlayer/VimeoPlayer';
import ImageAdsPanel from '../ScheduleInterview/ImageAdsPanel';
import ScheduleInterViewScreen from '../ScheduleInterview/ScheduleInterview';
import CompanyOfferScreen from '../CompanyOfferScreen/CompanyOffers';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { updateWatchOnBorading } from '../../restapi/Services';
import { updateOnBorading } from '../../restapi/Services';
import LoadingScreen from '../LoadingScreens/LoadingScreen';

function KeepWatchingVideoScreen({ navigation }) {
    const route = useRoute();
    const [question, setQuestion] = useState(false);
    const [onCurrect, setOnCurrect] = useState(false);
    const [afterGotot, setAfterGotit] = useState(false);
    const [onWrong, setOnWrong] = useState(false);
    const [next, setOnNext] = useState(false);
    const [videoUrl, setVideoUrl] = useState();
    const [videoTime, setVideoTime] = useState();
    const [timer, setTimer] = useState();
    const [imagePanel, setImagePanel] = useState(false);
    const [videoPanel, setVideoPanel] = useState(false);
    const [templatePanel, setTemplatePanel] = useState(false);
    const [imageUrl, setImageUrl] = useState('');
    const [adsVideoUrl, setAdsVideoUrl] = useState('');
    const [videoThem, setVideoThem] = useState('');
    const [companyLogo, setCompanyLogo] = useState('');
    const [age, setAge] = useState(0);
    const [hoursWeek, setHoursWeek] = useState('');
    const [companyname, setCompanyname] = useState('');
    const [AdsVideoTime, setAdsVideoTime] = useState('');
    const [videoId, setVideoId] = useState('');
    const [hideShare, setHideShare] = useState(true);
    const [hideBottom, setHideBottom] = useState(true);
    const [questionName, setQuestionName] = useState('');
    const [questionOption, setQuestionOption] = useState([]);
    const [currectAnswer, setCurrectAnswer] = useState('');
    const [loading, setLoading] = useState(false);
    const [numPoint, setPoint] = useState(0);
    const [plainText, setPlainText] = useState('');
    const [pointText, setPointText] = useState('');
    const index = route.params?.index

    useEffect(() => {
        handleKeepWatching();
        console.log(index)
    }, [getData])

    async function handleKeepWatching() {
        const response = await getData();
        if (response !== null) {
            if (response.length > 0) {
                const data = response[index]
                if (data != null) {
                    setVideoUrl(data.vimeo_embed_url);
                    setVideoId(data.video_id)
                }
            }
        }
    }

    async function onPressWrongAnsHnadler() {
        setLoading(true)
        const user = await userData();
        let data = JSON.stringify({
            "userdata": {
                "user_id": user.user_id,
                "video_id": videoId,
                "video_answer": "Wrong Answer"
            }
        });
        const response = await updateOnBorading(data);
        console.log(response)
        if (response.data != null) {
            setLoading(true)
            if (response.data.status) {
                setLoading(true)
                if (response.data.show_point === 1) {
                    setLoading(false)
                    setAfterGotit(true)
                    setTimeout(async function () {
                        setAfterGotit(false)
                        setOnCurrect(true)
                        setQuestion(false)
                        setOnWrong(false)
                    }, 3000)
                } else {
                    setLoading(false)
                    setAfterGotit(false)
                    setOnCurrect(false)
                    setQuestion(false)
                    setOnWrong(true)
                }
            }
        }
    }
    async function onPressCurrectAnsHnadler() {
        setLoading(true)
        const user = await userData();
        let data = JSON.stringify({
            "userdata": {
                "user_id": user.user_id,
                "video_id": videoId,
                "video_answer": currectAnswer
            }
        });
        const response = await updateOnBorading(data);
        console.log("correct : ", response)
        if (response.data != null) {
            setLoading(true)
            if (response.data.status) {
                setLoading(true)
                if (response.data.show_point === 1) {
                    setLoading(false)
                    setAfterGotit(true)
                    setPoint(response.data.point)
                    setPlainText(response.data.plain_text)
                    setPointText(response.data.point_text)
                    setTimeout(async function () {
                        setAfterGotit(false)
                        setOnCurrect(true)
                        setQuestion(false)
                        setOnWrong(false)
                    }, 3000)
                } else {
                    setLoading(false)
                    setAfterGotit(false)
                    setOnCurrect(true)
                    setQuestion(false)
                    setOnWrong(false)
                }
            }
        }
    }
    function onPressNoThanks() {
        setOnCurrect(false)
        setQuestion(false)
        setOnWrong(false)
        setAfterGotit(false)
        setOnNext(true)
    }
    function onPressRewatch() {
        setOnWrong(false)
        setHideBottom(true)
        setHideShare(true)
        navigation.navigate('KeepWatching', { index: index })
    }

    function handleNext(back) {
        if (!back) {
            setOnNext(true)
            setAfterGotit(false)
        } else {
            setOnNext(false)
            setAfterGotit(false)
        }
    }

    function convertToSecond() {
        var a = videoTime.split(':');
        const inputDateTime = (a[0] * 3600 + a[1] * 60 + a[2]) * 1000
        console.log('time in mill', inputDateTime);
    }

    async function handleAds() {
        setOnCurrect(false)
        const advertisement = await getAdvertisement();
        if (advertisement != null) {
            switch (advertisement.Ad_Type) {
                case "Image":
                    setImagePanel(true)
                    setImageUrl(advertisement.advertisement_thumbnail)
                    break;
                case "Video":
                    setVideoPanel(true)
                    setAdsVideoUrl(advertisement.advertisement_file_embed)
                    setVideoThem(advertisement.advertisement_thumbnail)
                    setCompanyLogo(advertisement.company_logo)
                    setAge(advertisement.age)
                    setHoursWeek(advertisement.hours_week)
                    setCompanyname(advertisement.company_name)
                    setAdsVideoTime(advertisement.advertisement_file_time)
                    break;
                case "Template":
                    setTemplatePanel(true)
                    setCompanyLogo(advertisement.company_logo)
                    setAge(advertisement.age)
                    setHoursWeek(advertisement.hours_week)
                    setCompanyname(advertisement.company_name)
                    break;
            }
        }
    }

    const getAdvertisement = async () => {
        try {
            const response = await AsyncStorage.getItem('advertisement')
            return response != null ? JSON.parse(response) : null;
        } catch (e) {
            console.log(e)
        }
    }

    const getData = async () => {
        try {
            const response = await AsyncStorage.getItem('onboardinfo')
            return response != null ? JSON.parse(response) : null;
        } catch (e) {
            console.log(e)
        }
    }

    async function goToNextVideo() {
        let newIndex = index + 1; //2
        const response = await getData();
        if (response !== null) {
            if (response.length > 0) //1
            {
                if (newIndex === 15)
                {
                    if (response.length >= newIndex) {
                        const data = response[newIndex]
                        console.log("video data : ", data)
                        if (data != null || data != "") {
                            await AsyncStorage.setItem('index', `${newIndex}`)
                            navigation.navigate('onBoardVideos', { loading: true })
                        } else {
                            navigation.navigate('CareerWizerd')
                        }
                    }
                } else {
                    navigation.navigate('CareerWizerd')
                }
            } else {
                navigation.navigate('CareerWizerd')
            }
        } else {
            navigation.navigate('CareerWizerd')
        }
    }

    const screenData = async () => {
        try {
            await AsyncStorage.setItem('screenState', '5')
        } catch (e) {
            console.log(e);
        }
    }

    const handleVideoEvents = (event) => {
        let eventObj = JSON.parse(event.nativeEvent.data);
        let data = eventObj.data;
        let eventName = eventObj.name;

        if (eventName === 'ended') {
            postUpdateWatch()
        }
    };

    const userData = async () => {
        try {
            const response = await AsyncStorage.getItem('userdata')
            return response != null ? JSON.parse(response) : null;
        } catch (e) {
            console.log(e)
        }
    }

    async function postUpdateWatch() {
        const user = await userData();
        let data = JSON.stringify({
            "userdata": {
                "user_id": user.user_id,
                "video_id": videoId
            }
        });
        const response = await updateWatchOnBorading(data);
        console.log(response)
        if (response.data != null) {
            if (response.data.status) {
                showPoints(response.data)
            }
        }
    }

    async function showPoints(data) {
        const getOnBoard = await getData();
        if (data.show_point === 1) {
            setAfterGotit(true)
            setPoint(data.point)
            setPlainText(data.point_text)
            setTimeout(async function () {
                if (getOnBoard !== null) {
                    if (getOnBoard.length > 0) {
                        const data = getOnBoard[index]
                        if (data != null) {
                            if (data.question_name === "" || data.question_name == null) {
                                setAfterGotit(false)
                                handleAds()
                            } else {
                                setAfterGotit(false)
                                setVideoThem(data.video_thumbnail)
                                setCurrectAnswer(data.question_answer)
                                setQuestionName(data.question_name)
                                setQuestionOption(data.question_option)
                                setHideShare(false)
                                setHideBottom(false)
                                setQuestion(true)
                            }
                        }
                    }
                }
            }, 3000)
        } else {
            setAfterGotit(false)
            if (getOnBoard !== null) {
                if (getOnBoard.length > 0) {
                    const data = getOnBoard[index]
                    if (data != null) {
                        if (data.question_name === "" || data.question_name == null) {
                            handleAds()
                        } else {
                            setVideoThem(data.video_thumbnail)
                            setCurrectAnswer(data.question_answer)
                            setQuestionName(data.question_name)
                            setQuestionOption(data.question_option)
                            setHideShare(false)
                            setHideBottom(false)
                            setQuestion(true)
                        }
                    }
                }
            }
        }
    }

    async function onSharePress() {
        try {
            const onBoard = await getData();
            console.log("referal index : ", index)
            const result = await Share.share({
                message:
                    "Hey what’s up!\n" +
                    "You should check out this app called Peerro. It’s great for finding new opportunities near you.\n" + onBoard[index].referral_data.referral_short_link,
            });

            if (result.action === Share.sharedAction) {
                if (result.activityType) {
                    console.log('ready')
                } else {
                    console.log('shared')
                }
            } else if (result.action === Share.dismissedAction) {
                console.log('dismiss')
            }
        } catch (error) {
            console.log(error)
        }
    };

    return (
        <View style={{ flex: 1, marginTop: Platform.OS === 'ios' ? 40 : 0 }}>
            <VimeoPlayer video_url={videoUrl} height={950} width={500} handleEvents={handleVideoEvents} />
            {hideShare && <View style={{ padding: 20, position: 'absolute', top: 0, alignItems: 'flex-end', alignSelf: 'flex-end' }}>
                <TouchableOpacity style={{ width: 50, height: 40, alignSelf: 'flex-end', alignItems: 'center' }} onPress={onSharePress}>
                    <Image style={{ width: 40, height: 40, tintColor: Colors.lightWhite }} source={require('../../src/images/share-inactive.png')} />
                    <Text style={{ fontSize: 11, alignSelf: 'center', textAlign: 'center', fontFamily: 'ws_regular', color: Colors.white }} >SHARE</Text>
                </TouchableOpacity>
            </View>}
            {hideBottom && <View style={{ width: '100%', position: 'absolute', bottom: 0, padding: 25 }}>
                <View style={{ width: '100%', height: 50, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', marginTop: 20 }}>
                    <BackgroundHideButton title='CAREER WIZERD' width={170} height={40} borderColor={Colors.white} borderWidth={2} borderRadius={20} textColor={Colors.white} onPress={() => navigation.navigate('CareerWizerd')} />
                    <BackgroundHideButton title='VIEW JOBS' width={170} height={40} borderColor={Colors.white} borderWidth={2} borderRadius={20} textColor={Colors.white} marginStart={5} onPress={() => {
                        navigation.navigate('PersonalInfo')
                        screenData()
                    }} />
                </View>
            </View>}
            <Modal
                animationType='fade'
                transparent={true}
                visible={question}
                style={{ borderRadius: 10 }}>
                <ImageBackground style={{ flex: 1, justifyContent: 'center' }} source={videoThem ? { uri: videoThem } : require('../../src/images/onboard_new_bg.png')}>
                    <ImageBackground style={{ flex: 1, justifyContent: 'center' }} source={require('../../src/images/keep_watch.png')}>
                        <View style={{ padding: 20 }}>
                            <Text style={{ fontSize: 24, alignSelf: 'center', marginTop: 20, textAlign: 'center', color: Colors.white, fontFamily: 'ws_regular' }} >{questionName}</Text>
                            <BackgroundHideButton title='CUREECT ANSWER' width={'100%'} height={40} borderColor={Colors.white} borderWidth={2} borderRadius={20} textColor={Colors.white} marginTop={40}
                                onPress={onPressCurrectAnsHnadler} />
                            <BackgroundHideButton title='WRONG ANSWER' width={'100%'} height={40} borderColor={Colors.white} borderWidth={2} borderRadius={20} textColor={Colors.white} marginTop={20}
                                onPress={onPressWrongAnsHnadler} />
                        </View>
                    </ImageBackground>
                </ImageBackground>
            </Modal>
            <Modal
                animationType='fade'
                transparent={true}
                visible={onWrong}
                style={{ borderRadius: 10 }}>
                <ImageBackground style={{ flex: 1, justifyContent: 'center' }} source={videoThem ? { uri: videoThem } : require('../../src/images/onboard_new_bg.png')}>
                    <ImageBackground style={{ flex: 1, justifyContent: 'center' }} source={require('../../src/images/keep_watch.png')}>
                        <View style={{ padding: 20 }}>
                            <Text style={{ fontSize: 26, alignSelf: 'center', marginTop: 20, textAlign: 'center', fontWeight: 'bold', fontFamily: 'ws_semibold', color: Colors.white }} >Aw Snap</Text>
                            <Text style={{ fontSize: 16, alignSelf: 'center', marginTop: 20, textAlign: 'center', color: Colors.white, fontFamily: 'ws_regular' }} >You did not answer correctly. Try{'\n'} watching the video again to discover{'\n'} the right answer</Text>
                            <Text style={{ fontSize: 16, alignSelf: 'center', marginTop: 20, textAlign: 'center', color: Colors.white, fontFamily: 'ws_regular' }} >The correct answer was : {currectAnswer}</Text>
                            <BackgroundHideButton title='Rewatch Video' width={'100%'} height={40} borderColor={Colors.white} borderWidth={2} borderRadius={20} textColor={Colors.white} marginTop={20}
                                onPress={onPressRewatch} />
                            <BackgroundHideButton title='No Thanks' width={'100%'} height={40} borderColor={Colors.white} borderWidth={2} borderRadius={20} textColor={Colors.white} marginTop={20}
                                onPress={onPressNoThanks} />
                        </View>
                    </ImageBackground>
                </ImageBackground>
            </Modal>
            <Modal
                animationType='fade'
                transparent={true}
                visible={onCurrect}
                style={{ borderRadius: 10 }}>
                <ImageBackground style={{ flex: 1, justifyContent: 'center' }} source={videoThem ? { uri: videoThem } : require('../../src/images/onboard_new_bg.png')}>
                    <ImageBackground style={{ flex: 1, justifyContent: 'center' }} source={require('../../src/images/keep_watch.png')}>
                        <Pressable style={{ padding: 20, flex: 1, justifyContent: 'center' }} onPress={handleAds}>
                            <Image style={{ width: 50, height: 50, alignSelf: 'center' }} source={require('../../src/images/ic_correct_answer.png')} />
                            <Text style={{ fontSize: 24, alignSelf: 'center', marginTop: 20, textAlign: 'center', fontFamily: 'ws_regular', color: Colors.white }} >You got it!</Text>
                        </Pressable>
                    </ImageBackground>
                </ImageBackground>
            </Modal>
            <Modal
                animationType='fade'
                transparent={true}
                visible={afterGotot}>
                <View style={{ flex: 1, backgroundColor: Colors.colorTransparentBlack }}>
                    <ImageBackground style={{ flex: 1, opacity: 0.8, backgroundColor: 'rgb(0, 0, 5, 5)' }} source={require('../../src/images/keep_watch.png')} />
                    <View style={{ width: '80%', height: '10%', flexDirection: 'row', backgroundColor: Colors.white, marginTop: 20, marginStart: 40, alignItems: 'center', borderRadius: 10, position: 'absolute', top: 0 }}>
                        <Image style={{ width: 40, height: 40, marginStart: 10 }} source={require('../../src/images/ic_point.png')} />
                        <View style={{ marginStart: 10 }}>
                            <Text style={{ fontSize: 18, textAlign: 'left', fontFamily: 'ws_semibold', color: Colors.accentMagenta }} >+ {numPoint} Points</Text>
                            <Text style={{ fontSize: 14, textAlign: 'left', fontFamily: 'ws_regular', color: Colors.accentMagenta }} >{plainText ? plainText : pointText}</Text>
                        </View>
                    </View>
                </View>
            </Modal>
            <Modal
                animationType='fade'
                transparent={true}
                visible={next}>
                <ImageBackground style={{ flex: 1, justifyContent: 'center' }} source={videoThem ? { uri: videoThem } : require('../../src/images/onboard_new_bg.png')}>
                    <ImageBackground style={{ flex: 1 }} source={require('../../src/images/keep_watch.png')}>
                        <View style={{ position: 'absolute', bottom: 0, width: '100%', padding: 20 }}>
                            <Text style={{ textAlign: 'center', fontSize: 22, fontWeight: '600', color: Colors.white }}>What's Next?</Text>
                            <CommanButton title='NEXT VIDEO' width={'100%'} height={40} backgroundColor={Colors.white} borderRadius={20} textColor={Colors.accentMagenta} marginTop={20} onPress={goToNextVideo} />
                            <BackgroundHideButton title='REPLAY VIDEO' width={'100%'} height={40} borderColor={Colors.white} borderWidth={2} borderRadius={20} textColor={Colors.white} marginTop={20} />
                            <BackgroundHideButton title='CAREER WIZARD' width={'100%'} height={40} borderColor={Colors.white} borderWidth={2} borderRadius={20} textColor={Colors.white} marginTop={20} onPress={() => navigation.navigate('CareerWizerd')} />
                            <BackgroundHideButton title='VIEW JOBS' width={'100%'} height={40} borderColor={Colors.white} borderWidth={2} borderRadius={20} textColor={Colors.white} marginTop={20}
                                onPress={() => navigation.navigate('PersonalInfo')} />
                        </View>
                    </ImageBackground>
                </ImageBackground>
            </Modal>
            {imagePanel && <ImageAdsPanel navigation={navigation} imagePanel={imagePanel} imageUri={imageUrl} onPressBack={handleNext} />}
            {videoPanel && <ScheduleInterViewScreen navigation={navigation} visible={videoPanel} videoUrl={adsVideoUrl} videoThem={videoThem} companyLogo={companyLogo} companyname={companyname} age={age} hoursWeek={hoursWeek} videoTime={AdsVideoTime} onPressBack={handleNext} />}
            {templatePanel && <CompanyOfferScreen navigation={navigation} template={templatePanel} pay={hoursWeek} age={age} companyName={companyname} companyLogo={companyLogo} onPressBack={handleNext} />}
            {/* {loading && <LoadingScreen/> } */}
        </View>
    );
}

export default KeepWatchingVideoScreen;