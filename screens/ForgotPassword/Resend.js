import { View, Text, StyleSheet, Image, TouchableOpacity, ToastAndroid, Platform, Alert } from 'react-native';
import CommanTextInput from '../../components/comman/CommanTextInput';
import { Colors } from '../../src/colors/Colors';
import CommanButton from '../../components/comman/CommanButton';
import { useState, useEffect } from 'react';
import LoadingScreen from '../LoadingScreens/LoadingScreen';
import { useRoute } from '@react-navigation/native';
import ForgotPassword from './ForgotPassword';
import { forgotPassword } from '../../restapi/Services';

function ResendScreen({ navigation }) {
    const [loading, setLoading] = useState(false);
    const route = useRoute();
    const email = route.params?.email
    console.log(email)

    async function handleTimer() {
        if (email !== '') {
            let data = JSON.stringify({
                "userdata": {
                    "email_login": email
                }
            });

            setLoading(true)
            const response = await forgotPassword(data);
            console.log('response : ', response);
            if (response.data != null) {
                setLoading(true)
                if (response.data.status) {
                    setLoading(false)
                    // if(Platform.OS === 'ios')
                    // {
                    // Alert.alert(response.data.message);
                    // } else {
                    ToastAndroid.show(response.data.message, ToastAndroid.SHORT);
                    // }
                } else {
                    setLoading(false)
                    // if(Platform.OS === 'ios')
                    // {
                    // Alert.alert(response.data.message);
                    // } else {
                    ToastAndroid.show(response.data.message, ToastAndroid.SHORT);
                    // }
                }
            }
        }
    }

    return (
        <View style={{ flex: 1, marginTop: Platform.OS === 'ios' ? 40 : 0  }}>
            <View style={{ padding: 20 }}>
                <TouchableOpacity style={{ width: 20, height: 20 }} onPress={() => navigation.navigate('SignIn')}>
                    <Image style={{ width: 20, height: 20 }} source={require('../../src/images/arrow-back.png')} />
                </TouchableOpacity>
            </View>
            <View>
                <Text style={{ fontSize: 18, alignSelf: 'center', marginTop: 50, marginEnd: 50, marginStart: 50, textAlign: 'center', fontFamily: 'ws_regular', color: Colors.colorBlack }} >Greate Now chcek your email for{'\n'}the link to reset your password</Text>
            </View>
            <View style={{ flex: 1, padding: 30 }}>
                <CommanButton title={'RESEND VERIFICATION'} width={'100%'} height={50} textColor={Colors.white} backgroundColor={Colors.accentMagenta} borderRadius={100} marginTop={40}
                    onPress={handleTimer} />
            </View>
            {loading && <LoadingScreen />}
        </View>
    );;
}

export default ResendScreen;