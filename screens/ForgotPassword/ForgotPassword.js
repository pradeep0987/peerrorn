import { View, Text, StyleSheet, Image, TouchableOpacity, ToastAndroid, Alert, Platform } from 'react-native';
import CommanTextInput from '../../components/comman/CommanTextInput';
import { Colors } from '../../src/colors/Colors';
import CommanButton from '../../components/comman/CommanButton';
import { useState } from 'react';
import { isVaildEmail } from '../../src/utils/Utils';
import { forgotPassword } from '../../restapi/Services';
import LoadingScreen from '../LoadingScreens/LoadingScreen';
import { useNavigation } from '@react-navigation/native';

function ForgotPassword() {
    const navigation = useNavigation();
    const [email , setEmail] = useState('');
    const [show , setShow] = useState(false);
    const [showWarning , setShowWarning] = useState('');
    const [loading , setLoading] = useState(false);

    async function onPressSendHandler() {
        if(email === '') 
        {
            setShow(true);
            setShowWarning('Please enter your email address');
        } else if (isVaildEmail(email))
        {
            setShow(true);
            setShowWarning('Please enter a valid email address');
        } else {
            let data = JSON.stringify({
                "userdata": {
                  "email_login": email
                }
            });

            setLoading(true)
            const response = await forgotPassword(data);
            console.log('response : ', response);
            if(response.data != null)
            {
                setLoading(true)
                if(response.data.status)
                {
                    setShow(false)
                    setEmail('')
                    setLoading(false)
                    ToastAndroid.show(response.data.message, ToastAndroid.SHORT);
                    navigation.navigate('Resend',{email : email})

                } else {
                    setShow(false)
                    setLoading(false)
                    ToastAndroid.show(response.data.message, ToastAndroid.SHORT);
                }
            }
        }
    }

    return(
        <View style={{flex: 1, marginTop: Platform.OS === 'ios' ? 40 : 0 }}>
            <View style={{padding: 20}}>
                <TouchableOpacity style={{width: 20 ,height: 20}} onPress={() => navigation.navigate('SignIn')}>
                    <Image style={{width: 20 ,height: 20}} source={require('../../src/images/arrow-back.png')} />
                </TouchableOpacity>
            </View>
            <View>
                <Text style={{fontSize: 20, alignSelf: 'center', marginTop: 20, fontFamily: 'ws_medium', color: Colors.colorBlack}} >Forgot Password</Text>
                <Text style={{fontSize: 16, alignSelf: 'center', marginTop: 20,marginEnd: 50, marginStart: 50, textAlign: 'center', fontFamily: 'ws_regular', color: Colors.colorBlack}} >Please enter your email address,{'\n'}below to verify your identity</Text>
            </View>
            {show && <View style={{flexDirection: 'row', backgroundColor: Colors.accentMagenta, marginTop: 10, alignItems: 'center',paddingEnd: 30,paddingStart: 5, paddingBottom: 5, paddingTop: 5}}>
                        <View style={{transform: [{rotate: '180deg'}], marginStart: 10}}>
                            <Image style={{width: 20 ,height: 20, tintColor: Colors.white}} source={require('../../src/images/warning.png')} />
                        </View>
                        <Text style={{color: Colors.white, fontSize: 16, marginStart: 10, marginEnd: 20, fontFamily: 'ws_regular'}}>{showWarning}</Text>
                    </View>}
            <View style={{flex: 1,padding: 30,marginTop: 10}}>
                <CommanTextInput 
                    title='Email' 
                    height={50} 
                    backgroundColor={Colors.white}
                    marginTopInput={5} 
                    borderRadius={10} 
                    textColor={Colors.blue} 
                    Value={email} 
                    OnChangeText={(email) => setEmail(email)}
                    keyboardType={'email-address'}
                    paddingStart={15}
                    borderWidth={showWarning ? 2 : 0}
                    borderColor={showWarning ? Colors.accentMagenta : Colors.colorGrey}
                    returnKeyType={'done'}
                    />

                <CommanButton title={'SEND VERIFICATION'} width={'100%'} height={50} textColor={Colors.white} backgroundColor={Colors.accentMagenta} borderRadius={100} marginTop={80}
                    onPress={onPressSendHandler}/>

            </View>
            {loading && <LoadingScreen/> }
        </View>
    );
}

export default ForgotPassword;