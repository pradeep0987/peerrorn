import { View, Text, StyleSheet, Image, TouchableOpacity, ToastAndroid, ScrollView, ImageBackground, Pressable, Modal, Alert, PermissionsAndroid, Share, Linking, Platform, BackHandler } from 'react-native';
import CommanTextInput from '../../components/comman/CommanTextInput';
import BackgroundHideButton from '../../components/comman/BackgroundHideButton';
import { Colors } from '../../src/colors/Colors';
import CommanButton from '../../components/comman/CommanButton';
import { useState, useEffect, useRef } from 'react';
import { isVaildEmail, isVaildPassword } from '../../src/utils/Utils';
import CommanMessage from '../../components/comman/CommanMassage';
import { useRoute } from '@react-navigation/native';
import LoadingScreen from '../LoadingScreens/LoadingScreen';
import DatePicker from 'react-native-date-picker';
import { launchCamera, launchImageLibrary, showImagePicker } from 'react-native-image-picker';
import * as ImagePicker from 'react-native-image-picker'
import AsyncStorage from '@react-native-async-storage/async-storage';
import TimeZone from 'react-native-timezone';
import { profileUpdate, uploadMedia } from '../../restapi/Services';
import axios from 'axios';


function ProfileInfoScreen({ navigation }) {

  const route = useRoute();
  const nameInputRef = useRef(null);
  const emailInputRef = useRef(null);
  const zipCodeInputRef = useRef(null);
  const dateOfBirthInputRef = useRef(null);
  const shortInfoInputRef = useRef(null);
  const numberInputRef = useRef(null);
  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [zipCode, setZipCode] = useState();
  const [preZipCode, setPreZipCode] = useState();
  const [dateOfBirth, setDateOfBirth] = useState('');
  const [shortInfo, setShortInfo] = useState('');
  const [number, setNumber] = useState('');
  const [modalShow, setModalShow] = useState(false);
  const [warning, setWarning] = useState(true);
  const [date, setDate] = useState(new Date());
  const [open, setOpen] = useState(false);
  const [imagePicker, setImagePicker] = useState(false);
  const [pickedImage, setPickedImage] = useState('');
  const [permissions, setPermissions] = useState(false);
  const [referalData, setReferalData] = useState();
  const [millis, setMillis] = useState();
  const [userId, setUserId] = useState();
  const [loading, setLoading] = useState(false);
  const [percentage, setPercentage] = useState(0);
  const [upload, setUpload] = useState(false);
  const [numPoint, setPoint] = useState(0);
  const [plainText, setPlainText] = useState('');
  const [afterGotot, setAfterGotit] = useState(false);
  const screen = route.params?.screen
  const goBack = route.params?.show

  async function onPressNextHandler() {
    if (name === '') {
      ToastAndroid.show('Please enter your name', ToastAndroid.SHORT);
    } else if (number === '') {
      ToastAndroid.show('Please enter phone number', ToastAndroid.SHORT);
    } else if (zipCode === '') {
      ToastAndroid.show('Please enter zip code', ToastAndroid.SHORT);
    } else if (zipCode.length !== 5 || zipCode.length < 5) {
      ToastAndroid.show('Zip code must be 5 digit', ToastAndroid.SHORT);
    } else if (dateOfBirth === '') {
      ToastAndroid.show('Please select date of birth', ToastAndroid.SHORT);
    } else if (shortInfo === '') {
      ToastAndroid.show('Please enter your short info', ToastAndroid.SHORT);
    } else {
      const timeZone = await TimeZone.getTimeZone().then(zone => zone);
      console.log(timeZone);

      let data = JSON.stringify({
        "userdata": {
          "user_id": userId,
          "first_name": name,
          "phone": number,
          "date_of_birth": millis,
          "zip": `${zipCode}`,
          "short_intro": shortInfo,
          "timeZone": timeZone
        }
      });

      setLoading(true)

      const response = await profileUpdate(data);
      if (response.data != null) {
        console.log(response.data)
        setLoading(true)
        if (response.data.status) {
          setPoint(response.data.point)
          setPlainText(response.data.point_text)
          setLoading(false)
          setModalShow(true)
          allResponseData(response.data)
          console.log(response.data.referral_data)
          const user = response.data.userdata
          CommanMessage(response.data.message)
        } else {
          setLoading(false)
          CommanMessage(response.data.message)
        }
      } else {
        CommanMessage(response.data.message)
      }


    }
  }

  const getData = async () => {
    try {
      const response = await AsyncStorage.getItem('userdata')
      return response != null ? JSON.parse(response) : null;
    } catch (e) {
      console.log(e)
    }
  }

  const allResponseData = async (_response) => {
    try {
      const allResponse = JSON.stringify(_response)
      await AsyncStorage.setItem('response', allResponse)
      await AsyncStorage.setItem('screenState', '7')
    } catch (e) {
      console.log(e);
    }
  }

  function onChange(text) {
    const format = formatMobileNumber(text);
    setNumber(format);
  }

  function handleOpenSetting() {
    if (Platform.OS === 'ios') {
      Linking.openURL('app-settings:')
    } else {
      Linking.openSettings();
    }
  }

  function formatMobileNumber(text) {
    var cleaned = ("" + text).replace(/\D/g, "");
    var match = cleaned.match(/^(1|)?(\d{3})(\d{3})(\d{4})$/);
    if (match) {
      var intlCode = match[1] ? "+1 " : "",
        number = [intlCode, "(", match[2], ") ", match[3], "-", match[4]].join("");
      return number;
    }
    return text;
  }

  function navigateToDashBoard() {
    setModalShow(false)
    navigation.navigate('DashBoard')
  }

  function getFormatedDate(date) {
    const day = date.getDate()
    const month = (date.getMonth() + 1)
    const year = date.getFullYear()
    const formateDay = (day < 10 ? '0' + day : day)
    const formateMonth = (month < 10 ? '0' + month : month)
    const formattedDate = formateDay + "/" + formateMonth + "/" + year

    const inputDateTime = new Date(year, date.getMonth(), day);
    const timeInMill = inputDateTime.getTime();
    setMillis(timeInMill)
    console.log('time in mill', timeInMill);

    setDateOfBirth(formattedDate)
  }

  useEffect(() => {
    handleData()
    handlePermission();
    const backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      () => BackHandler.exitApp(),
    );
    return () => backHandler.remove();
  }, [requestCameraPermission, getData])

  async function handlePermission() {
    const permission = await requestCameraPermission();
    setPermissions(permission);
    const profile = await userProfile();
    if (profile != null || profile !== "") {
      setPickedImage(profile)
    }
  }

  async function handleData() {
    const response = await getData();
    const userName = await AsyncStorage.getItem('first_name')
    const usernumber = await AsyncStorage.getItem('phone')
    const userdateOfBirth = await AsyncStorage.getItem('date_of_birth')
    console.log(response)
    if (response != null) {
      setZipCode(response.zip.toString())
      setEmail(response.email)
      // setName(userName)
      // setNumber(usernumber)
      // setDateOfBirth(userdateOfBirth)
      setUserId(response.user_id)
    }
  }

  const requestCameraPermission = async () => {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.CAMERA,
        {
          title: "App Camera Permission",
          message: "App needs access to your camera ",
          buttonNeutral: "Ask Me Later",
          buttonNegative: "Cancel",
          buttonPositive: "OK"
        }
      );
      const grantedstorage = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
        {
          title: "App Camera Permission",
          message: "App needs access to your camera ",
          buttonNeutral: "Ask Me Later",
          buttonNegative: "Cancel",
          buttonPositive: "OK"
        }
      );
      if (grantedstorage === PermissionsAndroid.RESULTS.GRANTED && granted === PermissionsAndroid.RESULTS.GRANTED) {
        return true
      } else {
        console.log("Camera permission denied");
        return false
      }
    } catch (err) {
      console.warn(err);
      return false
    }
    return true
  };

  function handleCamera() {
    if (permissions) {
      selectFile()
      setImagePicker(false)
    } else {
      Alert.alert('Permission', 'Please grand permission', [{ text: 'Cancal' }, { text: 'Ok', onPress: handleOpenSetting }])
    }
  }

  function handleImagePicker() {
    if (permissions) {
      pickImageFile()
      setImagePicker(false)
    } else {
      Alert.alert('Permission', 'Please grand permission', [{ text: 'Cancal' }, { text: 'Ok', onPress: handleOpenSetting }])
    }
  }

  const pickImageFile = () => {
    var options = {
      title: 'Select Image',
      customButtons: [
        {
          name: 'customOptionKey',
          title: 'Choose file from Custom Option'
        },
      ],
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };

    ImagePicker.launchImageLibrary(options, async (res) => {
      console.log('Response = ', res);
      if (res.didCancel) {
        console.log('User cancelled image picker');
      } else if (res.error) {
        console.log('ImagePicker Error: ', res.error);
      } else if (res.customButton) {
        console.log('User tapped custom button: ', res.customButton);
      } else {
        setPickedImage(res.assets[0].uri);
        storeProfiledata(res.assets[0].uri)
        // uploadProgress(0 , res.assets[0].fileSize)

        const getdate = await getData();
        var myHeaders = new Headers();
        myHeaders.append('Content-Type', 'multipart/form-data;');
        myHeaders.append('session_token', `${getdate.session_token}`);


        let formData = new FormData();
        formData.append('user_id', getdate.user_id);
        formData.append('type', "profile");
        formData.append('id', getdate.user_id);

        formData.append('file', {
          uri: res.assets[0].uri,
          type: 'image/png',
          name: res.assets[0].fileName,
        });
        var url = 'https://dev.peerro.com/api_v13/profile/upload_media'

        var requestOptions = {
          url: url,
          method: 'post',
          headers: myHeaders,
          body: formData
        };


        setLoading(true)
        fetch(url, requestOptions).then(response => response.json())
          .then(result => {
            console.log('Resutl', result);
            if (result != null) {
              setLoading(true)
              if (result.status) {
                setLoading(true)
                if (result.show_point === 1) {
                  setLoading(false)
                  setAfterGotit(true)
                  setPoint(result.point)
                  setPlainText(result.point_text)
                  setTimeout(function () {
                    setAfterGotit(false)
                  }, 3000)
                }
                CommanMessage(result.message)
              } else {
                CommanMessage(result.message)
              }
            }
          })
          .catch(error => {
            console.log('error', error);
          });
      }
    });
  };

  const selectFile = async () => {
    var options = {
      title: 'Select Image',
      customButtons: [
        {
          name: 'customOptionKey',
          title: 'Choose file from Custom Option'
        },
      ],
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };

    ImagePicker.launchCamera(options, async (res) => {
      console.log('Response = ', res);
      if (res.didCancel) {
        console.log('User cancelled image picker');
      } else if (res.error) {
        console.log('ImagePicker Error: ', res.error);
      } else if (res.customButton) {
        console.log('User tapped custom button: ', res.customButton);
      } else {
        // console.log('uri', res.assets[0].uri);
        setPickedImage(res.assets[0].uri);
        console.log(res.assets[0].fileSize)
        storeProfiledata(res.assets[0].uri)

        const getdate = await getData();
        var myHeaders = new Headers();
        myHeaders.append('Content-Type', 'multipart/form-data;');
        myHeaders.append('session_token', `${getdate.session_token}`);


        let formData = new FormData();
        formData.append('user_id', getdate.user_id);
        formData.append('type', "profile");
        formData.append('id', getdate.user_id);

        formData.append('file', {
          uri: res.assets[0].uri,
          type: 'image/png',
          name: res.assets[0].fileName,
        });
        var url = 'https://dev.peerro.com/api_v13/profile/upload_media'

        var requestOptions = {
          url: url,
          method: 'post',
          headers: myHeaders,
          body: formData
        };


        fetch(url, requestOptions).then(response => response.json())
          .then(result => {
            console.log('Resutl', result);
            setLoading(true)
            if (result != null) {
              setLoading(true)
              if (result.status) {
                setLoading(true)
                if (result.show_point === 1) {
                  setLoading(false)
                  setAfterGotit(true)
                  setPoint(result.point)
                  setPlainText(result.point_text)
                  setTimeout(function () {
                    setAfterGotit(false)
                  }, 3000)
                }
                CommanMessage(result.message)
              } else {
                CommanMessage(result.message)
              }
            }
          })
          .catch(error => {
            console.log('error', error);
          });


      }
    });
  };

  async function onShare() {
    try {
      const result = await Share.share({
        message:
          "Hey what’s up!\n" +
          "You should check out this app called Peerro. It’s great for finding new opportunities near you.\n",
      });

      if (result.action === Share.sharedAction) {
        if (result.activityType) {
          console.log('ready')
        } else {
          console.log('shared')
        }
      } else if (result.action === Share.dismissedAction) {
        console.log('dismiss')
      }
    } catch (error) {
      console.log(error)
    }
  };

  const uploadProgress = (loaded, total) => {
    var Percentage = Math.round(total / 100,);
    console.log("Percentage :", Percentage)
    console.log(loaded, total);
    console.log('Upload progress: ' + Math.round(total / 100,) + '%',);
  };

  const storeProfiledata = async (data) => {
    try {
      const profileData = JSON.stringify(data)
      await AsyncStorage.setItem('profileUrl', profileData)
    } catch (e) {
      console.log(e);
    }
  }

  const userProfile = async () => {
    try {
      const response = await AsyncStorage.getItem('profileUrl')
      return response != null ? JSON.parse(response) : null;
    } catch (e) {
      console.log(e)
    }
  }

  return (
    <View style={{ flex: 1, justifyContent: 'center', marginTop: Platform.OS === 'ios' ? 40 : 0 }}>
      {goBack && <View style={{ padding: 20 }}>
        <TouchableOpacity style={{ width: 20, height: 20 }} onPress={handleForgotPassword}>
          <Image style={{ width: 20, height: 20, tintColor: Colors.accentMagenta }} source={require('../../src/images/arrow-back.png')} />
        </TouchableOpacity>
      </View>}
      <ScrollView>
        <View style={{ padding: 20, marginTop: goBack ? 5 : 20 }}>
          <Text style={{ fontSize: 20, alignSelf: 'flex-start', color: Colors.blue, fontFamily: 'ws_medium' }} >Personal Info</Text>
          <Text style={{ fontSize: 16, alignSelf: 'flex-start', marginTop: 15, color: Colors.blue, fontFamily: 'ws_medium' }} >Profile Picture</Text>
          <View style={{ width: 130 }}>
            <TouchableOpacity style={{ width: 130, height: 130, borderRadius: 50, marginTop: 10 }} onPress={() => setImagePicker(true)}>
              <ImageBackground style={{ width: 130, height: 130, borderRadius: 100, overflow: 'hidden' }} source={pickedImage ? { uri: pickedImage } : require('../../src/images/profile_inactive.png')}>
              </ImageBackground>
              <Image style={{ width: 45, height: 45, borderRadius: 50, marginStart: 5, marginTop: 2, position: 'absolute', top: 0, alignSelf: 'flex-end' }} source={require('../../src/images/ic_camera.png')} />
            </TouchableOpacity>
          </View>
        </View>
        <View style={{ flex: 1, paddingEnd: 20, paddingStart: 20, paddingBottom: 20 }}>
          <CommanTextInput
            inputRef={nameInputRef}
            title='Name'
            height={50}
            backgroundColor={Colors.white}
            marginTopInput={5}
            borderRadius={10}
            textColor={Colors.blue}
            Value={name}
            OnChangeText={(name) => setName(name)}
            paddingStart={15}
            onSubmitEditing={() => numberInputRef.current.focus()}
            returnKeyType={'next'}
             />
          <CommanTextInput
            inputRef={emailInputRef}
            title='Email'
            height={50}
            backgroundColor={Colors.grey}
            marginTopInput={5} borderRadius={10}
            textColor={Colors.blue}
            marginTop={20}
            editable={false}
            Value={email}
            OnChangeText={(email) => setEmail(email)}
            paddingStart={15}
            onSubmitEditing={() => numberInputRef.current.focus()}
            returnKeyType={'next'}
          />
          <CommanTextInput
            inputRef={numberInputRef}
            title='Phone Number'
            height={50}
            backgroundColor={Colors.white}
            marginTopInput={5} borderRadius={10}
            textColor={Colors.blue}
            marginTop={20}
            Value={number}
            OnChangeText={(phoneNumber) => onChange(phoneNumber)}
            keyboardType={'numeric'}
            paddingStart={15}
            maxLength={14}
            onSubmitEditing={() => zipCodeInputRef.current.focus()}
            returnKeyType={'next'}
          />
          <CommanTextInput
            inputRef={zipCodeInputRef}
            title='Zip Code'
            height={50}
            backgroundColor={Colors.white}
            marginTopInput={5}
            borderRadius={10}
            textColor={Colors.blue}
            marginTop={20}
            keyboardType={'numeric'}
            maxLength={5}
            Value={zipCode}
            OnChangeText={(zipcode) => setZipCode(zipcode)}
            paddingStart={15}
            onSubmitEditing={() => dateOfBirthInputRef.current.focus()}
            returnKeyType={'next'}
          />
          <CommanTextInput
            inputRef={dateOfBirthInputRef}
            title='Date Of Birth'
            height={50}
            backgroundColor={Colors.white}
            marginTopInput={5}
            borderRadius={10}
            textColor={Colors.blue}
            marginTop={20}
            Value={dateOfBirth}
            OnChangeText={(dob) => setDateOfBirth(dob)}
            paddingStart={15}
            editable={false}
            onSubmitEditing={() => shortInfoInputRef.current.focus()}
            returnKeyType={'next'}
            onPress={() => setOpen(true)}
          />
          {open && <DatePicker date={date} mode='date' modal={true} open={open} onCancel={() => setOpen(false)} onDateChange={(date) => setDate(date)} onConfirm={(date) => {
            setOpen(false)
            getFormatedDate(date)
          }} />}
          <CommanTextInput
            inputRef={shortInfoInputRef}
            title='Short Info'
            height={50}
            backgroundColor={Colors.white}
            marginTopInput={5}
            borderRadius={10}
            textColor={Colors.blue}
            marginTop={20}
            Value={shortInfo}
            OnChangeText={(info) => setShortInfo(info)}
            paddingStart={15}
            returnKeyType={'done'}
          />

          <CommanButton title={'Save'} width={'100%'} height={50} textColor={Colors.white} backgroundColor={Colors.accentMagenta} borderRadius={100} marginTop={20}
            onPress={onPressNextHandler} />
        </View>
      </ScrollView>
      {loading && <LoadingScreen />}
      <Modal
        animationType='fade'
        transparent={true}
        visible={imagePicker}
        style={{ borderRadius: 10, justifyContent: 'center', alignItems: 'center' }}>
        <View style={{ flex: 1, backgroundColor: Colors.colorTransparentBlack, alignItems: 'center', justifyContent: 'center' }}>
          <View style={{ width: '80%', height: 220, justifyContent: 'center', backgroundColor: Colors.white, alignSelf: 'center' }}>
            <View style={{ flex: 1, justifyContent: 'center', paddingEnd: 20, paddingStart: 20, paddingBottom: 10 }}>
              <Text style={{ fontSize: 20, alignSelf: 'flex-start', fontWeight: '600', fontFamily: 'ws_medium', color: Colors.colorBlack }}>Upload Image</Text>
              <TouchableOpacity style={{ marginTop: 30 }} onPress={handleCamera} >
                <Text style={{ fontSize: 16, alignSelf: 'flex-start', fontWeight: '600', fontFamily: 'ws_regular', color: Colors.colorBlack }} >Take Photo</Text>
              </TouchableOpacity>
              <TouchableOpacity style={{ marginTop: 30 }} onPress={handleImagePicker}>
                <Text style={{ fontSize: 16, alignSelf: 'flex-start', fontWeight: '600', fontFamily: 'ws_regular', color: Colors.colorBlack }} >Choose from Library</Text>
              </TouchableOpacity>
              <TouchableOpacity style={{ marginTop: 30 }} onPress={() => setImagePicker(false)}>
                <Text style={{ fontSize: 16, alignSelf: 'flex-start', fontWeight: '600', fontFamily: 'ws_regular', color: Colors.colorBlack }} >Cancel</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </Modal>
      <Modal
        animationType='fade'
        transparent={true}
        visible={warning}
        style={{ borderRadius: 10, justifyContent: 'center', alignItems: 'center' }}>
        <View style={{ flex: 1, backgroundColor: Colors.colorTransparentBlack, alignItems: 'center', justifyContent: 'center' }}>
          <View style={{ width: '80%', height: 200, justifyContent: 'center', backgroundColor: Colors.white, borderRadius: 15 }}>
            <View style={{ flex: 1, padding: 20, justifyContent: 'center' }}>
              <Text style={{ fontSize: 24, alignSelf: 'center', textAlign: 'center', fontFamily: 'ws_medium', color: Colors.colorBlack }} >Profile Incomplete!</Text>
              <Text style={{ fontSize: 16, alignSelf: 'center', textAlign: 'center', marginTop: 10, fontFamily: 'ws_regular', color: Colors.colorBlack }} >You need to fill out the rest of{'\n'}your profile in order to apply for{'\n'}jobs and build your network.</Text>
              <CommanButton title={'Complete Profile'} width={'100%'} height={50} textColor={Colors.white} backgroundColor={Colors.blue} borderRadius={5} marginTop={15}
                onPress={() => setWarning(false)} />
            </View>
          </View>
        </View>
      </Modal>
      {/* <Modal
        animationType='fade'
        transparent={true}
        visible={false}
        style={{ borderRadius: 10, justifyContent: 'center', alignItems: 'center' }}>
        <View style={{ flex: 1, backgroundColor: Colors.colorTransparentBlack, alignItems: 'center', justifyContent: 'center' }}>
          <View style={{ width: '80%', height: 200, justifyContent: 'center', backgroundColor: Colors.white }}>
            <Text style={{ fontSize: 24, textAlign: 'left', fontFamily: 'ws_medium', color: Colors.colorBlack }} >Uploading</Text>

          </View>
        </View>
      </Modal> */}
      <Modal
        animationType='fade'
        transparent={true}
        visible={modalShow}
        style={{ borderRadius: 10 }}
      >
        <View style={{ flex: 1, backgroundColor: Colors.colorTransparentBlack, alignItems: 'center', justifyContent: 'center' }}>
          <View style={{ width: '80%', height: '80%', backgroundColor: Colors.white, borderRadius: 20, elevation: 10 }}>
            <View style={{ padding: 20, justifyContent: 'center' }}>
              <Pressable style={{ width: 20, height: 20, alignSelf: 'flex-end', marginTop: 10, marginStart: 20 }} onPress={navigateToDashBoard}>
                <Image style={{ width: 20, height: 20 }} source={require('../../src/images/icon_cross.png')} />
              </Pressable>
              <Text style={{ fontSize: 24, alignSelf: 'center', marginTop: 20, marginEnd: 50, marginStart: 50, textAlign: 'center', color: Colors.accentMagenta, fontFamily: 'ws_semibold' }} >+ 10 Points</Text>
              <Image style={{ width: 150, height: 150, alignSelf: 'center', marginTop: 10 }} source={require('../../src/images/gif_point_hand.gif')} />
              <Text style={{ fontSize: 20, alignSelf: 'center', marginTop: 20, marginEnd: 50, marginStart: 50, textAlign: 'center', fontFamily: 'ws_regular', color: Colors.colorBlack }} >You earned{'\n'}10 point for{'\n'}completing your{'\n'}profile</Text>
              <Text style={{ fontSize: 16, alignSelf: 'center', marginTop: 20, marginEnd: 50, marginStart: 50, textAlign: 'center', fontFamily: 'ws_regular', color: Colors.colorBlack }} >Asesome work{'\n'}racking up those{'\n'}points</Text>
              <View style={{ justifyContent: 'center', padding: 20, borderWidth: 2, borderColor: Colors.blue, borderRadius: 15, marginTop: 20 }}>
                <Text style={{ fontSize: 20, alignSelf: 'center', textAlign: 'center', fontFamily: 'ws_regular', color: Colors.colorBlack }} >Invite a friend to earn 25{'\n'}points!</Text>
                <CommanButton title={'INVITE FRIENDS'} width={'100%'} height={50} textColor={Colors.white} backgroundColor={Colors.accentMagenta} borderRadius={100} marginTop={10}
                  onPress={onShare} />
              </View>
            </View>
          </View>
        </View>
      </Modal>
      <Modal
        animationType='fade'
        transparent={true}
        visible={afterGotot}>
        <View style={{ flex: 1, backgroundColor: Colors.colorTransparentBlack }}>
          <ImageBackground style={{ flex: 1, opacity: 0.8, backgroundColor: 'rgb(0, 0, 5, 5)' }} source={require('../../src/images/keep_watch.png')} />
          <View style={{ width: '80%', height: '10%', flexDirection: 'row', backgroundColor: Colors.white, marginTop: 20, marginStart: 40, alignItems: 'center', borderRadius: 10, position: 'absolute', top: 0 }}>
            <Image style={{ width: 40, height: 40, marginStart: 10 }} source={require('../../src/images/ic_point.png')} />
            <View style={{ marginStart: 10 }}>
              <Text style={{ fontSize: 18, textAlign: 'left', fontFamily: 'ws_semibold', color: Colors.accentMagenta }} >+ {numPoint} Points</Text>
              <Text style={{ fontSize: 14, textAlign: 'left', fontFamily: 'ws_regular', color: Colors.accentMagenta }} >{plainText}</Text>
            </View>
          </View>
        </View>
      </Modal>
    </View>
  );
}

export default ProfileInfoScreen;