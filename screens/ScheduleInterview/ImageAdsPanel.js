import { View, Text, StyleSheet, Image, TouchableOpacity, ToastAndroid, ImageBackground, Modal } from 'react-native';
import CommanTextInput from '../../components/comman/CommanTextInput';
import { Colors } from '../../src/colors/Colors';
import CommanButton from '../../components/comman/CommanButton';
import { useState, useEffect } from 'react';
import BackgroundHideButton from '../../components/comman/BackgroundHideButton';
import AsyncStorage from '@react-native-async-storage/async-storage';
import VimeoPlayer from '../../components/VimeoPlayer/VimeoPlayer';

function ImageAdsPanel({navigation, imageUri, imagePanel, onPressBack}) {
    const [back , setBack] = useState(imagePanel);

    function handleBack()
    {
        if(back)
        {
            setBack(false)
            onPressBack(false)
        }
    }

    const screenData = async () => {
        try {
            await AsyncStorage.setItem('screenState', '5')
        } catch (e) {
          console.log(e);
        }
    }

    return(
        <Modal
            animationType="fade"
            transparent={true}
            visible={back}>
            <View style={{flex: 1}}>
                <ImageBackground style={{flex: 1}} source={imageUri ? {uri : imageUri} : require('../../src/images/onboard_new_bg.png')}>
                    <View style={{
                        width: '100%', 
                        height: 150, 
                        position: 'absolute',
                        bottom: 0,
                        paddingEnd: 30,
                        paddingStart: 30,
                        backgroundColor: Colors.colorBlack,
                        justifyContent: 'center'}}>
                        <CommanButton title='Schedule Interview' width={'100%'} height={50} backgroundColor={Colors.accentMagenta} borderRadius={20} textColor={Colors.white}
                            onPress={() => {
                                navigation.navigate('PersonalInfo')
                                screenData()}}/>
                        <BackgroundHideButton title='SKIP' width={'100%'} height={50} borderRadius={20} textColor={Colors.white} marginTop={20}
                            onPress={handleBack}/>
                    </View>
                </ImageBackground>
            </View>
        </Modal>
    );
}

export default ImageAdsPanel;