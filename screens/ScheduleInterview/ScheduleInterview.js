import { View, Text, StyleSheet, Image, TouchableOpacity, ToastAndroid, ImageBackground, Modal } from 'react-native';
import CommanTextInput from '../../components/comman/CommanTextInput';
import { Colors } from '../../src/colors/Colors';
import CommanButton from '../../components/comman/CommanButton';
import { useState, useEffect } from 'react';
import BackgroundHideButton from '../../components/comman/BackgroundHideButton';
import AsyncStorage from '@react-native-async-storage/async-storage';
import VimeoPlayer from '../../components/VimeoPlayer/VimeoPlayer';

function ScheduleInterViewScreen({navigation, visible , videoUrl , videoThem , companyLogo , age , hoursWeek, companyname, videoTime, onPressBack}) {
    // const [counter, setTimer] = useState(5);
    const [show , setShow] = useState(false);
    const [back , setBack] = useState(visible);
    // const [modalShow , setModalShow] = useState(false);
    // const [videoUrl , setVideoUrl] = useState();
    // const [videoTime , setVideoTime] = useState('');
    // const [img , setImg] = useState('');
    // const [companyLogo , setCompanyLogo] = useState();
    // const [jobTitle , setJobTitle] = useState('');
    // const [age , setAge] = useState('');
    // const [companyname , setCompanyName] = useState('');
    // const [hoursWeek , setHoursWeek] = useState('');
    const [sliderValue, setSliderValue] = useState(0);
    const [vimeoTimer, setVimeoTimer] = useState('');

    function handleBack()
    {
        if(back)
        {
            setBack(false)
            onPressBack(false)
        }
    }

    const handleVideoEvents = (event) => {
        let eventObj = JSON.parse(event.nativeEvent.data);
        let data = eventObj.data;
        let eventName = eventObj.name;
  
        switch (eventName) {
            case 'play':
              console.log('eventName :', eventName);
              break;
            case 'pause':
              console.log('eventName :', eventName);
              break;
            case 'ended':
              console.log('eventName :', eventName);
              setShow(true)
              break;
            case 'timeupdate':
              setSliderValue(data.seconds);
              let totalLeftSec = data.duration - data.seconds;
              const dateObj = new Date(totalLeftSec * 1000);
              let hours = dateObj.getUTCHours();
              let minutes = dateObj.getUTCMinutes();
              let seconds = dateObj.getSeconds();
      
              let timeString =
                hours.toString().padStart(2, '0') +
                ':' +
                minutes.toString().padStart(2, '0') +
                ':' +
                seconds.toString().padStart(2, '0');
              setVimeoTimer(timeString);
      
              break;
            case 'slideTo':
              console.log('eventName :', eventName);
              break;
          }
      };

      const screenData = async () => {
        try {
            await AsyncStorage.setItem('screenState', '5')
        } catch (e) {
          console.log(e);
        }
    }

    return(
        <Modal
        animationType='fade'
        transparent={true}
        visible={back}>
            <View style={{flex: 1}}>
                <ImageBackground style={{flex: 1, justifyContent: 'center'}} source={videoThem ? {uri : videoThem} : require('../../src/images/onboard_new_bg.png')}>
                    {!show && <VimeoPlayer video_url={videoUrl} height={950} width={500} handleEvents={handleVideoEvents} />}
                    {!show && <TouchableOpacity style={{width: '100%',padding: 20, position: 'absolute', marginBottom: 10, top: 0, flexDirection: 'row', justifyContent: 'space-between'}} 
                        onPress={handleBack}>
                        <Text style={{fontFamily: 'ws_regular', color: Colors.white}}>{vimeoTimer}</Text>
                        <Image style={{width: 20, height: 20, tintColor: Colors.accentMagenta}} source={require('../../src/images/close-icon.png')}/>
                    </TouchableOpacity>}
                    {show &&   <ImageBackground style={{flex: 1, justifyContent: 'center'}} source={require('../../src/images/keep_watch.png')}>
                                <View style={{padding: 30, justifyContent: 'center', marginTop: 100}}>
                                    <Image style={{width: 120 ,height: 120, alignSelf: 'center', marginTop: 10, borderRadius: 100, overflow: 'hidden'}} source={{uri : companyLogo}} />
                                    <Text style={{fontSize: 20, alignSelf: 'center', marginTop: 20,marginEnd: 50, marginStart: 50, textAlign: 'center',fontWeight: '600', fontFamily: 'ws_medium', color: Colors.white}} >{companyname} is{'\n'}offering{'\n'}you an interview!</Text>
                                    <Text style={{fontSize: 16, alignSelf: 'center', marginTop: 20,marginEnd: 50, marginStart: 50, textAlign: 'center',fontWeight: '500', fontFamily: 'ws_regular', fontFamily: 'ws_regular', color: Colors.white}} >
                                        Do you want to schedule an{'\n'}interview for a job that pays{'\n'}${hoursWeek}/Hourly?</Text>
                                    <Text style={{fontSize: 16, alignSelf: 'center', marginTop: 20, textAlign: 'center',fontWeight: '400', fontFamily: 'ws_regular', color: Colors.white}} >Are you {age} years or older?</Text>
                                    <CommanButton title={'Schedule Interview'} width={'100%'} height={50} textColor={Colors.white} backgroundColor={Colors.accentMagenta} borderRadius={100} marginTop={30}
                                        onPress={() => {
                                            navigation.navigate('PersonalInfo')
                                            screenData()}}/>
                                    <BackgroundHideButton title={'SKIP'} width={'100%'} height={50} textColor={Colors.accentMagenta} marginTop={10}
                                        onPress={handleBack}/>
                                </View>
                            </ImageBackground>}
                    {!show && <View style={{
                        flex: 1,
                        width: '100%', 
                        height: 50, 
                        position: 'absolute',
                        marginBottom: 40,
                        bottom: 0,
                        paddingEnd: 30,
                        paddingStart: 30}}>
                        <CommanButton title='Schedule Interview' width={'100%'} height={50} backgroundColor={Colors.accentMagenta} borderRadius={20} textColor={Colors.white}
                            onPress={() => {
                                navigation.navigate('PersonalInfo')
                                screenData()}}/>
                    </View>}
                </ImageBackground>
            </View>
        </Modal>
    );
}

export default ScheduleInterViewScreen;
