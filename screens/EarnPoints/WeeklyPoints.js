import { View, Text, StyleSheet, Image, TouchableOpacity, ToastAndroid, BackHandler, Platform } from 'react-native';
import CommanTextInput from '../../components/comman/CommanTextInput';
import { Colors } from '../../src/colors/Colors';
import CommanButton from '../../components/comman/CommanButton';
import { useState, useEffect } from 'react';
import AsyncStorage from '@react-native-async-storage/async-storage';

function EarnPointsScreen({ navigation }) {
    function onBackPressed() {
        BackHandler.exitApp();
    }

    useEffect(() => {
        const backHandler = BackHandler.addEventListener(
            'hardwareBackPress',
            () => true,
        );
        return () => backHandler.remove();
    })
    
    function componentWillMount() {
        const handler = BackHandler.addEventListener('hardwareBackPress', onBackPressed());
        return () => handler.remove();
    }


    const userData = async () => {
        try {
            await AsyncStorage.setItem('screenState', '2')
        } catch (e) {
            console.log(e);
        }
    }

    return (
        <View style={{ flex: 1, backgroundColor: Colors.white, marginTop: Platform.OS === 'ios' ? 40 : 0 }}>
            <View style={{ padding: 20, flexDirection: 'row', justifyContent: 'space-between' }}>
                <TouchableOpacity style={{ width: 20, height: 20 }} onPress={componentWillMount}>
                    <Image style={{ width: 20, height: 20 }} source={require('../../src/images/arrow-back.png')} />
                </TouchableOpacity>
                <View>
                    <Text style={{ fontSize: 14, alignSelf: 'flex-end', fontFamily: 'ws_regular' }} >Weekly Points</Text>
                    <Text style={{ fontSize: 14, alignSelf: 'flex-end', color: Colors.accentMagenta, fontFamily: 'ws_regular' }} >5</Text>
                </View>
            </View>
            <View style={{ flex: 1, justifyContent: 'center', padding: 25 }}>
                <View>
                    <Text style={{ fontSize: 26, alignSelf: 'center', color: Colors.accentMagenta, fontFamily: 'ws_medium', fontWeight: '700' }} >+ 5 Points</Text>
                    <Image style={{ width: 150, height: 150, alignSelf: 'center', marginTop: 10 }} source={require('../../src/images/gif_point_hand.gif')} />
                    <Text style={{ fontSize: 20, alignSelf: 'center', marginTop: 20, marginEnd: 50, marginStart: 50, textAlign: 'center', fontFamily: 'ws_regular', color: Colors.colorBlack }} >You earned 5 points for{'\n'}signing up!</Text>
                    <Text style={{ fontSize: 16, alignSelf: 'center', marginTop: 20, marginEnd: 50, marginStart: 50, textAlign: 'center', fontFamily: 'ws_regular', color: Colors.colorBlack }} >Awesome work on reacking up{'\n'}those points</Text>
                </View>
                <CommanButton title={'CONTINUE'} width={'100%'} height={50} textColor={Colors.white} backgroundColor={Colors.accentMagenta} borderRadius={100} marginTop={80}
                    onPress={() => {
                        navigation.navigate('GetMorePoint')
                        userData()
                    }} />
            </View>
        </View>
    );
}

export default EarnPointsScreen;