import { View, Text, StyleSheet, Image, TouchableOpacity, ToastAndroid, Alert, Share, BackHandler, Platform } from 'react-native';
import CommanTextInput from '../../components/comman/CommanTextInput';
import { Colors } from '../../src/colors/Colors';
import CommanButton from '../../components/comman/CommanButton';
import { useState, useEffect } from 'react';
import BackgroundHideButton from '../../components/comman/BackgroundHideButton';

function GetMorePointScreen({ navigation }) {
  async function onShare() {
    try {
      const result = await Share.share({
        message:
          'Send Massages',
      });

      if (result.action === Share.sharedAction) {
        if (result.activityType) {
          console.log('ready')
        } else {
          console.log('shared')
        }
      } else if (result.action === Share.dismissedAction) {
        console.log('dismiss')
      }
    } catch (error) {
      Alert.alert(error.message);
    }
  };

  useEffect(() => {
    const backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      () => BackHandler.exitApp(),
    );
    return () => backHandler.remove();
  })

  const userData = async () => {
    try {
      await AsyncStorage.setItem('screenState', '3')
    } catch (e) {
      console.log(e);
    }
  }

  return (
    <View style={{ flex: 1, padding: 20, justifyContent: 'center', backgroundColor: Colors.white, alignItems: 'center', marginTop: Platform.OS === 'ios' ? 40 : 0 }}>
      <Image style={{ width: 150, height: 150, alignSelf: 'center', marginTop: 10 }} source={require('../../src/images/gif_point_hand.gif')} />
      <Text style={{ fontSize: 24, alignSelf: 'center', marginTop: 20, marginEnd: 50, marginStart: 50, textAlign: 'center', fontFamily: 'ws_semibold', color: Colors.colorBlack }} >Get More Points{'\n'}When Your{'\n'} Friends Join</Text>
      <Text style={{ fontSize: 16, alignSelf: 'center', marginTop: 10, marginEnd: 50, marginStart: 50, textAlign: 'center', fontFamily: 'ws_regular', color: Colors.colorBlack }} >Rank up points to{'\n'}win prizes</Text>
      <View style={{ justifyContent: 'center', padding: 20, borderWidth: 2, borderColor: Colors.blue, borderRadius: 15, marginTop: 20, height: 160, width: '90%' }}>
        <Text style={{ fontSize: 20, alignSelf: 'center', textAlign: 'center', fontFamily: 'ws_regular', color: Colors.colorBlack }} >Invite a friend to earn 25{'\n'}points!</Text>
        <CommanButton title={'SHARE WITH CONTACTS'} width={'100%'} height={50} textColor={Colors.white} backgroundColor={Colors.accentMagenta} borderRadius={100} marginTop={20}
          onPress={onShare} />
      </View>
      <BackgroundHideButton title={'Continue'} width={'100%'} height={50} textColor={Colors.accentMagenta} marginTop={10} onPress={() => {
        navigation.navigate('GetRewards')
        userData()
      }} />
    </View>
  );
}

export default GetMorePointScreen;