import { View, Text, StyleSheet, Image, TouchableOpacity, ToastAndroid, Alert, ImageBackground, Modal, BackHandler, Platform } from 'react-native';
import CommanTextInput from '../../components/comman/CommanTextInput';
import { Colors } from '../../src/colors/Colors';
import CommanButton from '../../components/comman/CommanButton';
import { useState, useEffect } from 'react';
import BackgroundHideButton from '../../components/comman/BackgroundHideButton';
import NetInfo from '@react-native-community/netinfo';
import { getKeepWatching } from '../../restapi/Services';
import ImageAdsPanel from '../ScheduleInterview/ImageAdsPanel';
import ScheduleInterViewScreen from '../ScheduleInterview/ScheduleInterview';
import CompanyOfferScreen from '../CompanyOfferScreen/CompanyOffers';
import codegenNativeCommands from 'react-native/Libraries/Utilities/codegenNativeCommands';
import VimeoPlayer from '../../components/VimeoPlayer/VimeoPlayer';
import AsyncStorage from '@react-native-async-storage/async-storage';
import CommanMessage from '../../components/comman/CommanMassage';

function GetRewardsScreen({ navigation }) {
  let videoIndex = 0;
  const [imagePanel, setImagePanel] = useState(false);
  const [videoPanel, setVideoPanel] = useState(false);
  const [templatePanel, setTemplatePanel] = useState(false);
  const [imageUrl, setImageUrl] = useState('');
  const [adsVideoUrl, setAdsVideoUrl] = useState('');
  const [videoThem, setVideoThem] = useState('');
  const [companyLogo, setCompanyLogo] = useState('');
  const [age, setAge] = useState(0);
  const [hoursWeek, setHoursWeek] = useState('');
  const [companyname, setCompanyname] = useState('');
  const [AdsVideoTime, setAdsVideoTime] = useState('');
  const [show, setShow] = useState(false);
  const [sliderValue, setSliderValue] = useState(0);
  const [vimeoTimer, setVimeoTimer] = useState('');

  useEffect(() => {
    handleStore();
    const backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      () => true,
    );
    return () => backHandler.remove();
  })

  async function handleStore() {
    NetInfo.fetch().then(async (state) => {
      if (state.isConnected) {
        const response = await getKeepWatching();
        if (response.data != null) {
          if (response.data.status) {
            onBoardInfoData(response.data.onboard_info)
          }
        }
      } else {
        CommanMessage('Please connect to the internet')
      }
    })
  }

  const onBoardInfoData = async (data) => {
    try {
      const list = JSON.stringify(data)
      await AsyncStorage.setItem('onboardinfo', list)
    } catch (e) {
      console.log(e);
    }
  }

  const screenData = async () => {
    try {
      await AsyncStorage.setItem('screenState', '4')
      setImagePanel(false)
      setVideoPanel(false)
      setTemplatePanel(false)
      setShow(false)
    } catch (e) {
      console.log(e);
    }
  }

  async function handleAds() {
    const advertisement = await getAdvertisement();
    if (advertisement != null || advertisement !== "") {
      if (advertisement.Ad_Type === 'Image') {
        setImagePanel(true)
        setImageUrl(advertisement.advertisement_thumbnail)
      } else if (advertisement.Ad_Type === 'Video') {
        setVideoPanel(true)
        setAdsVideoUrl(advertisement.advertisement_file_embed)
        setVideoThem(advertisement.advertisement_thumbnail)
        setCompanyLogo(advertisement.company_logo)
        setAge(advertisement.age)
        setHoursWeek(advertisement.hours_week)
        setCompanyname(advertisement.company_name)
        setAdsVideoTime(advertisement.advertisement_file_time)
      } else if (advertisement.Ad_Type === 'Template') {
        navigation.navigate('VideoWizwedJobs')
        screenData()
      }
    } else if (advertisement == null || advertisement === "") {
      navigation.navigate('VideoWizwedJobs')
      screenData()
    }
  }

  const getAdvertisement = async () => {
    try {
      const response = await AsyncStorage.getItem('advertisement')
      return response != null ? JSON.parse(response) : null;
    } catch (e) {
      console.log(e)
    }
  }

  const handleVideoEvents = (event) => {
    let eventObj = JSON.parse(event.nativeEvent.data);
    let data = eventObj.data;
    let eventName = eventObj.name;

    switch (eventName) {
      case 'play':
        console.log('eventName :', eventName);
        break;
      case 'pause':
        console.log('eventName :', eventName);
        break;
      case 'ended':
        console.log('eventName :', eventName);
        setShow(true)
        break;
      case 'timeupdate':
        setSliderValue(data.seconds);
        let totalLeftSec = data.duration - data.seconds;
        const dateObj = new Date(totalLeftSec * 1000);
        let hours = dateObj.getUTCHours();
        let minutes = dateObj.getUTCMinutes();
        let seconds = dateObj.getSeconds();

        let timeString =
          hours.toString().padStart(2, '0') +
          ':' +
          minutes.toString().padStart(2, '0') +
          ':' +
          seconds.toString().padStart(2, '0');
        setVimeoTimer(timeString);

        break;
      case 'slideTo':
        console.log('eventName :', eventName);
        break;
    }
  };

  const screenProfileData = async () => {
    try {
      await AsyncStorage.setItem('screenState', '5')
    } catch (e) {
      console.log(e);
    }
  }

  return (
    <View style={{ flex: 1, backgroundColor: Colors.white , marginTop: Platform.OS === 'ios' ? 40 : 0}}>
      <View style={{ padding: 20 }}>
        <TouchableOpacity style={{ width: 20, height: 20 }} onPress={() => navigation.navigate('GetMorePoint')}>
          <Image style={{ width: 20, height: 20, tintColor: Colors.accentMagenta }} source={require('../../src/images/arrow-back.png')} />
        </TouchableOpacity>
      </View>
      <View style={{ padding: 25, justifyContent: 'center' }}>
        <Image style={{ width: 200, height: 150, alignSelf: 'center' }} source={require('../../src/images/ic_point_info.png')} />
        <Text style={{ fontSize: 24, alignSelf: 'center', marginTop: 20, marginEnd: 50, marginStart: 50, textAlign: 'center', fontWeight: '600', color: Colors.accentMagenta, fontFamily: 'ws_semibold' }} >Earn Points{'\n'}Get Rewards</Text>
        <Text style={{ fontSize: 18, alignSelf: 'center', marginTop: 10, marginEnd: 50, marginStart: 50, textAlign: 'center', fontWeight: '500', fontFamily: 'ws_regular', color: Colors.colorBlack }} >
          You can earn points by{'\n'}checking out our videos,{'\n'}completing your prfile,{'\n'}sharing the app with{'\n'}friends and more</Text>
        <Text style={{ fontSize: 16, alignSelf: 'center', marginTop: 20, textAlign: 'center', fontWeight: '400', fontFamily: 'ws_regular', color: Colors.colorBlack }} >
          The point will help you climb{'\n'}up in the leaderboard. The top{'\n'}sport in the leaderboard will{'\n'}get Peerro rewards and special{'\n'}offers</Text>
        <CommanButton title={'CONTINUE'} width={'100%'} height={50} textColor={Colors.white} backgroundColor={Colors.accentMagenta} borderRadius={100} marginTop={80}
          onPress={handleAds} />
      </View>
      <Modal
        animationType="fade"
        transparent={true}
        visible={imagePanel}>
        <View style={{ flex: 1 }}>
          <ImageBackground style={{ flex: 1, backgroundColor: Colors.colorBlack }} source={imageUrl ? { uri: imageUrl } : require('../../src/images/onboard_new_bg.png')}>
            <View style={{
              width: '100%',
              height: 150,
              position: 'absolute',
              bottom: 0,
              paddingEnd: 30,
              paddingStart: 30,
              backgroundColor: Colors.colorBlack,
              justifyContent: 'center'
            }}>
              <CommanButton title='Schedule Interview' width={'100%'} height={50} backgroundColor={Colors.accentMagenta} borderRadius={20} textColor={Colors.white}
                onPress={() => {
                  navigation.navigate('PersonalInfo')
                  screenProfileData()
                }} />
              <BackgroundHideButton title='SKIP' width={'100%'} height={50} borderRadius={20} textColor={Colors.white} marginTop={10}
                onPress={() => {
                  navigation.navigate('VideoWizwedJobs')
                  screenData()
                }} />
            </View>
          </ImageBackground>
        </View>
      </Modal>
      <Modal
        animationType='fade'
        transparent={true}
        visible={videoPanel}>
        <View style={{ flex: 1 }}>
          {!show && <VimeoPlayer video_url={adsVideoUrl} height={950} width={500} handleEvents={handleVideoEvents} />}
          {!show && <TouchableOpacity style={{ width: '100%', padding: 20, position: 'absolute', marginBottom: 10, top: 0, flexDirection: 'row', justifyContent: 'space-between' }}
            onPress={() => {
              navigation.navigate('VideoWizwedJobs')
              screenData()
            }}>
            <Text style={{ fontFamily: 'ws_regular', color: Colors.white }}>{vimeoTimer}</Text>
            <Image style={{ width: 20, height: 20 }} source={require('../../src/images/icon_cross.png')} />
          </TouchableOpacity>}
          {show && <ImageBackground style={{ flex: 1, justifyContent: 'center' }} source={videoThem ? { uri: videoThem } : require('../../src/images/onboard_new_bg.png')}>
            <ImageBackground style={{ flex: 1, justifyContent: 'center' }} source={require('../../src/images/keep_watch.png')}>
              <View style={{ padding: 30, justifyContent: 'center', marginTop: 20 }}>
                <Image style={{ width: 120, height: 120, alignSelf: 'center', marginTop: 10, borderRadius: 100, overflow: 'hidden' }} source={{ uri: companyLogo }} />
                <Text style={{ fontSize: 20, alignSelf: 'center', marginTop: 20, marginEnd: 50, marginStart: 50, textAlign: 'center', fontWeight: '600', fontFamily: 'ws_medium', color: Colors.white }} >{companyname} is{'\n'}offering{'\n'}you an interview!</Text>
                <Text style={{ fontSize: 16, alignSelf: 'center', marginTop: 20, marginEnd: 50, marginStart: 50, textAlign: 'center', fontWeight: '500', fontFamily: 'ws_regular', fontFamily: 'ws_regular', color: Colors.white }} >
                  Do you want to schedule an{'\n'}interview for a job that pays{'\n'}${hoursWeek}/Hourly?</Text>
                <Text style={{ fontSize: 16, alignSelf: 'center', marginTop: 20, textAlign: 'center', fontWeight: '400', fontFamily: 'ws_regular', color: Colors.white }} >Are you {age} years or older?</Text>
                <CommanButton title={'Schedule Interview'} width={'100%'} height={50} textColor={Colors.white} backgroundColor={Colors.accentMagenta} borderRadius={100} marginTop={30}
                  onPress={() => {
                    navigation.navigate('PersonalInfo')
                    screenProfileData()
                  }} />
                <BackgroundHideButton title={'SKIP'} width={'100%'} height={50} textColor={Colors.accentMagenta} marginTop={10}
                  onPress={() => {
                    navigation.navigate('VideoWizwedJobs')
                    screenData()
                  }} />
              </View>
            </ImageBackground>
          </ImageBackground>}
          {!show && <View style={{
            flex: 1,
            width: '100%',
            height: 50,
            position: 'absolute',
            marginBottom: 40,
            bottom: 0,
            paddingEnd: 30,
            paddingStart: 30
          }}>
            <CommanButton title='Schedule Interview' width={'100%'} height={50} backgroundColor={Colors.accentMagenta} borderRadius={20} textColor={Colors.white}
              onPress={() => {
                navigation.navigate('PersonalInfo')
                screenProfileData()
              }} />
          </View>}
        </View>
      </Modal>
    </View>
  );
}

export default GetRewardsScreen;