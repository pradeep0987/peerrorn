import { View, Text, StyleSheet, Image, TouchableOpacity, ToastAndroid, Pressable, ScrollView, Alert, Platform } from 'react-native';
import CommanTextInput from '../../components/comman/CommanTextInput';
import { Colors } from '../../src/colors/Colors';
import CommanButton from '../../components/comman/CommanButton';
import { useState, useEffect, useRef } from 'react';
import { isVaildEmail, isVaildPassword } from '../../src/utils/Utils';
import CommanMessage, { showWarningMessage } from '../../components/comman/CommanMassage';
import { useNavigation, useRoute } from '@react-navigation/native';
import LoadingScreen from '../LoadingScreens/LoadingScreen';
import { RSA } from 'react-native-rsa-native';
import axios from 'axios';
import { SignIn } from '../../restapi/Services';
import AsyncStorage from '@react-native-async-storage/async-storage';
import netInfo from '@react-native-community/netinfo'
import Toast from 'react-native-root-toast';
import TimeZone from 'react-native-timezone';

const PUBLIC_KEY =
    `-----BEGIN PUBLIC KEY-----
MIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEA3N5ALKr1kxDM1QVWJPpt
Z9TqQDny/oSyCPsC7qAKM2U2h1U9wx3YukkZQdLC6W8oVLGVjg33i2ghP4XFNN1w
Q97Bb2OVddsGcXWQbzfDuatUXRahTpqQgGDMedRJvY4NNV5VGs4eUNrFrzhZ78iN
n9AFNUJZp0MGakPEIQ79UAofUa6i4jtDFCDAfvH2AgM2S/D6OwppgYZsiy2SZOU8
3kSc3krsFGZx3RkWojC5oolnbgluMGOFsZECY+dVRAm9MNbRCqk2PQ7TysSsQtxt
+mz8syaVULOwWnbzPi5wBl2+QBokmsJbM0Nz3JkMt7QIB0+0dEPAz422rMmPqBD0
K/Wa7HJ9Jk5bxaevfmnobcJ5O/FBOkexjDK1FOW12LI6zWksOThsQHQvCVnkaUWu
IK5PGxPjnD9lUVCPdZAbzuDePZPobIH+MKOayzmLOS3D7I93uEDYM/DvtHqopZcN
DZAiHSZYelFOjyai3ER27yIcABpRsjOLR1G4iJAg6K2RzQ0TjNrukrBIh0ForNAA
Vjz73lN2PlDm7m0+ALSUhpHG9sXtZ1RIErE7Gd8mUwUA6+bi+XuW22JxTegSpO2i
MXL90e5CVtGcHwQPGO7vLw/JQiTCxWRP7RzWns5rPXYPB+rNJ31lZpEq/Vjc5RPW
lpGlaGCbu8+zxWDXo7Yxiw0CAwEAAQ==
-----END PUBLIC KEY-----`

function SignInScreen() {
    const navigation = useNavigation();
    const route = useRoute();
    const emailInputRef = useRef(null);
    const passwordInputRef = useRef(null);
    const [loading, setLoading] = useState(false);
    const [visible, setVisible] = useState(true);
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [show, setShow] = useState(false);
    const [showWarning, setShowWarning] = useState('');
    const video = route.params?.onBoardInfo;

    async function handleOnSignIn() {
        netInfo.fetch().then(async (response) => {
            if (response.isConnected) {
                if (email === '') {
                    setShow(true);
                    setShowWarning('Please enter your email address');
                } else if (isVaildEmail(email)) {
                    setShow(true);
                    setShowWarning('Please enter a valid email address');
                } else if (password === '') {
                    setShow(true);
                    setShowWarning('Please enter your password');
                } else if (isVaildPassword(password)) {
                    setShow(true);
                    setShowWarning(`Password must contain minimum 6 characters and must contain at least 1 digit & 1 alphabets`);
                } else {

                    const encryptPassword = await RSA.encrypt(password, PUBLIC_KEY);
                    console.log(encryptPassword);

                    const timeZone = await TimeZone.getTimeZone().then(zone => zone);
                    console.log(timeZone);

                    let data = JSON.stringify({
                        "userdata": {
                            "email_login": email,
                            "password": encryptPassword,
                            "fcm_token": "xyz",
                            "app_version": "3.2.5",
                            "osType": "android",
                            "timeZone": timeZone
                        }
                    });

                    setLoading(true)
                    const response = await SignIn(data);
                    if (response.data != null) {
                        setLoading(true)
                        console.log(response.data)
                        if (response.data.status) {
                            setShow(false)
                            setEmail('')
                            setPassword('')
                            setLoading(false)
                            CommanMessage(response.data.message)
                            storeData(response.data)
                        } else {
                            setShow(false)
                            setLoading(false)
                            CommanMessage(response.data.message)
                        }
                    }
                }
            } else {
                if (Platform.OS === 'android') {
                    ToastAndroid.show('No Internet Connection..!!', ToastAndroid.SHORT);
                } else {
                    Alert.alert('No Internet Connection..!!');
                }
            }
        })
    }

    const storeData = async (_response) => {
        try {
            const allResponse = JSON.stringify(_response)
            const userDate = JSON.stringify(_response.userdata)
            await AsyncStorage.setItem('userdata', userDate)
            await AsyncStorage.setItem('response', allResponse)
            await AsyncStorage.setItem('screenState', '7')
            navigation.navigate('DashBoard')
        } catch (e) {
            console.log(e);
        }
    }

    function handlePasswordVisible() {
        if (visible) {
            setVisible(false);
        } else {
            setVisible(true);
        }
    }

    function handleForgotPassword() {
        setShow(false)
        setEmail("")
        setPassword("")
        setVisible(false)
        navigation.navigate('SignMain', { onBoardInfo: video, show: true })
    }


    return (
        <View style={{ flex: 1 }}>
            <View style={{ flex: 1, marginTop: Platform.OS === 'ios' ? 40 : 0 }}>
                <View style={{ padding: 20 }}>
                    <TouchableOpacity style={{ width: 20, height: 20 }} onPress={handleForgotPassword}>
                        <Image style={{ width: 20, height: 20 }} source={require('../../src/images/arrow-back.png')} />
                    </TouchableOpacity>
                </View>
                <View style={{ width: '100%', alignItems: 'center', justifyContent: 'center', marginTop: 30 }}>
                    <View style={{ width: 100, height: 100, alignItems: 'center', justifyContent: 'center', backgroundColor: Colors.colorPrimary, borderRadius: 10 }}>
                        <Image style={{ width: 65, height: 78 }} source={require('../../src/images/peerro_branding_logo.png')} />
                    </View>
                </View>
                {show && <View style={{ flexDirection: 'row', backgroundColor: Colors.accentMagenta, marginTop: 10, alignItems: 'center', paddingEnd: 30, paddingStart: 5, paddingBottom: 5, paddingTop: 5, justifyContent: 'center' }}>
                    <View style={{ transform: [{ rotate: '180deg' }], marginStart: 10 }}>
                        <Image style={{ width: 20, height: 20, tintColor: Colors.white }} source={require('../../src/images/warning.png')} />
                    </View>
                    <Text style={{ color: Colors.white, fontSize: 16, marginStart: 10, marginEnd: 20, fontFamily: 'ws_regular' }}>{showWarning}</Text>
                </View>}
                <View style={{ padding: 30, marginTop: 10 }}>
                    <CommanTextInput
                        inputRef={emailInputRef}
                        title='Email'
                        height={50}
                        backgroundColor={Colors.white}
                        marginTopInput={5}
                        borderRadius={10}
                        textColor={Colors.blue}
                        Value={email}
                        OnChangeText={(email) => setEmail(email)}
                        keyboardType={"email-address"}
                        paddingStart={15}
                        onSubmitEditing={() => passwordInputRef.current.focus()}
                        returnKeyType={'next'}
                    />
                    <View>
                        <CommanTextInput
                            inputRef={passwordInputRef}
                            title='Password'
                            height={50}
                            backgroundColor={Colors.white}
                            marginTopInput={5} borderRadius={10}
                            textColor={Colors.blue}
                            marginTop={25}
                            Value={password}
                            OnChangeText={(password) => setPassword(password)}
                            visible={visible}
                            paddingStart={15}
                            paddingEnd={60}
                            returnKeyType={'done'}
                        />
                        <Pressable style={{ width: 30, height: 50, position: 'absolute', end: 0, bottom: 0, marginBottom: -15, marginEnd: 20 }} onPress={handlePasswordVisible}>
                            <Image style={{ width: 30, height: 20, opacity: 0.7 }} source={visible ? require('../../src/images/see-password.png') : require('../../src/images/hide-passwod.png')} />
                        </Pressable>
                    </View>

                    <View style={{ marginTop: 10, }}>
                        <Pressable onPress={() => navigation.navigate('ForgotPassword')}>
                            <Text style={{ fontSize: 16, alignSelf: 'flex-end', marginEnd: 10, fontFamily: 'ws_regular', color: Colors.colorBlack }} >Forgot Password ?</Text>
                        </Pressable>
                    </View>

                    <CommanButton title={'SIGN IN'} width={'100%'} height={50} textColor={Colors.white} backgroundColor={Colors.accentMagenta} borderRadius={100} marginTop={20}
                        onPress={handleOnSignIn} />
                </View>
                <View style={{ width: '100%', height: 30, marginBottom: 5, alignItems: 'center', marginTop: show ? 150 : 220 }}>
                    <Pressable style={{ flexDirection: 'row', marginBottom: 5 }} onPress={() => navigation.navigate('SignUp', { screen: 'SignIn', onBoardInfo: video, show: true })}>
                        <Text style={{ fontSize: 16, alignSelf: 'center', marginEnd: 5, fontFamily: 'ws_regular' }} >Don't have an account?</Text>
                        <Text style={{ fontSize: 16, alignSelf: 'flex-end', marginEnd: 10, fontWeight: '600', fontFamily: 'ws_medium' }} >SIGN UP</Text>
                    </Pressable>
                </View>
            </View>
            {loading && <LoadingScreen />}
        </View>
    );
}

export default SignInScreen;