import { View, StyleSheet, Modal, ImageBackground, Text, ToastAndroid, Alert, TouchableOpacity, useWindowDimensions, Platform } from 'react-native';
import BackgroundHideButton from '../../components/comman/BackgroundHideButton';
import CommanButton from '../../components/comman/CommanButton';
import { Colors } from '../../src/colors/Colors';
import { useRoute } from '@react-navigation/native';
import WebView from 'react-native-webview';
import { useRef, useEffect, useState } from 'react';
import { getOnBoardService } from '../../restapi/Services';
import LoadingScreen from '../LoadingScreens/LoadingScreen';
import VimeoPlayer from '../../components/VimeoPlayer/VimeoPlayer';
import NetInfo from '@react-native-community/netinfo';
import CommanMassage from '../../components/comman/CommanMassage.js';
import ToastMessage from '../../components/comman/CommanToastMassage';

function SignMainScreen({ navigation }) {
    const [video, setVideo] = useState();
    const route = useRoute();
    const video_uri = route.params?.onBoardInfo
    const show = route.params?.show
    const width = useWindowDimensions().width.toFixed();
    const height = useWindowDimensions().height.toFixed();

    useEffect(() => {
        checkNetwork();
    })
    async function checkNetwork() {
        NetInfo.fetch().then(async (state) => {
            if (state.isConnected) {
                const response = await getOnBoardService();
                if (response) {
                    if (response.data != null) {
                        if (response.data.status) {
                            setVideo(response.data.onboard_info[0].vimeo_embed_url)
                        }
                    }
                }
            } else {
                CommanMassage('No Internet Connection..!!')
            }
        })
    }

    return (
        <View style={{ flex: 1 }}>
            <VimeoPlayer video_url={video_uri ? video_uri : video} height={950} width={500} />
            <View style={styles.bottomView}>
                <TouchableOpacity style={{ backgroundColor: Colors.transparent, borderRadius: 100, borderWidth: 2, borderColor: Colors.white, height: 40, width: '49%', justifyContent: 'center', alignItems: 'center' }}
                    onPress={() => navigation.navigate('SignIn', { onBoardInfo: video })}>
                    <Text style={{ color: Colors.white, fontFamily: 'ws_semibold', fontWeight: 'bold'  }}>SIGN IN</Text>
                </TouchableOpacity>
                <TouchableOpacity style={{ backgroundColor: Colors.white, borderRadius: 100, borderWidth: 2, borderColor: Colors.white, height: 40, width: '49%', justifyContent: 'center', alignItems: 'center' }}
                    onPress={() => navigation.navigate('SignUp', { onBoardInfo: video })}>
                    <Text style={{ color: Colors.accentMagenta, fontFamily: 'ws_semibold', fontWeight: 'bold' }}>SIGN UP</Text>
                </TouchableOpacity>
                {/* <BackgroundHideButton title='SIGN IN' width={170} height={40} borderColor={Colors.white} borderWidth={2} borderRadius={20} textColor={Colors.white} onPress={() => navigation.navigate('SignIn',{onBoardInfo : video})}/> */}
                {/* <CommanButton title='SIGN UP' width={170} height={40} backgroundColor={Colors.white} borderRadius={20} textColor={Colors.accentMagenta} onPress={() => navigation.navigate('SignUp',{onBoardInfo : video})}/> */}
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    MainContainer: {
        padding: 20,
        alignItems: 'center',
        justifyContent: 'center',
    },

    bottomView: {
        width: '100%',
        height: 50,
        flexDirection: 'row',
        justifyContent: 'space-around',
        marginBottom: 35,
        position: 'absolute',
        bottom: 0,
        padding: 20
    }
})

export default SignMainScreen;