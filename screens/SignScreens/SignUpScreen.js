import { View, Text, StyleSheet, Image, TouchableOpacity, Platform, ToastAndroid, Pressable, Alert } from 'react-native';
import CommanTextInput from '../../components/comman/CommanTextInput';
import { Colors } from '../../src/colors/Colors';
import CommanButton from '../../components/comman/CommanButton';
import { useState, useEffect, useRef } from 'react';
import { isVaildEmail, isVaildPassword } from '../../src/utils/Utils';
import CommanMessage, { showWarningMessage } from '../../components/comman/CommanMassage';
import { useRoute } from '@react-navigation/native';
import LoadingScreen from '../LoadingScreens/LoadingScreen';
import { RSA } from 'react-native-rsa-native';
import { signUp } from '../../restapi/Services';
import TimeZone from 'react-native-timezone';
import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';

const BASE_URL = "https://dev.peerro.com/api_v13/";

const termsAndCondition = 'https://peerro.com/terms-and-conditions/'

const PUBLIC_KEY =
    `-----BEGIN PUBLIC KEY-----
MIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEA3N5ALKr1kxDM1QVWJPpt
Z9TqQDny/oSyCPsC7qAKM2U2h1U9wx3YukkZQdLC6W8oVLGVjg33i2ghP4XFNN1w
Q97Bb2OVddsGcXWQbzfDuatUXRahTpqQgGDMedRJvY4NNV5VGs4eUNrFrzhZ78iN
n9AFNUJZp0MGakPEIQ79UAofUa6i4jtDFCDAfvH2AgM2S/D6OwppgYZsiy2SZOU8
3kSc3krsFGZx3RkWojC5oolnbgluMGOFsZECY+dVRAm9MNbRCqk2PQ7TysSsQtxt
+mz8syaVULOwWnbzPi5wBl2+QBokmsJbM0Nz3JkMt7QIB0+0dEPAz422rMmPqBD0
K/Wa7HJ9Jk5bxaevfmnobcJ5O/FBOkexjDK1FOW12LI6zWksOThsQHQvCVnkaUWu
IK5PGxPjnD9lUVCPdZAbzuDePZPobIH+MKOayzmLOS3D7I93uEDYM/DvtHqopZcN
DZAiHSZYelFOjyai3ER27yIcABpRsjOLR1G4iJAg6K2RzQ0TjNrukrBIh0ForNAA
Vjz73lN2PlDm7m0+ALSUhpHG9sXtZ1RIErE7Gd8mUwUA6+bi+XuW22JxTegSpO2i
MXL90e5CVtGcHwQPGO7vLw/JQiTCxWRP7RzWns5rPXYPB+rNJ31lZpEq/Vjc5RPW
lpGlaGCbu8+zxWDXo7Yxiw0CAwEAAQ==
-----END PUBLIC KEY-----`

function SignUpScreen({ navigation }) {

    const route = useRoute();
    const emailInputRef = useRef(null);
    const passwordInputRef = useRef(null);
    const zipCodeInputRef = useRef(null);
    const [visible, setVisible] = useState(true);
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [zipcode, setZipCode] = useState('');
    const [show, setShow] = useState(false);
    const [showWarning, setShowWarning] = useState('');
    const [counter, setTimer] = useState(5);
    const [loading, setLoading] = useState(false);
    const screen = route.params?.screen;
    const video = route.params?.onBoardInfo;
    const inputFieldRef = useRef();

    async function onPressNextHandler() {
        if (email === '') {
            setShow(true);
            setShowWarning('Please enter your email address');
        } else if (isVaildEmail(email)) {
            setShow(true);
            setShowWarning('Please enter a valid email address');
        } else if (password === '') {
            setShow(true);
            setShowWarning('Please enter your password');
        } else if (isVaildPassword(password)) {
            setShow(true);
            setShowWarning(`Password must contain minimum 6 characters and must contain at least 1 digit & 1 alphabets`);
        } else if (zipcode === '') {
            setShow(true);
            setShowWarning('Please enter zip code');
        } else if (zipcode.length !== 5 || zipcode.length < 5) {
            setShow(true);
            setShowWarning('Zipcode should be 5 digit');
        } else {
            console.log(email, password, zipcode)
            const encryptPassword = await RSA.encrypt(password, PUBLIC_KEY);
            console.log(encryptPassword);

            const timeZone = await TimeZone.getTimeZone().then(zone => zone);
            console.log(timeZone);

            let data = JSON.stringify({
                "userdata": {
                    "email_login": email,
                    "password": encryptPassword,
                    "zip": zipcode,
                    "fcm_token": "xyz",
                    "app_version": "3.2.5",
                    "osType": "android",
                    "timeZone": timeZone
                }
            });

            setLoading(true)

            const response = await signUp(data);
            if (response.data != null) {
                setLoading(true)
                if (response.data.status) {
                    setShow(false)
                    setEmail('')
                    setPassword('')
                    setZipCode('')
                    setVisible(false)
                    setLoading(false)
                    allResponseData(response.data)
                    storeAdvertisement(response.data.advertisement)
                    storeMixPanelData(response.data.mixPanelData)
                    const user = response.data.userData
                    userData(user)
                    CommanMessage(response.data.message)
                } else {
                    setShow(false)
                    setLoading(false)
                    CommanMessage(response.data.message)
                }
            }
        }

    }

    const userData = async (data) => {
        try {
            const userList = JSON.stringify(data)
            await AsyncStorage.setItem('userdata', userList)
            await AsyncStorage.setItem('screenState', '1')
            navigation.navigate('EarnPoints')
        } catch (e) {
            console.log(e);
        }
    }

    const allResponseData = async (_response) => {
        try {
            const allResponse = JSON.stringify(_response)
            await AsyncStorage.setItem('response', allResponse)
            navigation.navigate('EarnPoints')
        } catch (e) {
            console.log(e);
        }
    }

    const storeAdvertisement = async (advertisement) => {
        try {
            const advertisementData = JSON.stringify(advertisement)
            await AsyncStorage.setItem('advertisement', advertisementData)
            navigation.navigate('EarnPoints')
        } catch (e) {
            console.log(e);
        }
    }

    const storeMixPanelData = async (mixPanelData) => {
        try {
            const mixPanelDataAl = JSON.stringify(mixPanelData)
            await AsyncStorage.setItem('mixPanelData', mixPanelDataAl)
            navigation.navigate('EarnPoints')
        } catch (e) {
            console.log(e);
        }
    }

    function handleBackNavigation() {
        if (screen === 'SignIn') {
            navigation.navigate('SignIn', { onBoardInfo: video, show: true })
        } else {
            navigation.navigate('SignMain', { onBoardInfo: video, show: true })
        }
    }

    function handlePasswordVisible() {
        if (visible) {
            setVisible(false);
        } else {
            setVisible(true);
        }
    }

    return (
        <View style={{ flex: 1, marginTop: Platform.OS === 'ios' ? 40 : 0 }}>
            <View style={{ padding: 20 }}>
                <TouchableOpacity style={{ width: 20, height: 20 }} onPress={() => handleBackNavigation()}>
                    <Image style={{ width: 20, height: 20 }} source={require('../../src/images/arrow-back.png')} />
                </TouchableOpacity>
            </View>
            <View>
                <Text style={{ fontSize: 24, alignSelf: 'center', fontFamily: 'ws_medium', color: Colors.colorBlack }} >Let's get started</Text>
                <Text style={{ fontSize: 16, alignSelf: 'center', marginTop: 15, marginEnd: 50, marginStart: 50, textAlign: 'center', fontFamily: 'ws_regular', color: Colors.colorBlack }} >To create an account with Peerro,{'\n'}we need your email address</Text>
            </View>
            {show && <View style={{ flexDirection: 'row', backgroundColor: Colors.accentMagenta, marginTop: 10, alignItems: 'center', paddingEnd: 30, paddingStart: 5, paddingBottom: 5, paddingTop: 5 }}>
                <View style={{ transform: [{ rotate: '180deg' }], marginStart: 10 }}>
                    <Image style={{ width: 20, height: 20, tintColor: Colors.white }} source={require('../../src/images/warning.png')} />
                </View>
                <Text style={{ color: Colors.white, fontSize: 16, marginStart: 10, marginEnd: 20, fontFamily: 'ws_regular' }}>{showWarning}</Text>
            </View>}
            <View style={styles.mainConstrain}>
                <CommanTextInput
                    inputRef={emailInputRef}
                    title='Email'
                    height={50}
                    backgroundColor={Colors.white}
                    marginTopInput={5}
                    borderRadius={10}
                    textColor={Colors.blue}
                    Value={email}
                    OnChangeText={(email) => setEmail(email)}
                    keyboardType={'email-address'}
                    paddingStart={15}
                    onSubmitEditing={() => passwordInputRef.current.focus()}
                    returnKeyType={'next'}
                />
                <View>
                    <CommanTextInput
                        inputRef={passwordInputRef}
                        title='Password'
                        height={50}
                        backgroundColor={Colors.white}
                        marginTopInput={5} borderRadius={10}
                        textColor={Colors.blue}
                        marginTop={25}
                        Value={password}
                        OnChangeText={(password) => setPassword(password)}
                        visible={visible}
                        paddingStart={15}
                        paddingEnd={60}
                        returnKeyType={'next'}
                        onSubmitEditing={() => zipCodeInputRef.current.focus()}
                    />
                    <Pressable style={{ width: 30, height: 20, position: 'absolute', end: 0, bottom: 0, marginBottom: 15, marginEnd: 20 }} onPress={handlePasswordVisible}>
                        <Image style={{ width: 30, height: 20, opacity: 0.7 }} source={visible ? require('../../src/images/see-password.png') : require('../../src/images/hide-passwod.png')} />
                    </Pressable>
                </View>
                <CommanTextInput
                    inputRef={zipCodeInputRef}
                    title='ZipCode'
                    height={50}
                    backgroundColor={Colors.white}
                    marginTopInput={5}
                    borderRadius={10}
                    textColor={Colors.blue}
                    marginTop={25}
                    Value={zipcode}
                    OnChangeText={(zipcode) => setZipCode(zipcode)}
                    keyboardType={'numeric'}
                    maxLength={5}
                    paddingStart={15}
                    returnKeyType={'done'}
                />

                <View style={{ padding: 20, marginTop: 10 }}>
                    <Text style={{ fontSize: 16, alignSelf: 'center', fontFamily: 'ws_regular', color: Colors.colorBlack }} >By create an account, you agree to our</Text>
                    <TouchableOpacity style={{ width: '100%', alignItems: 'center', justifyContent: 'center', fontFamily: 'ws_regular' }} onPress={() => navigation.navigate('TermAndConditiond', { termsAndCondition: termsAndCondition })}>
                        <Text style={{ fontSize: 16, color: Colors.blue, marginTop: 5, fontFamily: 'ws_regular' }}>Terms and Conditions</Text>
                    </TouchableOpacity>
                </View>

                <CommanButton title={'Next'} width={'100%'} height={50} textColor={Colors.white} backgroundColor={Colors.accentMagenta} borderRadius={100} marginTop={10}
                    onPress={onPressNextHandler} />

            </View>
            {loading && <LoadingScreen />}
        </View>
    );
}

const styles = StyleSheet.create({
    mainConstrain: {
        flex: 1,
        padding: 30,
        marginTop: 10,
    }
})

export default SignUpScreen;