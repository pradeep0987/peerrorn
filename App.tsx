import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { StatusBar } from 'react-native';
import React from 'react';
import SignMainScreen from './screens/SignScreens/SignMainScreen';
import { Colors } from './src/colors/Colors';
import SignUpScreen from './screens/SignScreens/SignUpScreen';
import SignInScreen from './screens/SignScreens/SignInScreen';
import TermAndConditions from './screens/TermAndCondition/TermAndCondition';
import ProfileInfoScreen from './screens/ProfileInfo/ProfileInfoScreen';
import ForgotPassword from './screens/ForgotPassword/ForgotPassword';
import ResendScreen from './screens/ForgotPassword/Resend';
import EarnPointsScreen from './screens/EarnPoints/WeeklyPoints';
import GetMorePointScreen from './screens/EarnPoints/GetMorePointsScreen';
import GetRewardsScreen from './screens/EarnPoints/GetRewards';
import VideoWizwedJobsScreen from './screens/VideoWizwedJobs/VideoWizwedJobs';
import OnBoardVideoScreen from './screens/OnBoardVideo/OnBoardVideoScreen';
import CareerWizerdScreen from './screens/CareerWizerd/CareerWizerdScreen';
import DashBoardScreen from './screens/DashBoard/DashBoradScreen';
import WelcomeScreen from './screens/welcome/WelcomeScreen';
import SplaseScreen from './screens/SplaseScreen/SplaseScreen';
import CareerWizerdMainScreen from './screens/CareerWizerd/CareerWizerdMain';
import CareerWizerdCarScreen from './screens/CareerWizerd/CareerWizerdCar';
import CareerWizerdFamilyScreen from './screens/CareerWizerd/CareerWizerdFamily';
import CareerWizerdHomeScreen from './screens/CareerWizerd/CareerWizerdHome';
import CareerWizerdSalaryGoalScreen from './screens/CareerWizerd/CareerWizardSalaryGoal';
import CareerWizerdConfirmSalaryGoal from './screens/CareerWizerd/CareerWizardConfirmSalaryGoal';
import KeepWatchingVideoScreen from './screens/OnBoardVideo/KeepWatchingScreen';
import CardSwapingScreen from './screens/CareerWizerd/CardSwapScreen';

const Stack = createNativeStackNavigator();

function App() {
  return (
    <>
      <StatusBar
        animated={true}
        backgroundColor={Colors.colorPrimary}
      />
      <NavigationContainer>
        <Stack.Navigator screenOptions={{headerShown: false}}>
          <Stack.Screen name='Splase' component={SplaseScreen}/>
          <Stack.Screen name='Welcome' component={WelcomeScreen}/>
          <Stack.Screen name='SignMain' component={SignMainScreen}/>
          <Stack.Screen name='SignUp' component={SignUpScreen}/>
          <Stack.Screen name='SignIn' component={SignInScreen}/>
          <Stack.Screen name='TermAndConditiond' component={TermAndConditions}/>
          <Stack.Screen name='PersonalInfo' component={ProfileInfoScreen}/>
          <Stack.Screen name='ForgotPassword' component={ForgotPassword}/>
          <Stack.Screen name='Resend' component={ResendScreen}/>
          <Stack.Screen name='EarnPoints' component={EarnPointsScreen}/>
          <Stack.Screen name='GetMorePoint' component={GetMorePointScreen}/>
          <Stack.Screen name='GetRewards' component={GetRewardsScreen}/>
          <Stack.Screen name='VideoWizwedJobs' component={VideoWizwedJobsScreen}/>
          <Stack.Screen name='onBoardVideos' component={OnBoardVideoScreen}/>
          <Stack.Screen name='CareerWizerd' component={CareerWizerdScreen}/>
          <Stack.Screen name='DashBoard' component={DashBoardScreen}/>
          <Stack.Screen name='CareerWizerdMain' component={CareerWizerdMainScreen}/>
          <Stack.Screen name='CareerWizerdCar' component={CareerWizerdCarScreen}/>
          <Stack.Screen name='CareerWizerdFamily' component={CareerWizerdFamilyScreen}/>
          <Stack.Screen name='CareerWizerdHome' component={CareerWizerdHomeScreen}/>
          <Stack.Screen name='CareerWizerdSalaryGoal' component={CareerWizerdSalaryGoalScreen}/>
          <Stack.Screen name='CareerWizerdConfirmSalaryGoal' component={CareerWizerdConfirmSalaryGoal}/>
          <Stack.Screen name='KeepWatching' component={KeepWatchingVideoScreen}/>
          <Stack.Screen name='CardView' component={CardSwapingScreen}/>
        </Stack.Navigator>
      </NavigationContainer>
    </>
  );
}

export default App;
