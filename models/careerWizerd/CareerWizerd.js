export class CareerWizerd {
    constructor (typeData)
    {
        this.typeData = typeData
        this.details = careerWizerdDetail
    }
}

export class careerWizerdDetail {
    constructor (career_id, career_type, career_label, career_sub_label, career_image)
    {
        this.career_id = career_id
        this.career_type = career_type
        this.career_label = career_label
        this.career_sub_label = career_sub_label
        this.career_image = career_image
    }
}