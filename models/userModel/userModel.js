export class userData {
    constructor(user_id, device_id, last_name, country, state, city, zip, address, date_of_birth, email_login, email, phone, password, latlong, session_token, photo_url, fcm_token, is_company, fb_token, fb_account_id, emailValidation, app_version, osType, version, referral_code, is_notification, timeZone, short_intro) 
    {
        this.city = city
        this.country = country
        this.email = email
        this.fb_account_id = fb_account_id
        this.first_name = first_name
        this.is_company = is_company
        this.is_notification = is_notification
        this.last_login = last_login
        this.last_name = last_name
        this.photo_url = photo_url
        this.session_token = session_token
        this.state = state
        this.user_id = user_id
        this.version = version
        this.zip = zip
        this.device_id = device_id
        this.address = address
        this.date_of_birth = date_of_birth
        this.phone = phone
        this.email_login = email_login
        this.password = password
        this.latlong = latlong
        this.fcm_token = fcm_token
        this.osType = osType
        this.app_version = app_version
        this.referral_code = referral_code
        this.short_intro = short_intro
        this.fb_token = fb_token
        this.emailValidation = emailValidation
        this.timeZone = timeZone
    }
}