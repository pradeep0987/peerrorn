export class MixPanelData {
    constructor (JobCity, JobID, JobInterviewType, JobTitle, JobZipCode, OrgCity, OrgContact, OrgEmail, OrgID, OrgName, OrgState, OrgZipCode, OtherUserCity, OtherUserDoB, OtherUserEmail, OtherUserID, OtherUserMobile, OtherUserName, OtherUserState, OtherUserZipCode, TrainingID, TrainingTitle, TrainingType, UserCity, UserDoB, UserEmail, UserID, UserMobile, UserName, UserState, UserZipCode)
    {
        this.JobCity = JobCity
        this.JobID = JobID
        this.JobInterviewType = JobInterviewType
        this.JobTitle = JobTitle
        this.JobZipCode = JobZipCode
        this.OrgCity = OrgCity
        this.OrgContact = OrgContact
        this.OrgEmail = OrgEmail
        this.OrgID = OrgID
        this.OrgName = OrgName
        this.OrgState = OrgState
        this.OrgZipCode = OrgZipCode
        this.OtherUserCity = OtherUserCity
        this.OtherUserDoB = OtherUserDoB
        this.OtherUserEmail = OtherUserEmail
        this.OtherUserID = OtherUserID
        this.OtherUserMobile = OtherUserMobile
        this.OtherUserName = OtherUserName
        this.OtherUserState = OtherUserState
        this.OtherUserZipCode = OtherUserZipCode
        this.TrainingID = TrainingID
        this.TrainingTitle = TrainingTitle
        this.TrainingType = TrainingType
        this.UserCity = UserCity
        this.UserDoB = UserDoB
        this.UserEmail = UserEmail
        this.UserID = UserID
        this.UserMobile = UserMobile
        this.UserName = UserName
        this.UserState = UserState
        this.UserZipCode = UserZipCode
    }
}