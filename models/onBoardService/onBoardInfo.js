import { MixPanelData } from "../mixPanelData/mixPanelData"
import { referralData } from "../referralData/referralData"

export class onBoardInfoData {
    constructor (video_id, has_video, video_link, video_time, video_thumbnail, question_name, question_answer, vimeo_id, vimeo_link, vimeo_embed_url, question_option, referral_data)
    {
        this.video_id = video_id
        this.has_video = has_video
        this.video_link = video_link
        this.video_time = video_time
        this.video_thumbnail = video_thumbnail
        this.question_name = question_name
        this.question_answer = question_answer
        this.vimeo_id = vimeo_id
        this.vimeo_link = vimeo_link
        this.vimeo_embed_url = vimeo_embed_url
        this.question_option = question_option
        this.referral_data = referralData
    }
}

export class KeepWatchingResponse {
    constructor (message, onboarding_video_count, status)
    {
        this.message = message
        this.mixPanelData = MixPanelData
        this.onboard_info = onBoardInfoData
        this.onboarding_video_count = onboarding_video_count
        this.referral_data = referralData
        this.status = status
    }
}