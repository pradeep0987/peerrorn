export class Advertisement {
    constructor (Ad_City, Ad_EndDate, Ad_ID, Ad_Name, Ad_Priority, Ad_Screen, Ad_StartDate, Ad_Type, Ad_Upload_Date, Ad_Zip_Code, Answer, Attached_Job_ID, Attached_Job_Title, Org_City, Org_ID, Org_Name, Org_State, Org_ZipCode, Question, Target_Job, User_City, User_Email, User_ID, User_Name, User_State, User_ZipCode, advertisement_answers, advertisement_external_url, advertisement_file_embed, advertisement_file_id, advertisement_file_link, advertisement_file_time, advertisement_id, advertisement_name, advertisement_option, advertisement_priority, advertisement_questions, advertisement_template_type, advertisement_thumbnail, advertisement_type, age, company_id, company_logo, company_name, has_job, hours_week, job_id, job_title, pay_per_type) 
    {
        this.Ad_City = Ad_City
        this.Ad_EndDate = Ad_EndDate
        this.Ad_ID = Ad_ID
        this.Ad_Name = Ad_Name
        this.Ad_Priority = Ad_Priority
        this.Ad_Screen = Ad_Screen
        this.Ad_StartDate = Ad_StartDate
        this.Ad_Type = Ad_Type
        this.Ad_Upload_Date = Ad_Upload_Date
        this.Ad_Zip_Code = Ad_Zip_Code
        this.Answer = Answer
        this.Attached_Job_ID = Attached_Job_ID
        this.Attached_Job_Title = Attached_Job_Title
        this.Org_City = Org_City
        this.Org_ID = Org_ID
        this.Org_Name = Org_Name
        this.Org_State = Org_State
        this.Org_ZipCode = Org_ZipCode
        this.Question = Question
        this.Target_Job = Target_Job
        this.User_City = User_City
        this.User_Email = User_Email
        this.User_ID = User_ID
        this.User_Name = User_Name
        this.User_State = User_State
        this.User_ZipCode = User_ZipCode
        this.advertisement_answers = advertisement_answers
        this.advertisement_external_url = advertisement_external_url
        this.advertisement_file_embed = advertisement_file_embed
        this.advertisement_file_id = advertisement_file_id
        this.advertisement_file_link = advertisement_file_link
        this.advertisement_file_time = advertisement_file_time
        this.advertisement_id = advertisement_id
        this.advertisement_name = advertisement_name
        this.advertisement_option = advertisement_option
        this.advertisement_priority = advertisement_priority
        this.advertisement_questions = advertisement_questions
        this.advertisement_template_type = advertisement_template_type
        this.advertisement_thumbnail = advertisement_thumbnail
        this.advertisement_type = advertisement_type
        this.age = age
        this.company_id = company_id
        this.company_logo = company_logo
        this.company_name = company_name
        this.has_job = has_job
        this.hours_week = hours_week
        this.job_id = job_id
        this.job_title = job_title
        this.pay_per_type = pay_per_type
    }
}