
const emailPattern = "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" + "\\@" + "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" + "(" + "\\." + "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" + ")+";
const passwordPattern = "((?=.*\\d)(?=.*[a-z]).{6,20})";

export function isVaildPassword(password) {
    if (!password.match(passwordPattern)) {
        return true;
    } else {
        return false;
    }
}

export function isVaildEmail(email) {
    if (!email.match(emailPattern)) {
        return true;
    } else {
        return false;
    }
}
