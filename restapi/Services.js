import axios from 'axios';
import { ToastAndroid } from 'react-native';
import { useEffect, useState } from 'react';
import LoadingScreen from '../screens/LoadingScreens/LoadingScreen';
import AsyncStorage from '@react-native-async-storage/async-storage';

const BASE_URL = "https://dev.peerro.com/api_v13/"
const ZipRecruiterBase_Url = "https://api.ziprecruiter.com/"


export async function getOnBoardService() {
  return await axios.get(`${BASE_URL}onboarding/single`)
}

/// start of the user auth

export async function SignIn(data) {
  try {
    let config = {
      method: 'post',
      maxBodyLength: Infinity,
      url: `${BASE_URL}auth/login`,
      headers: {
        'Content-Type': 'application/json',
        'Cookie': 'ci_session_frontend=bqf629v50u85vufmalnthu6rrc3j3pd1'
      },
      data: data
    };

    const response = await axios.request(config)
    return response
  } catch (e) {
    console.log(e)
    return;
  }
}


export async function signUp(data) {
  try {
    let config = {
      method: 'post',
      maxBodyLength: Infinity,
      url: `${BASE_URL}auth/register`,
      headers: {
        'Content-Type': 'application/json',
        'Cookie': 'ci_session_frontend=bqf629v50u85vufmalnthu6rrc3j3pd1'
      },
      data: data
    };

    const response = await axios.request(config)
    return response
  } catch (e) {
    console.log(e)
    return;
  }
}

export async function forgotPassword(data) {
  try {
    let config = {
      method: 'post',
      maxBodyLength: Infinity,
      url: `${BASE_URL}auth/forgot_password`,
      headers: {
        'Content-Type': 'application/json',
        'Cookie': 'ci_session_frontend=bqf629v50u85vufmalnthu6rrc3j3pd1'
      },
      data: data
    };

    const response = await axios.request(config)
    return response
  } catch (e) {
    console.log(e)
    return;
  }
}

export async function profileUpdate(data) {
  try {
    const userData = await getData();
    let config = {
      method: 'post',
      maxBodyLength: Infinity,
      url: `${BASE_URL}profile/update`,
      headers: {
        'Content-Type': 'application/json',
        'Cookie': 'ci_session_frontend=bqf629v50u85vufmalnthu6rrc3j3pd1',
        'session_token': userData.session_token
      },
      data: data
    };

    const response = await axios.request(config)
    return response
  } catch (e) {
    console.log(e)
    return;
  }
}

export async function uploadMedia(fileUri, fileName) {
  try {
    const getdate = await getData();

    var myHeaders = new Headers();
    myHeaders.append('Content-Type', 'multipart/form-data;');
    myHeaders.append('session_token', `${getdate.session_token}`);


    let formData = new FormData();
    formData.append('user_id', getdate.user_id);
    formData.append('type', "profile");
    formData.append('id', getdate.user_id);

    formData.append('file', {
      uri: fileUri,
      type: 'image/png',
      name: fileName,
    });

    var requestOptions = {
      url: `${BASE_URL}profile/upload_media`,
      method: 'post',
      headers: myHeaders,
      body: formData
    };


    await fetch(url, requestOptions).then(response => response.json()).then(result => {
      return result
    })
  } catch (e) {
    console.log(e)
    return;
  }
}

export async function getProfileInfoData() {
  try {
    const userData = await getData();
    let config = {
      method: 'get',
      maxBodyLength: Infinity,
      url: `${BASE_URL}profile`,
      headers: {
        'session_token': `${userData.session_token}`,
        'Content-Type': 'application/json'
      },
      params: {
        user_id: userData.user_id,
        app_version: "3.2.5",
        osType: "android"
      },
    };

    const response = (await axios.request(config)).data
    return response
  } catch (e) {
    console.log(e)
    return;
  }
}

/// end of the user auth

/// start of the on board videos 

export async function getKeepWatching() {
  try {
    const userData = await getData();
    let config = {
      method: 'get',
      maxBodyLength: Infinity,
      url: `${BASE_URL}onboarding?user_id=${userData.user_id}&app_version=3.2.5&osType=android"`,
      headers: {
        'Content-Type': 'application/json',
        'Cookie': 'ci_session_frontend=bqf629v50u85vufmalnthu6rrc3j3pd1',
        'session_token': userData.session_token
      },
    };

    const response = await axios.request(config)
    return response
  } catch (e) {
    console.log(e)
    return;
  }
}

export async function getOnBoardingSingleVideo(video_id) {
  try {
    const userData = await getData();
    let config = {
      method: 'get',
      maxBodyLength: Infinity,
      url: `${BASE_URL}onboarding/singleVideo?video_id=${video_id}&user_id=${userData.user_id}`,
      headers: {
        'Content-Type': 'application/json',
        'Cookie': 'ci_session_frontend=bqf629v50u85vufmalnthu6rrc3j3pd1',
      },
    };

    const response = await axios.request(config)
    return response
  } catch (e) {
    console.log(e)
    return;
  }
}

export async function updateWatchOnBorading(data) {
  try {
    const userData = await getData();
    let config = {
      method: 'post',
      maxBodyLength: Infinity,
      url: `${BASE_URL}onboarding/update_watch`,
      headers: {
        'Content-Type': 'application/json',
        'Cookie': 'ci_session_frontend=bqf629v50u85vufmalnthu6rrc3j3pd1',
        'session_token': userData.session_token
      },
      data: data
    };

    const response = await axios.request(config)
    return response
  } catch (e) {
    console.log(e)
    return;
  }
}

export async function updateOnBorading(data) {
  console.log(data)
  try {
    const userData = await getData();
    let config = {
      method: 'post',
      maxBodyLength: Infinity,
      url: `${BASE_URL}onboarding/update`,
      headers: {
        'Content-Type': 'application/json',
        'Cookie': 'ci_session_frontend=bqf629v50u85vufmalnthu6rrc3j3pd1',
        'session_token': userData.session_token
      },
      data: data
    };

    const response = await axios.request(config)
    return response
  } catch (e) {
    console.log(e)
    return;
  }
}

/// end of the on board videos 

/// start of the career wizerd

export async function getCareerWizerd() {
  try {
    const userData = await getData();
    let config = {
      method: 'get',
      maxBodyLength: Infinity,
      url: `${BASE_URL}system_data/career_wizard`,
      headers: {
        'session_token': userData.session_token,
        'Cookie': 'ci_session_frontend=rl9j47k50squhf5o0coedf7q2q6v6gtm'
      },
      params: {
        user_id: userData.user_id,
      }
    };

    const response = await axios.request(config)
    return response
  } catch (e) {
    console.log(e)
    return;
  }
}

/// end of career wizerd

/// start of the jobs opportunities

export async function getOpportunities(jobtypes, distance, pay, search_text, isTraining, isJob, offset, limit, extJoboffset) {
  try {
    const userData = await getData();
    let config = {
      method: 'get',
      maxBodyLength: Infinity,
      url: `${BASE_URL}jobs`,
      headers: {
        'session_token': userData.session_token,
        'Cookie': 'ci_session_frontend=rl9j47k50squhf5o0coedf7q2q6v6gtm'
      },
      params: {
        user_id: userData.user_id,
        distance: distance,
        jobtypes: jobtypes,
        pay: pay,
        search_text: search_text,
        isTraining: isTraining,
        isJob: isJob,
        offset: offset,
        limit: limit,
        ext_joboffset: extJoboffset,
        app_version: "3.1.8",
        osType: "android",
      }
    };

    const response = await axios.request(config)
    return response.data;
  } catch (e) {
    console.log(e)
    return;
  }
}

/// end of the jobs opportunities

/// start of the jobs Zip Recruiter

export async function getZipRecruiter(offsetZipRecruiter, search , distence) {
  try {
    const userData = await getData();
    let config = {
      method: 'get',
      maxBodyLength: Infinity,
      url: `${ZipRecruiterBase_Url}jobs/v1`,
      headers: {
        'session_token': userData.session_token,
        'Cookie': 'ci_session_frontend=rl9j47k50squhf5o0coedf7q2q6v6gtm'
      },
      params: {
        search: search,
        location: `${userData.city}, ${userData.state}`,
        radius_miles: distence,
        days_ago: "",
        jobs_per_page: 10,
        page: offsetZipRecruiter,
        api_key: "wjhh8tvk82tuz45y26u7eg8xksgcdxf5"
      }
    };

    const response = await axios.request(config)
    return response.data;
  } catch (e) {
    console.log(e)
    return;
  }
}

/// start of the jobs Zip Recruiter

// start profile update salary

export async function updateProfileSalary(data) {
  try {
    const userData = await getData();
    let config = {
      method: 'post',
      maxBodyLength: Infinity,
      url: `${BASE_URL}profile/update_salary`,
      headers: {
        'session_token': `${userData.session_token}`,
        'Content-Type': 'application/json'
      },
      data: data,
    };

    const response = await axios.request(config)
    return response
  } catch (e) {
    console.log(e)
    return;
  }
}

// end of the profile update salary

// start of the update industry 

export async function updateIndustry(data) {
  try {
    const userData = await getData();
    let config = {
      method: 'post',
      maxBodyLength: Infinity,
      url: `${BASE_URL}profile/update_industry`,
      headers: {
        'session_token': `${userData.session_token}`,
        'Content-Type': 'application/json'
      },
      data: data,
    };

    const response = await axios.request(config)
    return response
  } catch (e) {
    console.log(e)
    return;
  }
}

// end of the update industry

// start of the logout

export async function userLogout() {
  try {
    const userData = await getData();
    let config = {
      method: 'get',
      maxBodyLength: Infinity,
      url: `${BASE_URL}auth/logout`,
      headers: { 
        'Cookie': 'ci_session_frontend=ast5q6dcjhorb4gn0mcitkcr5cm7mc19'
      },
      params: {
        user_id: userData.user_id,
        fcm_token: "xyz"
      }
    };

    const response = await axios.request(config)
    return response.data;
  } catch (e) {
    console.log(e)
    return;
  }
}

// end of the logout

/// user data from the local storege
const getData = async () => {
  try {
    const response = await AsyncStorage.getItem('userdata')
    return response != null ? JSON.parse(response) : null;
  } catch (e) {
    console.log(e)
  }
}