import { View, Text, StyleSheet, Image, TouchableOpacity, ToastAndroid, ImageBackground, Modal } from 'react-native';
import CommanTextInput from '../../components/comman/CommanTextInput';
import { Colors } from '../../src/colors/Colors';
import CommanButton from '../../components/comman/CommanButton';
import { useState, useEffect } from 'react';
import BackgroundHideButton from '../../components/comman/BackgroundHideButton';
import AsyncStorage from '@react-native-async-storage/async-storage';
import VimeoPlayer from '../../components/VimeoPlayer/VimeoPlayer';

function ImageAdsJobsList({ imageUri, companyLogo, age, hoursWeek, companyname }) {
    return (
        <View style={{ width: '100%', height: 600, borderRadius: 10, marginTop: 10, backgroundColor: Colors.colorCGrey }}>
            <Image style={{ height: 300, width: '100%', padding: 5 }} source={imageUri ? { uri: imageUri } : require('../../src/images/onboard_new_bg.png')}></Image>
            <View style={{ justifyContent: 'center', width: '100%', height: 300, position: 'absolute', bottom: 0, backgroundColor: Colors.adsLightTransparentGrey, }}>
                <View style={{ flexDirection: 'row', marginTop: 10, marginStart: 10, marginEnd: 10 , alignItems: 'center'}}>
                    <View style={{ width: 90, height: 90, borderRadius: 100, backgroundColor: Colors.white, justifyContent: 'center', alignItems: 'center' }}>
                        <Image style={{ width: 70, height: 70, alignSelf: 'center', borderRadius: 100, overflow: 'hidden' }} source={{ uri: companyLogo }} />
                    </View>
                    <View style={{ marginStart: 10 }}>
                        <Text style={{ fontSize: 14, marginTop: 10, textAlign: 'left', fontFamily: 'ws_regular', color: Colors.accentMagenta }} >PROMOTED</Text>
                        <Text style={{ fontSize: 20, marginTop: 5, textAlign: 'left', fontWeight: '600', fontFamily: 'ws_medium', color: Colors.colorBlack, width: '90%' }} >{companyname} is offering{'\n'}you an interview!</Text>
                    </View>
                </View>
                {hoursWeek && age && <Text style={{ fontSize: 16, alignSelf: 'center', marginTop: 10, marginEnd: 50, marginStart: 50, textAlign: 'center', fontWeight: '500', fontFamily: 'ws_regular', fontFamily: 'ws_regular', color: Colors.colorBlack }} >
                    Do you want to schedule an{'\n'}interview for a job that pays{'\n'}${hoursWeek}/Hourly?</Text>}
                {hoursWeek && age && <Text style={{ fontSize: 16, alignSelf: 'center', marginTop: 10, textAlign: 'left', fontWeight: '400', fontFamily: 'ws_regular', color: Colors.colorBlack }} >Are you {age} years or older?</Text>}
                {!hoursWeek || !age && <Text style={{ fontSize: 16, alignSelf: 'center', marginTop: 20, textAlign: 'left', fontWeight: '400', fontFamily: 'ws_regular', color: Colors.colorBlack }} >Learn more about this jobs!</Text>}
                <View style={{paddingEnd: 20, paddingStart: 20, marginBottom: 10, marginTop: 5}} ><CommanButton title={'Schedule Interview'} width={'100%'} height={50} textColor={Colors.white} backgroundColor={Colors.accentMagenta} borderRadius={100} marginTop={30}/></View>
            </View>
        </View>
    );
}

export default ImageAdsJobsList;