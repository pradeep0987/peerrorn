import { View, Text, StyleSheet, Image, TouchableOpacity, ToastAndroid, ImageBackground, Modal } from 'react-native';
import CommanTextInput from '../../components/comman/CommanTextInput';
import { Colors } from '../../src/colors/Colors';
import CommanButton from '../../components/comman/CommanButton';
import { useState, useEffect } from 'react';
import BackgroundHideButton from '../../components/comman/BackgroundHideButton';
import AsyncStorage from '@react-native-async-storage/async-storage';
import VimeoPlayer from '../../components/VimeoPlayer/VimeoPlayer';


function VideoAdsJobsList({ videoUrl, videoThem, companyLogo, age, hoursWeek, companyname, videoTime, onPressBack }) {
    const [show, setShow] = useState(false);
    const [sliderValue, setSliderValue] = useState(0);
    const [vimeoTimer, setVimeoTimer] = useState('');
    const [onPlay , setOnPlay] = useState(false)

    function readVideo(state) {
        if (state === 'ended') {
            setShow(true)
        }
    }

    const handleVideoEvents = (event) => {
        let eventObj = JSON.parse(event.nativeEvent.data);
        let data = eventObj.data;
        let eventName = eventObj.name;

        switch (eventName) {
            case 'play':
              console.log('eventName :', eventName);
              setOnPlay(true)
              break;
            case 'pause':
              console.log('eventName :', eventName);
              break;
            case 'ended':
              console.log('eventName :', eventName);
              setShow(true)
              break;
            case 'timeupdate':
              setSliderValue(data.seconds);
              let totalLeftSec = data.duration - data.seconds;
              const dateObj = new Date(totalLeftSec * 1000);
              let hours = dateObj.getUTCHours();
              let minutes = dateObj.getUTCMinutes();
              let seconds = dateObj.getSeconds();
      
              let timeString =
                hours.toString().padStart(2, '0') +
                ':' +
                minutes.toString().padStart(2, '0') +
                ':' +
                seconds.toString().padStart(2, '0');
              setVimeoTimer(timeString);
      
              break;
            case 'slideTo':
              console.log('eventName :', eventName);
              break;
          }
    };


    return (
        <View style={{ width: '100%', height: 600, borderRadius: 10, marginTop: 10 }}>
            <ImageBackground style={{ flex: 1, justifyContent: 'center', overflow: 'hidden', borderRadius: 10 }} source={videoThem ? { uri: videoThem } : require('../../src/images/onboard_new_bg.png')}>
                {!show && <VimeoPlayer video_url={videoUrl} height={950} width={500} playerREF={readVideo} handleEvents={handleVideoEvents} />}
                {!show && <TouchableOpacity style={{width: '100%',padding: 20, position: 'absolute', marginBottom: 10, top: 0, flexDirection: 'row', justifyContent: 'space-between'}} 
                    >
                    <Text style={{fontFamily: 'ws_regular', color: Colors.white}}>{vimeoTimer}</Text>
                    {/* <Image style={{ width: 20, height: 20, tintColor: Colors.accentMagenta}} source={require('../../src/images/close-icon.png')} /> */}
                </TouchableOpacity>}
                {show &&
                    <View style={{ justifyContent: 'center', width: '100%', height: 350, position: 'absolute', bottom: 0, backgroundColor: Colors.adsLightTransparentGrey }}>
                        <View style={{ flexDirection: 'row', marginTop: 10 , marginStart: 10 , marginEnd: 10}}>
                            <View style={{ width: 90, height: 90, borderRadius: 100, backgroundColor: Colors.white, justifyContent: 'center', alignItems: 'center' }}>
                                <Image style={{ width: 70, height: 70, alignSelf: 'center', borderRadius: 100, overflow: 'hidden' }} source={{ uri: companyLogo }} />
                            </View>
                            <View style={{ marginStart: 5 }}>
                                <Text style={{ fontSize: 14, marginTop: 10, textAlign: 'left', fontFamily: 'ws_regular', color: Colors.accentMagenta }} >PROMOTED</Text>
                                <Text style={{ fontSize: 20, width: '90%', marginTop: 5, textAlign: 'left', fontWeight: '600', fontFamily: 'ws_medium', color: Colors.colorBlack }} >{companyname} is offering{'\n'}you an interview!</Text>
                            </View>
                        </View>
                        <Text style={{ fontSize: 16, marginTop: 10, marginEnd: 50, marginStart: 50, textAlign: 'center', fontWeight: '500', fontFamily: 'ws_regular', fontFamily: 'ws_regular', color: Colors.colorBlack }} >
                            Do you want to schedule an{'\n'}interview for a job that pays{'\n'}${hoursWeek}/Hourly?</Text>
                        <Text style={{ fontSize: 16, alignSelf: 'center', marginTop: 10, textAlign: 'left', fontWeight: '400', fontFamily: 'ws_regular', color: Colors.colorBlack }} >Are you {age} years or older?</Text>
                        <View style={{paddingEnd: 20 , paddingStart: 20, marginTop: 20}}><CommanButton title={'Schedule Interview'} width={'100%'} height={50} textColor={Colors.white} backgroundColor={Colors.accentMagenta} borderRadius={100} marginTop={30}
                        /></View>
                    </View>
                }
                {!show && <View style={{
                    flex: 1,
                    width: '100%',
                    height: 50,
                    position: 'absolute',
                    marginBottom: 20,
                    bottom: 0,
                    paddingEnd: 30,
                    paddingStart: 30
                }}>
                    <CommanButton title='Schedule Interview' width={'100%'} height={50} backgroundColor={Colors.accentMagenta} borderRadius={20} textColor={Colors.white} />
                </View>}
            </ImageBackground>
        </View>
    );
}

export default VideoAdsJobsList;