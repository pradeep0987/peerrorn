import { View, TouchableOpacity, Text, Alert, Pressable } from "react-native";

const CommanButton = (props) => {
    return (
        <View style={{marginTop: props.marginTop}}>
            <TouchableOpacity
            style={{
                width: props.width,
                height: props.height,
                backgroundColor: props.backgroundColor,
                alignItems: 'center',
                justifyContent: 'center',
                borderRadius: props.borderRadius,
                borderWidth: props.borderWidth,
                borderColor: props.borderColor,
                elevation: props.elevation,
                marginStart: props.marginStart,
            }}
            onPress={props.onPress}>
                <View style={{ alignItems: 'center',justifyContent: 'center'}}>
                    <Text
                    style={{
                        fontSize: 16, 
                        color: props.textColor,
                        fontFamily: 'ws_semibold'}}
                        >
                        {props.title}
                        </Text>
                </View>
            </TouchableOpacity>
        </View>
    );
}

export default CommanButton;