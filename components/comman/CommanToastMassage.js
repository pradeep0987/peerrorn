import { useEffect, useRef, useState } from "react";
import { Colors } from "../../src/colors/Colors";
const { View, Text, Animated, Dimensions, Image } = require("react-native");

function ToastMessage(props) {
    const windowHeight = Dimensions.get("window").height;
    const [status, setStatus] = useState(null);
    const popAnim = useRef(new Animated.Value(windowHeight * -1)).current;

    useEffect(() => {
        if (props.showToast) {
            popIn()
        }
    })
    const popIn = () => {
        Animated.timing(popAnim, {
            toValue: windowHeight * 0.35 * -1,
            duration: 300,
            useNativeDriver: true,
        }).start(popOut());
    };

    const popOut = () => {
        setTimeout(() => {
            Animated.timing(popAnim, {
                toValue: windowHeight * -1,
                duration: 300,
                useNativeDriver: true,
            }).start();
        }, props.duration);
    };
    return (
        <View style={{width: '80%', height: 60, alignItems: 'center', borderRadius: 50, }}>
            <Animated.View style={[{width: '80%', height: 60, alignItems: 'center', borderRadius: 50, }, { transform: [{ translateY: popAnim }], },]}>
                <View style={{ flexDirection: 'row' }}>
                    <Image style={{ width: 20, height: 20, borderRadius: 100, overflow: 'hidden' }} source={require('../../src/images/peerro_branding_logo.png')} />
                    <Text style={{ marginStart: 10, textAlign: 'center', width: '100%', color: Colors.colorBlack, fontWeight: 'bold' }}>{props.message}</Text>
                </View>
            </Animated.View>
        </View>
    );
}

export default ToastMessage;