import { View, Text, StyleSheet, Image, TouchableOpacity, ToastAndroid, Alert, Platform } from 'react-native';
import Toast from 'react-native-root-toast';
import { Colors } from '../../src/colors/Colors';

function CommanMessage(message) {
    Toast.show(message, {
        duration: Toast.durations.SHORT,
        position: Toast.positions.BOTTOM,
        shadow: true,
        animation: true,
        hideOnPress: true,
        delay: 0,
        backgroundColor: Colors.colorBlack,
        textColor: Colors.white,
    })
}

export default CommanMessage;