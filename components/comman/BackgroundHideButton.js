import { View, TouchableOpacity, Text } from "react-native";
import { Colors } from "../../src/colors/Colors";


const BackgroundHideButton = (props) => {
    return (
        <View style={{marginTop: props.marginTop,}}>
            <TouchableOpacity
            style={{
                width: props.width,
                height: props.height,
                backgroundColor: Colors.transparent,
                alignItems: 'center',
                justifyContent: 'center',
                borderRadius: props.borderRadius,
                borderWidth: props.borderWidth,
                borderColor: props.borderColor,
                elevation: props.elevation,
                marginStart: props.marginStart,
            }}
            onPress={props.onPress}>
                <View style={{ alignItems: 'center',justifyContent: 'center'}}>
                    <Text
                    style={{
                        fontSize: 17, 
                        color: props.textColor,
                        fontFamily: 'ws_semibold', }}
                        >
                        {props.title}
                        </Text>
                </View>
            </TouchableOpacity>
        </View>
    );
}

export default BackgroundHideButton;