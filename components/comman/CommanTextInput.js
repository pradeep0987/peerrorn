import { TextInput, View, Text, Image, TouchableOpacity, Pressable } from 'react-native';
import { Fonts } from '../../assets/fonts/Fonts';
import Icon from 'react-native-vector-icons/FontAwesome5';
import { Colors } from '../../src/colors/Colors';


function CommanTextInput(props) {
    return (
        <View style={{ marginTop: props.marginTop }}>
            <Pressable onPress={props.onPress}>
                <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                    <Text
                        style={{
                            fontSize: 16,
                            color: props.textColor,
                            alignSelf: 'flex-start',
                            fontFamily: 'ws_medium'
                        }}
                    >
                        {props.title}
                    </Text>
                </View>
                <TextInput
                    style={{
                        width: '100%',
                        height: props.height,
                        borderRadius: props.borderRadius,
                        elevation: props.elevation,
                        backgroundColor: props.backgroundColor,
                        borderWidth: props.borderWidth,
                        borderColor: props.borderColor,
                        marginTop: props.marginTopInput,
                        paddingStart: props.paddingStart,
                        paddingEnd: props.paddingEnd,
                        fontSize: 17,
                        color: Colors.colorBlack
                    }}
                    onChangeText={props.OnChangeText}
                    onChange={props.onChange}
                    value={props.Value}
                    editable={props.editable}
                    keyboardType={props.keyboardType}
                    maxLength={props.maxLength}
                    secureTextEntry={props.visible}
                    placeholder={props.placeholder}
                    placeholderTextColor={props.placeholderTextColor}
                    ref={props.inputRef}
                    onSubmitEditing={props.onSubmitEditing}
                    returnKeyType={props.returnKeyType}
                />
            </Pressable>
        </View>
    );
}

export default CommanTextInput;