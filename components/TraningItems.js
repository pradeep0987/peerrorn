import { View, Text, StyleSheet, Image, TouchableOpacity, ToastAndroid, ImageBackground, Pressable } from 'react-native';
import { Colors } from '../src/colors/Colors';
import CommanButton from './comman/CommanButton';
import VimeoPlayer from './VimeoPlayer/VimeoPlayer';
import React ,{ useState, useEffect } from 'react';
import VideoAdsJobsList from './JobsAds/VideoAdsJobsList';
import ImageAdsJobsList from './JobsAds/ImageAdsJobList';
import TemplateAdsJobsList from './JobsAds/TemplateAdsJobsList';


const TrainingItem = React.memo(({items , advertisement, index}) => {
    const [show, setShow] = useState(false);
    const [imagePanel, setImagePanel] = useState(false);
    const [videoPanel, setVideoPanel] = useState(false);
    const [templatePanel, setTemplatePanel] = useState(false);

    useEffect(() => {
        if (advertisement != null) {
            switch (advertisement.Ad_Type) {
                case "Image":
                    setImagePanel(true)
                    break;
                case "Video":
                    setVideoPanel(true)
                    break;
                case "Template":
                    setTemplatePanel(true)
                    break;
            }
        }


    })
    return(
        <View>
                {videoPanel && index === 0 && <VideoAdsJobsList
                    videoThem={advertisement.advertisement_thumbnail}
                    companyLogo={advertisement.company_logo}
                    videoUrl={advertisement.advertisement_file_embed}
                    age={advertisement.age}
                    hoursWeek={advertisement.hours_week}
                    companyname={advertisement.company_name}
                />}
                {imagePanel && index === 0 && <ImageAdsJobsList
                    imageUri={advertisement.advertisement_thumbnail}
                    companyLogo={advertisement.company_logo}
                    age={advertisement.age}
                    hoursWeek={advertisement.hours_week}
                    companyname={advertisement.company_name}
                />}
                {templatePanel && index === 0 && <TemplateAdsJobsList
                    companyLogo={advertisement.company_logo}
                    companyname={advertisement.company_name}
                    hoursWeek={advertisement.hours_week}
                    age={advertisement.age}
                />}
            <View style={{backgroundColor: Colors.white, borderRadius: 10,paddingBottom: 35, paddingEnd: 20, paddingStart: 20,paddingTop: 5, marginTop: 20}}>
                <View style={{flexDirection: 'row', padding: 5, alignSelf: 'flex-end'}}>
                    {items.top_label && <View style={{width: 110, height: 20, justifyContent: 'center', alignItems: 'center', backgroundColor: Colors.blue, borderRadius: 20, marginEnd: 80}}>
                        <Text style={{color: Colors.white, fontSize: 10, fontFamily: 'ws_regular'}}>{items.top_label}</Text>
                    </View>}
                    {items.top_label_right && <Text style={{color: Colors.accentMagenta, alignSelf: 'flex-end', fontSize: 10, marginBottom: 10, fontFamily: 'ws_regular'}} >{items.top_label_right}</Text>}
                </View>
                <View style={{flexDirection: 'row', alignItems: 'center'}}>
                    <View style={{width: 80, height: 80, borderRadius: 100, borderColor: Colors.colorGrey, borderWidth: 1, justifyContent: 'center', alignItems: 'center'}}>
                        <Image style={{width: 50, height: 50, overflow: 'hidden'}} source={items.logo_url ? {uri : items.logo_url} : require('../src/images/profile_without_border.png')} />
                    </View>
                    <View style={{marginStart: 15}}>
                        <Text style={{color: Colors.colorBlack,fontSize: 20, width: 200, fontFamily: 'ws_regular'}} >{items.training_title}</Text>
                        <Text style={{color: Colors.blue,fontSize: 16, marginTop: 5, width: 200}} >{items.company}</Text>
                        <View style={{flexDirection: 'row',marginTop: 2}}>
                            <Text style={{color: Colors.accentMagenta,fontSize: 16, fontFamily: 'ws_regular'}} >Age: </Text>
                            <Text style={{color: Colors.accentMagenta,fontSize: 16, fontFamily: 'ws_regular'}} >{items.minAgeFlag}</Text>
                        </View>
                    </View>
                </View>
                <View style={{flexDirection: 'row',marginTop: 20, alignItems: 'center', justifyContent: 'space-around',}}>
                    <View style={{justifyContent: 'center', alignItems: 'center'}}>
                        <Text style={{fontSize: 11}} >Distence</Text>
                        <Text style={{color: Colors.colorBlack ,fontSize: 14, marginTop: 10, fontFamily: 'ws_regular'}} >{items.distance.toFixed(2)} mi</Text>
                    </View>
                    <View style={{justifyContent: 'center', alignItems: 'center'}}>
                        <Text style={{fontSize: 11}} >TYPE</Text>
                        <Text style={{color: Colors.colorBlack ,fontSize: 14, marginTop: 10, fontFamily: 'ws_regular'}} >{items.training_type}</Text>
                    </View>
                    <View style={{justifyContent: 'center', alignItems: 'center'}}>
                        <Text style={{fontSize: 11,}} >Ready</Text>
                        <Text style={{color: items.pathway_score > 50 ? Colors.blue : Colors.colorBlack, fontSize: 14, marginTop: 10, fontFamily: 'ws_regular'}} >{items.pathway_score}%</Text>
                    </View>
                </View>
            </View>
        </View>
    );
})

export default TrainingItem;