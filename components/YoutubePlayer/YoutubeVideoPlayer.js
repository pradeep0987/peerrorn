import WebView from "react-native-webview";
import { Colors } from "../../src/colors/Colors";

function YoutubeVideoPlayer({video_url , autoPlay , loop , muted , control, height , width}) {
    const unformattedString = `<!DOCTYPE html>
      <html>
          <style type="text/css">
              html, body {
                  height: 100%;
                  width: 100%;
                  margin: 0;
                  padding: 0;
                  background-color: #<BACKGROUND_COLOR>;
                  overflow: hidden;
                  position: fixed;
                  position: relative;
              }
            
              .flexbox {
                  height: 100%;
                  width: 100%;
                  display: flex;
                  align-items: center;
                  justify-content: center;
              }
          </style>
            
          <head>
              <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
            
          </head>
            
          <body>
              <div class="flexbox">
                  <div id="player">
                  <iframe src="<VIDEO_URL>&autoplay=<AUTOPLAY>&muted=<MUTED>&controls=<CONTROL>&title=<TITLE>&byline=0&sidedock=0" height="<HEIGHT>" width="<WIDTH>"  frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>
                  </div>
              </div>
          </body>
      <script>
      var player = null;
          function initVimeo()
          {
            const iframe = document.querySelector('iframe');
             player = new Vimeo.Player(iframe);
          
            const sendEvent = (evt, data) => {
                  var payload = {
                    name: evt,
                    data: data,
                  };
                window.ReactNativeWebView.postMessage(JSON.stringify(payload));
            };
          
            player.on('play', function (data) {
                sendEvent("play", data);
            });
          
            player.on('pause', function (data) {
                sendEvent("pause", data);
            });
          
            player.on('ended', function (data) {
                sendEvent("ended", data);
            });
          
            player.on('error', function (data) {
              sendEvent("error", data);
            });
          
            player.on('timeupdate', function (data) {
              sendEvent("timeupdate", data);
            });
          
            player.getVideoTitle().then(function(title) {
              sendEvent("title", title);
            });
          }
        
          const sendEvent = (evt, data) => {
            var payload = {
              name: evt,
              data: data,
            };
            window.ReactNativeWebView.postMessage(JSON.stringify(payload));
          
      };
        
          function playVideo()
          {
              player.play()
          }
          function pauseVideo()
          {
              player.pause()
          }
          function slideTo(startSeconds)
          {
              player.setCurrentTime(startSeconds)
              playVideo();
              sendEvent("slideTo", startSeconds);
          }
             
    </script>
    </html>`;

    let formattedString = unformattedString.toString();
    formattedString = formattedString.replace('<VIDEO_URL>', video_url);
    formattedString = formattedString.replace('<AUTOPLAY>', `${autoPlay}`);
    formattedString = formattedString.replace('<LOOP>', `${loop}`);
    formattedString = formattedString.replace('<MUTED>', `${muted}`);
    formattedString = formattedString.replace('<CONTROL>', `${control}`);
    formattedString = formattedString.replace('<TITLE>', '0');
    formattedString = formattedString.replace('<HEIGHT>', `${height}`);
    formattedString = formattedString.replace('<WIDTH>', `${width}`);
    formattedString = formattedString.replace('<BACKGROUND_COLOR>', Colors.colorBlack);

    return(
      <WebView
        style={{width : width , height: height}}
        source={{uri : `${video_url}?rel=0&autoplay=1&showinfo=0&controls=0&loop=1`}}
        mediaPlaybackRequiresUserAction={false}
        startInLoadingState={true}
        scrollEnabled={false}
      />
    );
}

export default YoutubeVideoPlayer;