import { FlatList, View } from 'react-native';
import JobsListItems from './JobsItems';
import { useState } from 'react';
import TrainingItem from '../components/TraningItems.js'

function TrainingList({data , advertisement}) {
    return(
        <View style={{flex: 1}}>
            <FlatList data={data} renderItem={(itemsData) => {
                return(<TrainingItem items={itemsData.item} advertisement={advertisement} />);
            }} keyExtractor={(item , index) => index} showsVerticalScrollIndicator={false} showsHorizontalScrollIndicator={false} />
        </View>
    );
}

export default TrainingList;