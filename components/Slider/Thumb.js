import React, {memo} from 'react';
import {View, StyleSheet} from 'react-native';
import { Colors } from '../../src/colors/Colors';

const THUMB_RADIUS_LOW = 7;
const THUMB_RADIUS_HIGH = 7;

const Thumb = ({name}) => {
  return <View style={name === 'high' ? styles.rootHigh : styles.rootLow} />;
};

const styles = StyleSheet.create({
  rootLow: {
    width: THUMB_RADIUS_LOW * 2,
    height: THUMB_RADIUS_LOW * 2,
    borderRadius: THUMB_RADIUS_LOW,
    borderWidth: 1,
    borderColor: Colors.colorPrimary,
    backgroundColor: Colors.white,
  },
  rootHigh: {
    width: THUMB_RADIUS_HIGH * 2,
    height: THUMB_RADIUS_HIGH * 2,
    borderRadius: THUMB_RADIUS_HIGH,
    borderWidth: 2,
    borderColor: Colors.colorPrimary,
    backgroundColor: Colors.white,
  },
});

export default memo(Thumb);
