import { FlatList, View, Text, Image } from 'react-native';
// import JobsListItems from './JobsItems';
import React, { useEffect, useState } from 'react';
import { Colors } from '../src/colors/Colors';
import VideoAdsJobsList from './JobsAds/VideoAdsJobsList';
import ImageAdsJobsList from './JobsAds/ImageAdsJobList';
import TemplateAdsJobsList from './JobsAds/TemplateAdsJobsList';
import CommanButton from './comman/CommanButton';

function JobsList({ data, advertisement, onEndReached, flatListRef, onScroll, mainIndex, scrollBeginDrag, distanceFilter, selectedId, extraData, onRefresh, viewAllJobs }) {
    const [onScrollBeginDrag, setOnScrollBeginDrag] = useState(true);
    const [onEndReachedCalledDuringMomentum, setOnEndReachedCalledDuringMomentum] = useState(true)
    const [noDataFound, setNoDataFound] = useState(data == null || data === "" ? false : true)

    const JobsListItems = React.memo(({ top_label, top_label_right, logo_url, title, company, minage, distance, salary_offered, pathway_score, index, advertisement, salary_interval, distanceFilter , indexEnd }) => {
        const [show, setShow] = useState(false);
        const [imagePanel, setImagePanel] = useState(false);
        const [videoPanel, setVideoPanel] = useState(false);
        const [templatePanel, setTemplatePanel] = useState(false);
        const [adsIndex, setAdsIndex] = useState(mainIndex)

        useEffect(() => {
            if (advertisement != null) {
                if (onEndReached) {
                    switch (advertisement.Ad_Type) {
                        case "Image":
                            setImagePanel(true)
                            break;
                        case "Video":
                            setVideoPanel(true)
                            break;
                        case "Template":
                            setTemplatePanel(true)
                            break;
                    }
                }
            }


        })

        function insert(str, index, value) {
            if (str.charAt(0) !== "$") {
                return str.substr(0, index) + value + str.substr(index);
            } else {
                return str;
            }
        }

        function Capitalize(str) {
            return str.charAt(0).toUpperCase() + str.slice(1);
        }

        return (
            <View>
                {videoPanel && adsIndex === index && <VideoAdsJobsList
                    videoThem={advertisement.advertisement_thumbnail}
                    companyLogo={advertisement.company_logo}
                    videoUrl={advertisement.advertisement_file_embed}
                    age={advertisement.age}
                    hoursWeek={advertisement.hours_week}
                    companyname={advertisement.company_name}
                />}
                {imagePanel && adsIndex === index && <ImageAdsJobsList
                    imageUri={advertisement.advertisement_thumbnail}
                    companyLogo={advertisement.company_logo}
                    age={advertisement.age}
                    hoursWeek={advertisement.hours_week}
                    companyname={advertisement.company_name}
                />}
                {templatePanel && mainIndex === index && <TemplateAdsJobsList
                    companyLogo={advertisement.company_logo}
                    companyname={advertisement.company_name}
                    hoursWeek={advertisement.hours_week}
                    age={advertisement.age}
                />}
                <View style={{ backgroundColor: Colors.white, borderRadius: 7, paddingBottom: 35, paddingEnd: 20, paddingStart: 20, paddingTop: 5, marginTop: index >= 0 ? 15 : 1, elevation: 10 }}>
                    <View style={{ flexDirection: 'row', padding: 5, alignSelf: 'flex-end' }}>
                        {top_label && <View style={{ width: 110, height: 20, justifyContent: 'center', alignItems: 'center', backgroundColor: Colors.blue, borderRadius: 20, marginEnd: 80 }}>
                            <Text style={{ color: Colors.white, fontSize: 10, fontFamily: 'ws_regular' }}>{top_label}</Text>
                        </View>}
                        {top_label_right && <Text style={{ color: Colors.accentMagenta, alignSelf: 'flex-end', fontSize: 10, marginBottom: 10, fontFamily: 'ws_regular' }} >{top_label_right}</Text>}
                    </View>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <View style={{ width: 80, height: 80, borderRadius: 100, borderColor: Colors.colorCGrey, borderWidth: 1, justifyContent: 'center', alignItems: 'center' }}>
                            <Image style={{ width: 55, height: 55, overflow: 'hidden', borderRadius: 5 }} source={logo_url ? { uri: logo_url } : require('../src/images/default_picture.png')} />
                        </View>
                        <View style={{ marginStart: 15 }}>
                            <Text style={{ color: Colors.colorBlack, fontSize: 20, width: 200, fontFamily: 'ws_regular' }} >{title}</Text>
                            <Text style={{ color: Colors.blue, fontSize: 16, marginTop: 5, width: 200 }} >{company}</Text>
                            <Text style={{ color: Colors.accentMagenta, fontSize: 16, fontFamily: 'ws_regular' }} >{minage ? "Age : " + minage + "+" : ''}</Text>
                        </View>
                    </View>
                    <View style={{ flexDirection: 'row', marginTop: 20, alignItems: 'center', justifyContent: 'space-around', }}>
                        <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                            <Text style={{ fontSize: 11 }} >Distance</Text>
                            <Text style={{ color: Colors.colorBlack, fontSize: 14, marginTop: 10, fontFamily: 'ws_regular' }} >{distance ? distance?.toFixed(1) + " mi" : `Within ${distanceFilter} mi`}</Text>
                        </View>
                        <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                            <Text style={{ fontSize: 11 }} >PAY</Text>
                            {salary_offered && <Text style={{ color: Colors.colorBlack, fontSize: 14, marginTop: 10, fontFamily: 'ws_regular', }} >{salary_interval ? "$" + salary_offered + "/" + Capitalize(salary_interval) : salary_offered}</Text>}
                            {!salary_offered && <Text style={{ color: Colors.colorBlack, fontSize: 14, marginTop: 10, fontFamily: 'ws_regular' }} >NA</Text>}
                        </View>
                        <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                            <Text style={{ fontSize: 11, }} >Ready</Text>
                            <Text style={{ color: pathway_score > 50 ? Colors.accentGreen : Colors.colorBlack, fontSize: 14, marginTop: 10, fontFamily: 'ws_regular' }} >{pathway_score}%</Text>
                        </View>
                    </View>
                </View>
                {/* {noDataFound && index == mainIndex && <Text style={{ color: Colors.colorBlack, fontSize: 20, fontFamily: 'ws_regular', textAlign: 'center' }} >No Data Found</Text>} */}
                {viewAllJobs && indexEnd && <CommanButton title='All Jobs' width={'100%'} height={50} backgroundColor={Colors.accentMagenta} borderRadius={20} textColor={Colors.white} marginTop={20} />}
            </View>
        );
    })


    return (
        <View style={{ flex: 1 }}>
            <FlatList ref={flatListRef} data={data} renderItem={({ item, index }) => {
                const isEnd = index === data.length - 1;
                return (<JobsListItems indexEnd={isEnd} top_label={item.top_label} top_label_right={item.top_label_right} logo_url={item.logo_url} title={item.title ? item.title : item.name} minage={item.minage} distance={item.distance} salary_offered={item.salary_offered ? item.salary_offered : item.salary_min_annual} salary_interval={item.salary_interval} pathway_score={item.pathway_score ? item.pathway_score : 100} company={item.company ? item.company : item.hiring_company.name} index={index} advertisement={advertisement} distanceFilter={distanceFilter} />);
            }} keyExtractor={(item, index) => index} showsVerticalScrollIndicator={false} showsHorizontalScrollIndicator={false}
                onEndReached={() => {
                    if (!onEndReachedCalledDuringMomentum) {
                        onEndReached(true)
                        setOnEndReachedCalledDuringMomentum(true)
                    }
                }} onEndReachedThreshold={0.5} onScroll={onScroll} initialNumToRender={10} maxToRenderPerBatch={20}
                onScrollBeginDrag={() => {
                    setTimeout(() => { setOnScrollBeginDrag(false) }, 3000)
                    scrollBeginDrag(false)
                }}
                onMomentumScrollBegin={() => setOnEndReachedCalledDuringMomentum(false)}
                extraData={extraData}
                onRefresh={onRefresh} />
        </View>
    );
}

export default JobsList;