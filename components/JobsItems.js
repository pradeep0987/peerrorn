import { View, Text, StyleSheet, Image, TouchableOpacity, ToastAndroid, ImageBackground, Pressable } from 'react-native';
import { Colors } from '../src/colors/Colors';
import VimeoPlayer from './VimeoPlayer/VimeoPlayer';
import React, { useMemo, useState } from 'react';
import codegenNativeCommands from 'react-native/Libraries/Utilities/codegenNativeCommands';
import CommanButton from './comman/CommanButton';


const JobsListItems = React.memo(({ items, index, advertisement }) => {
    const [show, setShow] = useState(false);
    const [imagePanel, setImagePanel] = useState(false);
    const [videoPanel, setVideoPanel] = useState(false);
    const [templatePanel, setTemplatePanel] = useState(false);
    return (
        <View>
            <View style={{ backgroundColor: Colors.white, borderRadius: 7, paddingBottom: 35, paddingEnd: 20, paddingStart: 20, paddingTop: 5, marginTop: 15, elevation: 10 }}>
                <View style={{ flexDirection: 'row', padding: 5, alignSelf: 'flex-end' }}>
                    {items.top_label && <View style={{ width: 110, height: 20, justifyContent: 'center', alignItems: 'center', backgroundColor: Colors.blue, borderRadius: 20, marginEnd: 80 }}>
                        <Text style={{ color: Colors.white, fontSize: 10, fontFamily: 'ws_regular' }}>{items.top_label}</Text>
                    </View>}
                    {items.top_label_right && <Text style={{ color: Colors.accentMagenta, alignSelf: 'flex-end', fontSize: 10, marginBottom: 10, fontFamily: 'ws_regular' }} >{items.top_label_right}</Text>}
                </View>
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <View style={{ width: 80, height: 80, borderRadius: 100, borderColor: Colors.colorGrey, borderWidth: 1, justifyContent: 'center', alignItems: 'center' }}>
                        <Image style={{ width: 50, height: 50, overflow: 'hidden' }} source={items.logo_url ? { uri: items.logo_url } : require('../src/images/profile_without_border.png')} />
                    </View>
                    <View style={{ marginStart: 15 }}>
                        <Text style={{ color: Colors.colorBlack, fontSize: 20, width: 200, fontFamily: 'ws_regular' }} >{items.title}</Text>
                        <Text style={{ color: Colors.blue, fontSize: 16, marginTop: 5, width: 200 }} >{items.company}</Text>
                        <View style={{ flexDirection: 'row', marginTop: 2 }}>
                            <Text style={{ color: Colors.accentMagenta, fontSize: 16, fontFamily: 'ws_regular' }} >Age: {items.minage}+ </Text>
                        </View>
                    </View>
                </View>
                <View style={{ flexDirection: 'row', marginTop: 20, alignItems: 'center', justifyContent: 'space-around', }}>
                    <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                        <Text style={{ fontSize: 11 }} >Distence</Text>
                        <Text style={{ color: Colors.colorBlack, fontSize: 14, marginTop: 10, fontFamily: 'ws_regular' }} >{items.distance?.toFixed(1)} mi</Text>
                    </View>
                    <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                        <Text style={{ fontSize: 11 }} >PAY</Text>
                        <Text style={{ color: Colors.colorBlack, fontSize: 14, marginTop: 10, fontFamily: 'ws_regular' }} >{items.salary_offered}</Text>
                    </View>
                    <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                        <Text style={{ fontSize: 11, }} >Ready</Text>
                        <Text style={{ color: items.pathway_score > 50 ? Colors.accentGreen : Colors.colorBlack, fontSize: 14, marginTop: 10, fontFamily: 'ws_regular' }} >{items.pathway_score}%</Text>
                    </View>
                </View>
            </View>
        </View>
    );
})

export default JobsListItems;